/**
 * Created by andrey on 13.05.2018.
 */
var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var sass        = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
// Static server
gulp.task('serve', ['sass'], function() {
    browserSync.init({
        proxy: "http://iwobox.com1"
    });
    gulp.watch("inc/themes/default/assets/scss/*/*.scss", ['sass']);
    gulp.watch("inc/themes/default/assets/scss/*.scss", ['sass']);
    gulp.watch("app/*/*.php").on('change', browserSync.reload);
});
gulp.task('sass', function() {
    return gulp.src("inc/themes/default/assets/scss/core.scss")
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(sourcemaps.write('inc/themes/default/assets/css'))
        .pipe(gulp.dest("inc/themes/default/assets/css/"))
        .pipe(browserSync.stream());
});

gulp.task('default', ['serve']);


gulp.task('sass2', function() {
    return gulp.src("assets/scss/core.scss")
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(sourcemaps.write('assets/css'))
        .pipe(gulp.dest("assets/css/"))
});