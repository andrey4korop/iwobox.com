
  var Cropper = window.Cropper;
  var URL = window.URL || window.webkitURL;
  var container = document.querySelector('.img-container');
  window.image = container.getElementsByTagName('img').item(0);
  var download = document.getElementById('download');
  var actions = document.getElementById('actions');

  var options = {
      dragMode:true,
    viewMode:1,

    crop: function (e) {
      var data = e.detail;

    },
  };
  var cropper = new Cropper(image, options);
  var originalImageURL = image.src;
  var uploadedImageType = 'image/jpeg';
  var uploadedImageName = 'cropped.jpg';
  var uploadedImageURL;
  function dataURItoBlob(dataURI) {
      var byteString = atob(dataURI.split(',')[1]);
      var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
      var ab = new ArrayBuffer(byteString.length);
      var ia = new Uint8Array(ab);
      for (var i = 0; i < byteString.length; i++) {
          ia[i] = byteString.charCodeAt(i);
      }
      return new Blob([ab], {type: mimeString});
  }
  //CROP btn
  function _crop(autoselect) {
      var result = cropper.getCroppedCanvas({maxWidth: 4096, maxHeight: 4096, imageSmoothingQuality: 'high'})
      var blob = dataURItoBlob(result.toDataURL(uploadedImageType));
      var t = new FormData();
      t.append('ofm-files', blob, uploadedImageName);
      veryNeedUpload(t.getAll('ofm-files'), autoselect);
  }
$('body').on('click', '.crop_btn', function () {
    if($(":input[name='type']:checked").val()=='album' && $('.toggle_type').data('value') == 'albumCroper'){
        var original_data = JSON.parse(JSON.stringify(cropper.getData()));
        var set_data = JSON.parse(JSON.stringify(cropper.getData()));
        var sizeCrop = Math.round(original_data.height);
        var row = Math.round(original_data.width / sizeCrop);
        cropper.setAspectRatio(1);
        if(sizeCrop < 320){
            $('#modalCrop .er').show();
            setTimeout(function () {
                $('#modalCrop .er').hide();
            }, 5000);
            return false;
        }else {
            for (var i = 0; i < row; i++) {
                    set_data.width = sizeCrop;
                    set_data.height = sizeCrop;
                    set_data.x = original_data.x + sizeCrop * i;
                    set_data.y = original_data.y;
                    cropper.setData(set_data);
                    _crop(true);
            }
            $('.toggle_type').data('value', 'default');
        }
    }else if($(":input[name='type']:checked").val()!='mozaika') {
        _crop()
    }else{
        var original_data = JSON.parse(JSON.stringify(cropper.getData()));
        var set_data = JSON.parse(JSON.stringify(cropper.getData()));
        var sizeCrop = Math.round(original_data.width / 3);
        var row = Math.round(original_data.height / sizeCrop);
        cropper.setAspectRatio(1);
        if(sizeCrop < 320){
            $('#modalCrop .er').show();
            setTimeout(function () {
                $('#modalCrop .er').hide();
            }, 5000);
            return false;
        }else {
            for (var i = 0; i < row; i++) {
                for (var j = 0; j < 3; j++) {
                    set_data.width = sizeCrop;
                    set_data.height = sizeCrop;
                    set_data.x = original_data.x + sizeCrop * j;
                    set_data.y = original_data.y + sizeCrop * i;
                    cropper.setData(set_data);
                    _crop(true);
                }
            }
        }
    }
    $('#modalCrop').toggleClass('on');
});
  //dragMode_btn
  $('body').on('click', '.dragMode_btn', function () {
      cropper.setDragMode($(this).data('value'));
  });
  //dragMode_btn
  $('body').on('click', '.zoom_btn', function () {
      cropper.zoom($(this).data('value'));
  });
  //rotate
  $('body').on('click', '.rotate_btn', function () {
      cropper.rotate($(this).data('value'));
  });
  //scale
  $('body').on('click', '.scale_btn', function () {
    if($(this).data('method') == 'scaleX') {
        cropper.scaleX($(this).data('value'));
        $(this).data('value', -$(this).data('value'))
    }
    if($(this).data('method') == 'scaleY') {
        cropper.scaleY($(this).data('value'));
        $(this).data('value', -$(this).data('value'))
    }
  });
  //reset
  $('body').on('click', '.reset_btn', function () {
      cropper.reset();
  });
  $('body').on('click', '.close', function () {
      $('#modalCrop').toggleClass('on');
      $('.toggle_type').data('value', 'default');
  });
  //AspestRatio
  $('body').on('click', '.AspectRatio_btn', function () {
      cropper.setAspectRatio($(this).data('value'));
  });
  $('body').on('click', '.uploadImg', function () {
    $('#inputImage').click();
  });
  // Buttons
  if (!document.createElement('canvas').getContext) {
    $('button[data-method="getCroppedCanvas"]').prop('disabled', true);
  }

  if (typeof document.createElement('cropper').style.transition === 'undefined') {
    $('button[data-method="rotate"]').prop('disabled', true);
    $('button[data-method="scale"]').prop('disabled', true);
  }
    $('body').on('click', '.crop-function', function () {
        var string = $(this).siblings('.ofm-file-preview').css('background-image');
        image.src = string.substring(string.indexOf('"')+1,string.indexOf('"',7));
        createCrop();
        if($(":input[name='type']:checked").val()=='mozaika') {
            cropper.setAspectRatio(1);
            $('.AspectRatio_btns').hide();
            $('.AspectRatio_btnM').show();
            $('.AspectRatio_btnG').hide();
            $('.AspectRatio_btnT').hide();
        }else{
            $('.AspectRatio_btns').show();
            $('.AspectRatio_btnM').hide();
            $('.AspectRatio_btnG').hide();
            $('.AspectRatio_btnT').hide();
            if($(":input[name='type']:checked").val()=='album'){
                $('.AspectRatio_btnT').show();
            }
        }
    })

  // Import image
  var inputImage = document.getElementById('inputImage');
    $(document).on('change', '#inputImage',  function () {
      var files = this.files;
      var file;
      if (cropper && files && files.length) {
        file = files[0];
          console.log(file.type);
        if (/^image\/\w+/.test(file.type)) {
          uploadedImageType = file.type;
          uploadedImageName = file.name;
          if (uploadedImageURL) {
            URL.revokeObjectURL(uploadedImageURL);
          }
          image.src = uploadedImageURL = URL.createObjectURL(file);
          createCrop();
          if($(":input[name='type']:checked").val()=='mozaika') {
              cropper.setAspectRatio(1);
              $('.AspectRatio_btns').hide();
              $('.AspectRatio_btnM').show();
              $('.AspectRatio_btnG').hide();
              $('.AspectRatio_btnT').hide();
          }else{
              $('.AspectRatio_btns').show();
              $('.AspectRatio_btnM').hide();
              $('.AspectRatio_btnG').hide();
              $('.AspectRatio_btnT').hide();
              if($(":input[name='type']:checked").val()=='album'){
                  $('.AspectRatio_btnT').show();
              }
          }
          inputImage.value = null;
        } else if(/^video\/\w+/.test(file.type)){
            filemanager.upload(files, true);
        }else{
          window.alert('Please choose an image file.');
        }
      }
    });
function createCrop() {
    cropper.destroy();
    cropper = new Cropper(image, options);
    $('#modalCrop').toggleClass('on');
}
$('body').on('click', '.toggle_type', ()=>{
    if($('.toggle_type').data('value') == 'default'){
        $('.toggle_type').data('value', 'albumCroper');
        $('.AspectRatio_btns').hide();
        $('.AspectRatio_btnM').hide();
        $('.AspectRatio_btnG').show()
        cropper.setAspectRatio(2);
    }else{
        $('.toggle_type').data('value', 'default');
        $('.AspectRatio_btns').show();
        $('.AspectRatio_btnM').hide();
        $('.AspectRatio_btnG').hide()
    }
});

