<?php 
	/**
	 * User Model
	 *
	 * @version 1.0
	 * @author Onelab <hello@onelab.co> 
	 * 
	 */
	
	class UserProxyModel extends DataEntry
	{
	    /**
         * Table name
	    */
	    private $table="user_proxy";
		/**
		 * Extend parents constructor and select entry
		 */
	    public function __construct()
	    {
	        parent::__construct();
	        //$this->select($uniqid);
            //Проверка существует таблица
            if(!is_object(DB::query("Show tables like '". TABLE_PREFIX . $this->table ."'")->first())){
                DB::query("CREATE TABLE `". TABLE_PREFIX . $this->table ."` (
                    `id` INT NOT NULL AUTO_INCREMENT, PRIMARY KEY (`id`),
                    `user_id` INT NOT NULL,
                    `country` VARCHAR(5) NULL DEFAULT NULL,
                    `proxy` VARCHAR(255) NULL DEFAULT NULL,
                    `log_from_proxy` TEXT NULL DEFAULT NULL,
                    `date_exp_proxy` VARCHAR(50) NULL DEFAULT NULL,
                    `date_create` DATETIME NOT NULL
                ) COLLATE='utf8_general_ci';");
            }

	    }
	    /**
	     * Получение данных по пользователю
	     * @param  int|string $id Value of the any unique field
	     * @return self       
	     */
	    public function select($id)
	    {
	    	if ($id>0) {
		    	$query = DB::table(TABLE_PREFIX.$this->table)
			    	      ->where('user_id', "=", $id)
			    	      ->limit(1)
			    	      ->select("*");
		    	if ($query->count() == 1) {
		    		$resp = $query->get();
		    		$r = $resp[0];

		    		foreach ($r as $field => $value)
		    			$this->set($field, $value);

		    		$this->is_available = true;
		    	} else {
		    		$this->data = array();
		    		$this->is_available = false;
		    	}
	    	}
	    	return $this;
	    }

	    /**
	     * Extend default values
	     * @return self
	     */
	    public function extendDefaults()
	    {
	    	$defaults = array(
	    		"user_id" => "1",
	    		"country" => "ua",
	    		"proxy" => "",
	    		"log_from_proxy" =>'{}',
	    		"date_exp_proxy" => "",
	    		"date_create" => date("Y-m-d H:i:s"),
	    	);


	    	foreach ($defaults as $field => $value) {
	    		if (is_null($this->get($field)))
	    			$this->set($field, $value);
	    	}
	    }


	    /**
	     * Insert Data as new entry
	     */
	    public function insert()
	    {
	    	$this->extendDefaults();

	    	$id = DB::table(TABLE_PREFIX.$this->table)
		    	->insert(array(
		    		"id" => null,
		    		"user_id" => $this->get("user_id"),
		    		"country" => $this->get("country"),
		    		"proxy" => $this->get("proxy"),
		    		"log_from_proxy" => $this->get("log_from_proxy"),
		    		"date_exp_proxy" => $this->get("date_exp_proxy"),
		    		"date_create" => $this->get("date_create"),
		    	));

	    	$this->set("id", $id);
	    	$this->markAsAvailable();
	    	return $this->get("id");
	    }


	    /**
	     * Update selected entry with Data
	     */
	    public function update()
	    {
	    	if (!$this->isAvailable())
	    		return false;

	    	$this->extendDefaults();

	    	$id = DB::table(TABLE_PREFIX.$this->table)
	    		->where("id", "=", $this->get("id"))
		    	->update(array(
                    "user_id" => $this->get("user_id"),
		    		"country" => $this->get("country"),
		    		"proxy" => $this->get("proxy"),
		    		"log_from_proxy" => $this->get("log_from_proxy"),
		    		"date_exp_proxy" => $this->get("date_exp_proxy"),
		    		"date_create" => $this->get("date_create"),
		    	));
	    	return $this;
	    }

	    /**
		 * Remove selected entry from database
		 */
	    public function delete()
	    {
	    	if(!$this->isAvailable())
	    		return false;

	    	DB::table(TABLE_PREFIX.$this->table)->where("id", "=", $this->get("id"))->delete();
	    	$this->is_available = false;
	    	return true;
	    }
	}
?>