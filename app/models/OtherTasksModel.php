<?php 
/**
 * Posts model
 *
 * @version 1.0
 * @author Onelab <hello@onelab.co> 
 * 
 */
class OtherTasksModel extends DataList
{
    private $tableName = 'other_task';
	/**
	 * Initialize
	 */
	public function __construct()
	{
		$this->setQuery(DB::table(TABLE_PREFIX.$this->tableName));
	}

    public function fetchData()
    {
        $this->getQuery()
             ->leftJoin(TABLE_PREFIX.TABLE_USERS,
                        TABLE_PREFIX.$this->tableName.".user_id",
                        "=",
                        TABLE_PREFIX.TABLE_USERS.".id")
             ->leftJoin(TABLE_PREFIX.TABLE_ACCOUNTS,
                        TABLE_PREFIX.$this->tableName.".account_id",
                        "=",
                        TABLE_PREFIX.TABLE_ACCOUNTS.".id");
        $this->paginate();

        $this->getQuery()
             ->select(TABLE_PREFIX.$this->tableName.".*")
             ->select(TABLE_PREFIX.TABLE_USERS.".firstname")
             ->select(TABLE_PREFIX.TABLE_USERS.".lastname")
             ->select(TABLE_PREFIX.TABLE_ACCOUNTS.".username");
        $this->data = $this->getQuery()->get();
        return $this;
    }
}
