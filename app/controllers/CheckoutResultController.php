<?php
/**
 * CheckoutResult Controller
 */
class CheckoutResultController extends Controller
{
    /**
     * Process
     */
    public function process()
    {
        $Route = $this->getVariable("Route");
        $AuthUser = $this->getVariable("AuthUser");

        if (isset($Route->params->id, $Route->params->hash)) {
            if (!$AuthUser) {
                header("Location: ".APPURL."/login");
                exit;
            }

            $Order = Controller::model("Order", $Route->params->id);
            $this->setVariable("Order", $Order);

            if ($Order->isAvailable() && 
                $Order->get("user_id") == $AuthUser->get("id") && 
                $Route->params->hash == sha1($Order->get("id").NP_SALT)) 
            {
                $PaymentGateway = Payments\Gateway::choose($Order->get("payment_gateway"));

                if ($PaymentGateway) {
                    $PaymentGateway->setOrder($Order);

                    try {
                        $resp = $PaymentGateway->callback([
                            "paymentId" => Input::get("paymentId")
                        ]);

                        if ($resp) {
                            $this->setVariable("Success", true);

                            /**
                             * buy proxy
                             */



                            $plan =  json_decode($Order->get("data"))->plan;
                            $countday = 31;
                            if($plan == 'annual') {
                                $countday = 365;
                            }else{
                                $countday = 31;
                            }
                            $user_proxy = Controller::model('UserProxy')->select($Order->get("user_id"));
                            try {
                                $responseProxy = file_get_contents('https://proxy6.net/api/2d7923a2a9-8762c794a2-2bb5a847b5/buy?nokey=&count=1&period=' . $countday . '&country=' . $user_proxy->get('country'));
                                //$responseProxy = file_get_contents('https://proxy6.net/api/2d7923a2a9-8762c794a2-2bb5a847b5/getcountry');

                                   $objResponseProxy = json_decode($responseProxy);
                                if ($objResponseProxy->status == 'yes') {
                                    foreach ($objResponseProxy->list as $pr) {
                                        $user_proxy->set('date_exp_proxy', $pr->date_end);
                                        $user_proxy->set('proxy', 'https://' . $pr->user . ':' . $pr->pass . '@' . $pr->host . ':' . $pr->port);

                                        $accounts = Controller::model('Accounts')->where('user_id', '=', $AuthUser->get('id'))->fetchData()->getDataAs('Account');
                                        if (count($accounts) > 0) {
                                            foreach ($accounts as $account) {
                                                $account->set("proxy", $user_proxy->get('proxy'))->save();
                                            }
                                        }
                                    }
                                }
                                $user_proxy->set('log_from_proxy', $responseProxy)->save();
                            }catch (Exception $e){
                                $user_proxy->set('log_from_proxy', 'Error')->save();
                            }
                            /**
                             * end buy Proxy
                             */
                        }
                    } catch (Exception $e) {
                        $this->setVariable("ErrMsg", $e->getMessage());
                    }
                }
            }
        }

        $AuthUser->refresh();
        $this->view("checkout-result");
    }
}
