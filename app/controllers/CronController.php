<?php
/**
 * Cron Controller
 */
class CronController extends Controller
{
    /**
     * Process
     */
    public function process()
    {
        set_time_limit(0);
        var_dump(date("Y-m-d H:i"));
        $this->posts();
        try {
            \Event::trigger("cron.add");
        }catch (Exception $e){
            echo '<pre>';
            var_dump($e);
            echo '</pre>';
        }
        try {
        $this->otherTask();
        }catch (Exception $e){
            echo '<pre>';
            var_dump($e);
            echo '</pre>';
        }
        echo "Cron task processed!";
    }


    /**
     * Process scheduled posts
     */
    private function posts()
    {
        // Get scheduled posts
        $Posts = Controller::model("Posts");
        $Posts->whereIn(TABLE_PREFIX.TABLE_POSTS.".status", ["scheduled"])
              ->where(TABLE_PREFIX.TABLE_POSTS.".is_scheduled", "=", "1")
              ->where(TABLE_PREFIX.TABLE_POSTS.".schedule_date", "<=", date("Y-m-d H:i").":59")
              ->setPageSize(5) // Limit posts to prevent server overload
              ->setPage(1)
              ->fetchData();


        if ($Posts->getTotalCount() < 1) {
            // There is not any scheduled posts
            return true;
        }

        foreach ($Posts->getDataAs("Post") as $Post) {
            // Update post status
            $Post->set("status", "publishing")->save();

            try {
                \InstagramController::publish($Post);
            } catch (\Exception $e) {
                // Do nothing here
            }
        }

        return true;
    }
    private function otherTask(){
        $tasks = Controller::model('OtherTasks');
        $tasks->whereIn(TABLE_PREFIX.'other_task'.".status", ["wait"])
            ->where(TABLE_PREFIX.'other_task'.".schedule_date", "<=", date("Y-m-d H:i").":59")
            ->setPageSize(30)
            ->setPage(1)
            ->fetchData();
        if ($tasks->getTotalCount() < 1) {
            // There is not any scheduled posts
            return true;
        }
        $trotlet = [];
        $as = 0;
        foreach ($tasks->getDataAs("OtherTask") as $task) {
            // Update post status
            //$task->set("status", "worked")->save();
            $l = false;
            var_dump(++$as);
            if(count($trotlet)){
                var_dump($task->get('account_id'));
                var_dump($task->get('id'));
                var_dump($trotlet);
                $l = $trotlet[$task->get('account_id')];
                if(!is_null($l)){
                    $l = true;
                }
                var_dump($l);
                echo $task->get('id');
            }
            if(!$l) {
                switch ($task->get('type')) {
                    case 'like':
                        if ($this->likeAfterFollow($task) === 'trotlet') {
                            $trotlet[$task->get('account_id')]=$task->get('account_id');
                        }
                        break;
                    case 'unfollow':
                        //$this->unFollowAfterFollow($task);
                        if ($this->unFollowAfterFollow($task) === 'trotlet') {
                            $trotlet[$task->get('account_id')]=$task->get('account_id');
                        }
                        break;
                    case 'follow':
                        if ($this->follow($task) === 'trotlet') {
                            $trotlet[$task->get('account_id')]=$task->get('account_id');
                        }
                        break;
                    case 'direct':
                        $this->direct($task);
                        break;
                }
            }
        }
        return true;
    }
    private function likeAfterFollow($task)
    {
        $Account = \Controller::model("Account", $task->get("account_id"));
        try {
            $Instagram = InstagramController::login($Account);
        }catch (Exception $exception){
            echo '<pre>';
            echo 'likeAfterFollow login ';
            echo ' task_id = ' . $task->get('id');
            echo '</pre>';
            return false;
        }

        try {
            $feed = $Instagram->timeline->getUserFeed($task->get('data.pk'));

        } catch (InstagramAPI\Exception\ThrottledException $e){
            var_dump('trotlet');
            return 'trotlet';
        }catch (\Exception $e) {
            // Couldn't get instagram feed related to the user
            $task->set('status', 'error');
            $task->set('worked_date', date("Y-m-d H:i:s"));
            $task->save();
            return false;
        }
        $items = $feed->getItems();
        $sort = 'sortable';
        $likeProperty = json_decode($task->get('data.likeProperty'));
        if(!is_null($likeProperty) && property_exists($likeProperty, 'sort')){
            $sort = $likeProperty->sort;
        }
        if($sort=='shuffle') {
            shuffle($items);
        }
        foreach ($items as $item) {
            if ($item->getId() && !$item->getHasLiked()) {
                try {
                    $like_module = "feed_timeline";
                    $like_extra_data = [];
                    $like_extra_data['username'] = $item->getUser()->getUsername();
                    $like_extra_data['user_id'] = $item->getUser()->getPk();
                    InstagramController::like($Instagram, $item->getId(), $like_module, $like_extra_data);
                    //$Instagram->media->like($item->getId(), $like_module, $like_extra_data);
                }catch (Exception $e){
                    $task->set('status', 'error');
                    $task->set('worked_date', date("Y-m-d H:i:s"));
                    $task->save();
                    return false;
                }
                $task->set('status', 'success');
                $task->set('worked_date', date("Y-m-d H:i:s"));
                $task->save();
                var_dump('yes');
                return true;
            }
        }
        $task->set('status', 'error');
        $task->set('worked_date', date("Y-m-d H:i:s"));
        $task->save();
    }
    private function unFollowAfterFollow($task)
    {
        $Account = \Controller::model("Account", $task->get("account_id"));
        try {
            $Instagram = InstagramController::login($Account);
        }catch (Exception $exception){
            echo '<pre>';
            var_dump($exception->getCode(), $exception->getMessage());
            echo 'unFollowAfterFollow login ';
            echo ' task_id = ' . $task->get('id');
            echo '</pre>';
            return false;
        }
        try {
            echo '<pre>';

            var_dump($task->get('data.pk'));
            $user = $Instagram->people->getFriendship($task->get('data.pk'));
            var_dump($user->getFollowing());
            var_dump($user->getFollowed_by());

            echo '</pre>';

            if (!$user->getFollowed_by()) {
                $Instagram->people->unfollow($task->get('data.pk'));
            }
            $task->set('status', 'success');
            $task->set('worked_date', date("Y-m-d H:i:s"));
            $task->save();
            var_dump('yes');
            return true;
        }catch (InstagramAPI\Exception\FeedbackRequiredException $e){
            $task->set('status', 'error');
            $task->set('worked_date', date("Y-m-d H:i:s"));
            $task->save();
            echo '<pre>';
            var_dump($e->getCode(), $e->getMessage());
            echo 'unFollowAfterFollow FeedbackRequiredException ';
            echo ' task_id = ' . $task->get('id');
            echo '</pre>';
            return false;

        } catch (InstagramAPI\Exception\ThrottledException $e){
            var_dump('trotlet');
            return 'trotlet';
        }catch (\Exception $e) {
            // Couldn't get instagram feed related to the user
            echo '<pre>';
            var_dump($e->getCode(), $e->getMessage());
            echo 'unFollowAfterFollow ';
            echo ' task_id = ' . $task->get('id');
            echo '</pre>';
            return false;
        }
    }
    private function follow($task)
    {
        var_dump('follow');
        $Account = \Controller::model("Account", $task->get("account_id"));
        try {
            $Instagram = InstagramController::login($Account);
        }catch (Exception $exception){
            return false;
        }

        try {

            //$t = $Instagram->people->follow($task->get('data.pk'));
            $t = \InstagramController::follow($Instagram, $task->get('data.pk'));
            var_dump($t);
            $task->set('status', 'success');
            $task->set('worked_date', date("Y-m-d H:i:s"));
            $task->save();
            var_dump('yes');
            return true;
        }catch (InstagramAPI\Exception\ThrottledException $e) {
            var_dump('trotlet');
            return 'trotlet';
        }catch (InstagramAPI\Exception\NotFoundException $e){
            $task->set('status', 'error');
            $task->set('worked_date', date("Y-m-d H:i:s"));
            $task->save();
        } catch (\Exception $e) {
            // Couldn't get instagram feed related to the user
            echo '<pre>';
            var_dump($e->getCode(), $e->getMessage());
            echo 'follow ';
            echo ' task_id = ' . $task->get('id');
            echo '</pre>';
            return false;
        }
    }
    private function direct($task){
     var_dump('direct');
        $Account = \Controller::model("Account", $task->get("account_id"));
        try {
            $Instagram = InstagramController::login($Account);
        }catch (Exception $exception){
            return false;
        }
        try {

            //$t = $Instagram->people->follow($task->get('data.pk'));
            //$t = \InstagramController::follow($Instagram, $task->get('data.pk'));
            var_dump($task->get('data.pk'));
            var_dump($task->get('data.message'));
            $t = $Instagram->direct->sendText(["users" => [$task->get('data.pk')]], $task->get('data.message'));
            $task->set('status', 'success');
            $task->set('worked_date', date("Y-m-d H:i:s"));
            $task->save();
            var_dump('yes');
            return true;
        }catch (InstagramAPI\Exception\ThrottledException $e) {
            var_dump('trotlet');
            return 'trotlet';
        }catch (\Exception $e) {
            // Couldn't get instagram feed related to the user
            echo '<pre>';
            var_dump($e->getCode(), $e->getMessage());
            echo 'direct ';
            echo ' task_id = ' . $task->get('id');
            echo '</pre>';
            return false;
        }
    }
}