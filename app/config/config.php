<?php 
require_once APPPATH.'/config/common.config.php'; // Common configuration
require_once APPPATH.'/config/db.config.php'; // Database configuration
require_once APPPATH.'/config/i18n.config.php'; // i18n configuration

// ASCII Secure random crypto key
define("CRYPTO_KEY", "def0000035e3e44d7531d66d7890e36c6d2cf6c4b6919d5d218d68c3d9870338488ddaf505d8b4f25dfee11d4f985400dfa006c07a9bb31c16ded90bc7e9838839edc9ab");

// General purpose salt
define("NP_SALT", "rloKbTRjk1GIBCqJ");


// Path to instagram sessions directory
define("SESSIONS_PATH", APPPATH . "/sessions");
// Path to temporary files directory
define("TEMP_PATH", ROOTPATH . "/assets/uploads/temp");


// Path to themes directory
define("THEMES_PATH", ROOTPATH . "/inc/themes");
// URI of themes directory
define("THEMES_URL", APPURL . "/inc/themes");


// Path to plugins directory
define("PLUGINS_PATH", ROOTPATH . "/inc/plugins");
// URI of plugins directory
define("PLUGINS_URL", APPURL . "/inc/plugins");

// Path to ffmpeg binary executable
// NULL means it's been installed on global path
// If you set the value other than null, then it will only be 
// validated during posting the videos
define("FFMPEGBIN", '/var/www/admin/data/www/iwobox.com/bin/ffmpeg');

// Path to ffprobe binary executable
// NULL means it's been installed on global path
// If you set the value other than null, then it will only be 
// validated during posting the videos
define("FFPROBEBIN", '/var/www/admin/data/www/iwobox.com/bin/ffprobe');




/*
@exec('ffprobe -version 2>&1', $output, $statusCode);


echo '<pre>';
var_dump($statusCode);
var_dump($output);
echo '</pre>';
*/