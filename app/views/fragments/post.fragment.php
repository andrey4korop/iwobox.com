        <?php 
            $media_ids = [];

            if ($Post->get("media_ids")) {
                $ids = explode(",", $Post->get("media_ids"));
                foreach ($ids as $id) {
                    $id = (int)$id;
                    if ($id > 0) {
                        $media_ids[] = $id;
                    }
                }
            }
        ?>

        <div class='skeleton' id="post">
            <form action="javascript:void(0)" 
                  data-url="<?= APPURL."/post" ?>"
                  data-post-id="<?= $Post->get("id") ?>"
                  autocomplete="off">

                <input type="hidden" name="media-ids" value="<?= implode(",", $media_ids) ?>">

                <div class="container-1200">
                    <div class="row">
                        <?php if ($Post->get("status") == "failed"): ?>
                            <div class="prev-fail-note">
                                <div class="title"><?= __("This post has been failed to publish previously because of the following reason:") ?></div>
                                <div class="error"><?= $Post->get("data") ?></div>
                            </div>
                        <?php endif ?>

                        <div class="types clearfix">
                            <?php 
                                $allowed = [
                                    "timeline" => [],
                                    "story" => [],
                                    "album" => [],
                                ];

                                $types = $AuthUser->get("settings.post_types");
                                foreach ($types as $key => $val) {
                                    if ($val) {
                                        $p = explode("_", $key, 2);
                                        if (isset($allowed[$p[0]])) {
                                            if ($p[1] == "video") {
                                                if ($isVideoExtenstionsLoaded) {
                                                    $allowed[$p[0]][] = __("Video");
                                                }
                                            } else {
                                                $allowed[$p[0]][] = __("Photo");
                                            }
                                        }
                                    }
                                }

                                $type = $Post->isAvailable() ? $Post->get("type") : null; 
                            ?>

                            <label>
                                <input name="type" value="timeline" type="radio" 
                                       <?= $type=="timeline" ? "checked" : "" ?>
                                       <?= empty($allowed["timeline"]) ? "disabled" : "" ?>>
                                <div>
                                    <span class="sli sli-camera icon"></span>

                                    <div class="type">
                                        <div class="name">
                                            <span class="hide-on-small-only"><?= __("Add Post") ?></span>
                                            <span class="hide-on-medium-and-up"><?= __("Post") ?></span>
                                        </div>
                                        <div>
                                            <?= empty($allowed["timeline"]) ? 
                                                __("Photo") ." / ". __("Video") : 
                                                implode(" / ", $allowed["timeline"]) ?>    
                                        </div>
                                    </div>
                                </div>
                            </label>
                            <label>
                                <input name="type" value="mozaika" type="radio"
                                    <?= $type=="mozaika" ? "checked" : "" ?>>
                                <div>
                                    <span class="fas fa-th icon"></span>

                                    <div class="type">
                                        <div class="name">
                                            <span class="hide-on-small-only">Создать мозаику</span>
                                            <span class="hide-on-medium-and-up"><?= __("Post") ?></span>
                                        </div>
                                        <div>
                                            <?= empty($allowed["mozaika"]) ?
                                                __("Photo")  :
                                                implode(" / ", $allowed["mozaika"]) ?>
                                        </div>
                                    </div>
                                </div>
                            </label>
                            <label>
                                <input name="type" type="radio" value="story" 
                                       <?= $type=="story" ? "checked" : "" ?>
                                       <?= empty($allowed["story"]) ? "disabled" : "" ?>>
                                <div>
                                    <span class="sli sli-plus icon"></span>

                                    <div class="type">
                                        <div class="name">
                                            <span class="hide-on-small-only"><?= __("Add Story") ?></span>
                                            <span class="hide-on-medium-and-up"><?= __("Story") ?></span>    
                                        </div>
                                        <div>
                                            <?= empty($allowed["story"]) ? 
                                                __("Photo") ." / ". __("Video") : 
                                                implode(" / ", $allowed["story"]) ?>    
                                        </div>
                                    </div>
                                </div>
                            </label>

                            <label>
                                <input name="type" type="radio" value="album" 
                                      <?= $type=="album" ? "checked" : "" ?>
                                      <?= empty($allowed["album"]) ? "disabled" : "" ?>>
                                <div>
                                    <span class="sli sli-layers icon"></span>

                                    <div class="type">
                                        <div class="name">
                                            <span class="hide-on-small-only"><?= __("Add Album") ?></span>
                                            <span class="hide-on-medium-and-up"><?= __("Album") ?></span>        
                                        </div>
                                        
                                        <div>
                                            <?= empty($allowed["album"]) ? 
                                                __("Photo") ." / ". __("Video") : 
                                                implode(" / ", $allowed["album"]) ?>    
                                        </div>
                                    </div>
                                </div>
                            </label>
                        </div>

                        <div class="clearfix">
                            <div class="col s12 m6 l4">
                                <div class="hide-on-medium-and-up mobile-uploader">
                                    <a href="javascript:void(0)" class="uploadImg fluid button button--dark">
                                        <span class="sli sli-cloud-upload fz-18 mr-10" style="vertical-align: -3px"></span>
                                        <?= __("Pick a file from your device") ?>
                                    </a>

                                    <div class="result"></div>
                                </div>

                                <section class="section hide-on-small-only">
                                    <div class="section-header clearfix">
                                        <h2 class="section-title"><?= __("Media") ?></h2>

                                        <div class="section-actions clearfix">
                                           <!-- <a class="mdi mdi-laptop icon tippy js-fm-filebrowser"
                                               data-size="small"
                                               href="javascript:void(0)"
                                               title="<?= __("Your PC") ?>"></a>-->
                                            <a class="mdi mdi-laptop icon tippy uploadImg"
                                               data-size="small"
                                               href="javascript:void(0)"
                                               title="<?= __("Your PC") ?>"></a>
                                            <input type="file" name="uploadImg" id="inputImage" style="display: none">
                                            <a class="mdi mdi-link-variant icon tippy js-fm-urlformtoggler"
                                               data-size="small"
                                               href="javascript:void(0)"
                                               title="<?= __("URL") ?>"></a>

                                            <?php if ($Integrations->get("data.dropbox.api_key") && $AuthUser->get("settings.file_pickers.dropbox")): ?>
                                                <a class="mdi mdi-dropbox icon cloud-picker tippy"
                                                   data-size="small"
                                                   data-service="dropbox"
                                                   href="javascript:void(0)"
                                                   title="<?= __("Dropbox") ?>"></a>
                                            <?php endif; ?>

                                            <?php if (SSL_ENABLED && $Integrations->get("data.onedrive.client_id") && $AuthUser->get("settings.file_pickers.onedrive")): ?>
                                                <a class="mdi mdi-onedrive icon cloud-picker tippy"
                                                   data-size="small"
                                                   data-service="onedrive" 
                                                   data-client-id="<?= htmlchars($Integrations->get("data.onedrive.client_id")) ?>"
                                                   href="javascript:void(0)"
                                                   title="<?= __("OneDrive") ?>"></a>
                                            <?php endif; ?>

                                            <?php if ($Integrations->get("data.google.api_key") && $Integrations->get("data.google.client_id") && $AuthUser->get("settings.file_pickers.google_drive")): ?>
                                                <a class="mdi mdi-google-drive icon cloud-picker tippy"
                                                   data-size="small"
                                                   data-service="google-drive"
                                                   data-api-key="<?= htmlchars($Integrations->get("data.google.api_key")) ?>"
                                                   data-client-id="<?= htmlchars($Integrations->get("data.google.client_id")) ?>"
                                                   href="javascript:void(0)"
                                                   title="<?= __("Google Drive") ?>"></a>
                                            <?php endif; ?>

                                            <div class="more clearfix">
                                                <a class="mdi mdi-delete icon tippy js-fm-delete-mode" 
                                                   data-size="small"
                                                   href="javascript:void(0)"
                                                   title="<?= __("Delete Mode") ?>"></a>

                                                <a class="mdi mdi-information-outline icon tippy js-fm-infobox" 
                                                   data-size="small"
                                                   href="javascript:void(0)"
                                                   title="<?= __("Info") ?>"></a>
                                            </div>
                                        </div>
                                    </div>

                                    <div>
                                        <div id="filemanager" 
                                             data-connector-url="<?= APPURL."/file-manager/connector" ?>"
                                             data-maxselect="10"
                                             data-selected-file-ids="[<?= implode(",", $media_ids) ?>]"
                                             data-img-assets-url="<?= APPURL."/assets/img/" ?>"
                                             style="height: 538px"></div>
                                    </div>
                                </section>
                            </div>

                            <div class="col s12 m6 m-last l4">
                                <section class="section">
                                    <div class="section-header clearfix hide-on-small-only">
                                        <h2 class="section-title"><?= __($Post->isAvailable() ? 'Edit Post' : 'New Post') ?></h2>


                                    </div>

                                    <div class="section-content controls" style="min-height: 429px;">
                                        <div class="form-result"></div>

                                        <div class="mini-preview droppable">
                                            <div class="items clearfix">
                                            </div>

                                            <div class="drophere">
                                                <span class="none"><?= __("Drop here") ?></span>
                                                <span><?= __("Drag media here to post") ?></span>
                                            </div>
                                        </div>

                                        <div class="tabs mb-20 ">
                                            <div class="tabheads clearfix">
                                                <a class="active" href="javascript:void(0)" style="width: 50%; border-bottom: none;" data-tab="1"><?= __("Caption") ?></a>
                                                <a href="javascript:void(0)" style="width: 50%; border-bottom: none;" data-tab="2"><?= __("First Comment") ?></a>
                                            </div>

                                            <div class="tabcontents">
                                                <div class="active pos-r" data-tab="1">
                                                    <?php 
                                                        $caption = $Post->get("caption");
                                                        if (!$Post->isAvailable()) {
                                                            $CaptionModel = Controller::model("Caption", Input::get("caption"));

                                                            if ($CaptionModel->isAvailable() && 
                                                                $CaptionModel->get("user_id") == $AuthUser->get("id")) {
                                                                $caption = $CaptionModel->get("caption");
                                                            }
                                                        }
                                                    ?>

                                                    <div class="caption input <?= get_option("np_search_in_caption") ? "js-search-enabled" : "" ?>" 
                                                         data-placeholder="<?= __("Write a caption") ?>"
                                                         contenteditable="true"><?= htmlchars($caption) ?></div>

                                                    <a class="sli sli-grid caption-picker js-open-popup" href="<?= APPURL."/captions" ?>" data-popup="#captions-overlay"></a>
                                                </div>

                                                <div class="pos-r" data-tab="2">
                                                    <div class="first-comment input <?= get_option("np_search_in_caption") ? "js-search-enabled" : "" ?>" 
                                                         data-placeholder="<?= __("Write the first comment") ?>"
                                                         contenteditable="true"><?= htmlchars($Post->get("first_comment")) ?></div>

                                                    <a class="sli sli-grid caption-picker js-open-popup" href="<?= APPURL."/captions" ?>" data-popup="#captions-overlay"></a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="search-results none"></div>

                                        <div class="mb-20">
                                            <?php 
                                                $is_scheduled = (bool)$Post->get("is_scheduled");

                                                $timezone = new DateTimeZone($AuthUser->get("preferences.timezone"));

                                                $now = new DateTime();
                                                $now->setTimezone($timezone);

                                                if (!$Post->isAvailable() && 
                                                    isValidDate(Input::get("date"), "Y-m-d") &&
                                                    Input::get("date") >= $now->format("Y-m-d")) {
                                                    $date = new DateTime(Input::get("date")." ".$now->format("H:i"), $timezone);
                                                    $is_scheduled = true;
                                                } else {
                                                    $date = new DateTime($is_scheduled ? $Post->get("schedule_date") : "now");
                                                    $date->setTimezone($timezone);
                                                }
                                            ?>
                                            <label>
                                                <input type="checkbox" 
                                                       class="checkbox" 
                                                       name="schedule" 
                                                       value="1" 
                                                       <?= $is_scheduled ? "checked" : "" ?>>
                                                <span style="margin-left:14px;">
                                                    <span class="icon unchecked">
                                                        <span class="mdi mdi-check"></span>
                                                    </span>
                                                    <?= __('Schedule') ?>
                                                </span>
                                            </label>
                                        </div>
                                       <!-- <div class="mb-20">
                                            <?php
                                            $replay = (bool)$Post->get("replay");
                                            if (!$Post->isAvailable()){
                                                $replay = false;
                                            }
                                            ?>
                                            <label>
                                                <input type="checkbox"
                                                       class="checkbox"
                                                       name="replay"
                                                       value="1"
                                                    <?= $replay ? "checked" : "" ?>>
                                                <span style="margin-left:14px;">
                                                    <span class="icon unchecked">
                                                        <span class="mdi mdi-check"></span>
                                                    </span>
                                                    <?= __('Повторять каждый день') ?>
                                                </span>
                                            </label>
                                        </div>-->
                                        <div class="mb-20">
                                            <?php
                                            $replay = (bool)$Post->get("replay");
                                            if (!$Post->isAvailable()){
                                                $replay = false;
                                            }
                                            ?>
                                            <label>
                                                <select
                                                       class="input"
                                                       name="replay">
                                                    <option  <?= $replay == 0 ? "selected" : "" ?> value="0">Не повторять</option>
                                                    <option  <?= $replay == 1 ? "selected" : "" ?> value="1">Повторять каждый день</option>
                                                    <option  <?= $replay == 2 ? "selected" : "" ?> value="2">Повторять каждую неделю</option>
                                                </select>
                                            </label>
                                        </div>
                                        <?php 
                                            $dateformat = $AuthUser->get("preferences.dateformat");
                                            $timeformat = $AuthUser->get("preferences.timeformat") == "24" ? "H:i" : "h:i A";
                                            $format = $dateformat." ".$timeformat;
                                        ?>

                                        <div class="pos-r mb-20">
                                            <input class="input leftpad js-datepicker" 
                                                   name="schedule-date" 
                                                   data-position="top left"
                                                   data-date-format="<?= str_replace(["Y", "m", "d", "F"], ["yyyy", "mm", "dd", "MM"], $dateformat) ?>"
                                                   data-time-format="<?= str_replace(["h:i", "H:i", "A"], ["hh:ii", "hh:ii", "AA"], $timeformat) ?>"
                                                   data-min-date="<?= $now->format("c") ?>"
                                                   data-start-date="<?= $date->format("c") ?>"
                                                   data-user-datetime-format="<?= $format ?>"
                                                   type="text" 
                                                   value="<?= $date->format($format); ?>"
                                                   readonly>
                                            <span class="sli sli-calendar field-icon--left pe-none"></span>
                                        </div>
                                        <div class="section-actions">
                                            <?php if ($Accounts->getTotalCount() > 0): ?>
                                                <div class="dropdown onprogress">
                                                    <span class="sli sli-social-instagram icon pe-none"></span>
                                                    <img class="loading pe-none" src="<?= APPURL."/assets/img/round-loading.svg" ?>" alt="loading">

                                                    <select name="account" disabled>
                                                        <?php
                                                        $selected = 0;
                                                        if ($Post->isAvailable()) {
                                                            $selected = $Post->get("account_id");
                                                        } else if ((int)Input::get("account") > 0) {
                                                            $selected = (int)Input::get("account");
                                                        }
                                                        ?>
                                                        <?php foreach ($Accounts->getDataAs("Account") as $a): ?>
                                                            <option value="<?= $a->get("id") ?>" <?= $a->get("id") == $selected ? "selected" : "" ?>><?= $a->get("username") ?></option>
                                                        <?php endforeach; ?>
                                                    </select>

                                                    <span class="mdi mdi-menu-down caret"></span>
                                                </div>
                                            <?php else: ?>
                                                <a href="<?= APPURL."/accounts/new" ?>" class="btn">
                                                    <span class="sli sli-user-follow fz-14 mr-5"></span>
                                                    <?= __("Add account") ?>
                                                </a>
                                            <?php endif ?>
                                        </div>
                                        <div class="mb-5">
                                            <a href="javascript:void(0)" class="advanced-settings-toggler">
                                                <?= __("Advanced Settings") ?>
                                                <span class="mdi mdi-menu-down"></span>
                                            </a>
                                        </div>


                                        <div class="advanced-settings">
                                            <div class="mb-20">
                                                <div class="pos-r">
                                                    <input type="hidden" name="location" value="<?= htmlchars($Post->get("location.object")) ?>">
                                                    <input class="input leftpad rightpad" 
                                                           name="location-search" 
                                                           type="text" 
                                                           placeholder="<?= __("Location") ?>"
                                                           value="<?= htmlchars($Post->get("location.label")); ?>">
                                                    <a href="javascript:void(0)" class="js-enable-location-search mdi mdi-close-circle field-icon--right none"></a>
                                                    <span class="sli sli-location-pin field-icon--left pe-none"></span>
                                                </div>
                                            </div>

                                            <div>
                                                <label>
                                                    <input type="checkbox" 
                                                           class="checkbox" 
                                                           name="remove-media" 
                                                           value="1" 
                                                           <?= $Post->get("remove-media") ? "checked" : "" ?>>
                                                    <span style="margin-left:14px;">
                                                        <span class="icon unchecked">
                                                            <span class="mdi mdi-check"></span>
                                                        </span>
                                                        <?= __('Auto remove media') ?>

                                                        <span class="tooltip tippy" 
                                                              data-position="top"
                                                              data-size="small"
                                                              title="<?= __('Remove media files after posting') ?>">
                                                              <span class="mdi mdi-help-circle"></span>    
                                                          </span>
                                                    </span>
                                                </label>
                                            </div>
                                        </div>

                                        <p class="account-error none"></p>
                                    </div>

                                    <div class="post-submit">
                                        <input class="fluid large button"
                                               data-value-now="<?= __("Post now") ?>"
                                               data-value-schedule="<?= __("Schedule the post") ?>"
                                               type="submit" 
                                               value="<?= __($Post->get("is_scheduled") ? "Schedule the post" : "Post now") ?>"
                                               disabled>
                                    </div>
                                </section>
                            </div>

                            <div class="col s12 m6 l4 l-last hide-on-medium-and-down">
                                <section class="section">
                                    <div class="post-preview" data-type="timeline">
                                        <div class="preview-header">
                                            <img src="<?= APPURL."/assets/img/instagram-logo.png" ?>" alt="Instagram">
                                        </div>

                                        <div class="preview-account clearfix">
                                            <span class="circle"></span>
                                            <span class="lines">
                                                <span class="line-placeholder" style="width: 47.76%"></span>
                                                <span class="line-placeholder" style="width: 29.85%"></span>
                                            </span>
                                        </div>

                                        <div class="preview-media--timeline">
                                            <div class="placeholder"></div>
                                            <!-- <video src="#" playsinline autoplay muted loop></video> -->    
                                        </div>

                                        <div class="preview-media--story">
                                            <!-- <div class="img"></div> -->
                                            <!-- <video src="#" playsinline autoplay muted loop></video> -->    
                                        </div>
                                        <div class="story-placeholder"></div>

                                        <div class="preview-media--album">
                                            <!-- <div class="img"></div> -->
                                            <!-- <video src="http://demo.thepostcode.co/nextpost/assets/uploads/1/instagram/19026330_428324574201218_2358753720650432512_n.mp4" playsinline autoplay muted loop class="video-preview"></video> -->    
                                        </div>

                                        <div class="preview-caption-wrapper">
                                            <div class="preview-caption-placeholder" style="<?= $caption ? "display:none" : "" ?>">
                                                <span class="line-placeholder"></span>
                                                <span class="line-placeholder" style="width: 61.19%"></span>
                                            </div>

                                            <div class="preview-caption" style="<?= $caption ? "display:block" : "" ?>"><?= htmlchars($caption) ?></div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <style>
            #modalCrop{
                position: fixed;
                top:0;
                left: 0;
                z-index: -1000;
                width: 100%;
                height: 100vh;
                background: rgba(255,255,255,0.5);
                display: none;
            }
            #modalCrop.on{
                display: flex;
                z-index: 1000;
            }
            .modalCropContainer{
                margin: auto;
                width: 80%;
                position: relative;
                background: #fff;
                padding: 1%;
                border: 1px solid #ccc;
                max-height: 80%;
                -webkit-box-shadow: 0px 0px 50px 0px rgba(102,102,102,1);
                -moz-box-shadow: 0px 0px 50px 0px rgba(102,102,102,1);
                box-shadow: 0px 0px 50px 0px rgba(102,102,102,1);
            }
            .close{
                font-size: 30px;
                position: absolute;
                right: -15px;
                color: #212121;
                top: -15px;
                background: rgba(255,255,255,0.9);
                border-radius: 50%;
                transition: all 0.2s linear;
            }
            .close:hover, .close:active{
                color: #3b7cff;
                text-shadow: 0px 0px 10px rgba(59, 124, 255, 0.6);
            }
            .img-container{
                max-height:  60vh;
            }
            #modalCrop .docs-buttons{
                display: flex;
                margin-top: 5px;
                flex-wrap: wrap;
            }
            #modalCrop .btn-group{
                display: flex;
                margin-right: 5px;
            }
            #modalCrop button{
                background: #fff;
                margin: 0;
                border: none;
                color: #212121;
                padding: 9px;
                font-weight: 600;
                transition: all 0.2s linear;
            }
            #modalCrop button.crop_btn{
                background-color: rgba(59, 124, 255, 1);
                color: #fff;
            }
            #modalCrop button:hover, #modalCrop button:active{
                background-color: rgba(59, 124, 255, 0.2);
                color: #3b7cff;
            }
            #modalCrop .er{
                display: none;
                color: red;
                margin: 5px;
            }

        </style>
<div id="modalCrop">
    <div class="modalCropContainer">
        <i class="far fa-times-circle close"></i>
        <div class="container">
        <div class="row">
            <div class="col-md-9">
                <!-- <h3>Demo:</h3> -->
                <div class="img-container">
                    <img src="" alt="Picture" class="cropper-hidden" style="max-width: 100%; max-height: 100%">
                </div>
            </div>

        </div>
        <div class="row" id="actions">
            <div class="col-md-9 docs-buttons">

                <div class="btn-group btn-group-crop">
                    <button type="button" class="crop_btn">
                        <span>CROP</span>
                    </button>

                </div>
                <!--DragMode-->
                <div class="btn-group">
                    <button type="button" class="dragMode_btn" data-value="move">
                        <span class="fas fa-arrows-alt"></span>
                    </button>
                    <button type="button" class="dragMode_btn" data-value="crop" >
                        <span class="fa fa-crop"></span>
                    </button>
                </div>

                <div class="btn-group">
                    <button type="button" class="zoom_btn" data-value="0.1">
                         <span class="fa fa-search-plus"></span>
                    </button>
                    <button type="button" class="zoom_btn" data-value="-0.1">
                         <span class="fa fa-search-minus"></span>
                       </button>
                </div>

                <div class="btn-group">
                    <button type="button" class="rotate_btn" data-value="-45">
                          <span class="fas fa-undo-alt"></span>
                    </button>
                    <button type="button" class="rotate_btn" data-value="45">
                        <span class="fas fa-redo-alt"></span>
                    </button>
                </div>

                <div class="btn-group">
                    <button type="button" class="scale_btn" data-method="scaleX" data-value="-1">
                          <span class="fas fa-arrows-alt-h"></span>
                    </button>
                    <button type="button" class="scale_btn" data-method="scaleY" data-value="-1">
                        <span class="fas fa-arrows-alt-v"></span>
                    </button>
                </div>

                <div class="btn-group">
                    <button type="button" class="reset_btn">
                        <span class="fas fa-sync-alt"></span>
                    </button>

                </div>


                <div class="btn-group d-flex flex-nowrap AspectRatio_btns" data-toggle="buttons">
                    <button type="button" class="AspectRatio_btn" data-value="1.77777">
                        <span>16:9</span>
                    </button>
                    <button type="button" class="AspectRatio_btn" data-value="1.33333">
                        <span>4:3</span>
                    </button>
                    <button type="button" class="AspectRatio_btn" data-value="1">
                        <span>1:1</span>
                    </button>
                    <button type="button" class="AspectRatio_btn" data-value="0.66666">
                        <span>2:3</span>
                    </button>
                    <button type="button" class="AspectRatio_btn" data-value="NaN">
                        <span>Free</span>
                    </button>

                </div>
                <div class="btn-group d-flex flex-nowrap AspectRatio_btnM" data-toggle="buttons">
                    <button type="button" class="AspectRatio_btn" data-value="3">
                        <span>3x1</span>
                    </button>
                    <button type="button" class="AspectRatio_btn" data-value="1.5">
                        <span>3x2</span>
                    </button>
                    <button type="button" class="AspectRatio_btn" data-value="1">
                        <span>3x3</span>
                    </button>
                    <button type="button" class="AspectRatio_btn" data-value="0.75">
                        <span>3x4</span>
                    </button>
                </div>
                <div class="btn-group d-flex flex-nowrap AspectRatio_btnG" data-toggle="buttons">
                    <button type="button" class="AspectRatio_btn" data-value="2">
                        <span>2x1</span>
                    </button>
                    <button type="button" class="AspectRatio_btn" data-value="3">
                        <span>3x1</span>
                    </button>
                    <button type="button" class="AspectRatio_btn" data-value="4">
                        <span>4x1</span>
                    </button>
                    <button type="button" class="AspectRatio_btn" data-value="5">
                        <span>5x1</span>
                    </button>
                    <button type="button" class="AspectRatio_btn" data-value="6">
                        <span>6x1</span>
                    </button>
                </div>
                <div class="btn-group d-flex flex-nowrap AspectRatio_btnT" data-toggle="buttons">
                    <button type="button" class="toggle_type" data-value="default">
                        <span>
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"  viewBox="0 0 512 512" xml:space="preserve" style="width: 20px;display: block;"><g><g><path d="M511.988,25.715l-27.096,10.282c-1.02,0.387-103.42,38.694-228.892,38.694S28.127,36.384,27.12,36.002L0,25.67v430.021 l0.027,30.528l27.995-12.207c0.878-0.384,89.479-38.321,227.979-38.321s227.1,37.938,227.952,38.31L512,486.33V54.692 L511.988,25.715z M152,402.519c-49.948,6.682-88.182,16.946-112,24.62V82.393c24.387,7.378,63.696,17.674,112,24.643V402.519z M322,398.415c-20.531-1.705-42.564-2.723-66-2.723c-22.673,0-44.042,0.949-64,2.554V111.743c20.337,1.846,41.784,2.949,64,2.949 c22.947,0,45.069-1.179,66-3.136V398.415z M472,427.139c-23.498-7.571-61.036-17.659-110-24.345v-296.05 c47.389-6.962,85.951-17.075,110-24.351V427.139z"/></g></g></svg>
                        </span>
                    </button>
                </div>
                <div class="btn-group d-flex flex-nowrap er">
                    Размер фрагмента изображения меньше 320px, виберите больший участок, или загрузите изображение большего разрешения
                </div>
            </div><!-- /.docs-buttons -->
        </div>

    </div>
    </div>
</div>
        <?php if ($Captions->getTotalCount() > 0): ?>
            <div class="overlay none js-popup" id="captions-overlay">
                <a href="javascript:void(0)" class="close-btn mdi mdi-close js-close-popup"></a>
                <div class="content">
                    <h2 class="overlay-title"><?= __("Caption Templates") ?></h2>
                    <div class="simple-list js-loadmore-content" data-loadmore-id="1">
                        <?php $Emojione = new \Emojione\Client(new \Emojione\Ruleset()); ?>
                        <?php foreach($Captions->getDataAs("Caption") as $c): ?>
                            <div class="simple-list-item">
                                <h3 class="title"><?= htmlchars($c->get("title")) ?></h3>

                                <div class="info">
                                    <p>
                                        <?= $Emojione->shortnameToUnicode($c->get("caption")); ?>
                                    </p>
                                </div>

                                <input type="hidden" name="capion-<?= $c->get("id") ?>" value="<?= htmlchars($Emojione->shortnameToUnicode($c->get("caption"))); ?>">

                                <a href="javascript:void(0)" class="full-link"></a>
                            </div>
                        <?php endforeach; ?>
                    </div>

                    <?php if($Captions->getPage() < $Captions->getPageCount()): ?>
                        <div class="loadmore mt-40">
                            <?php 
                                $url = parse_url($_SERVER["REQUEST_URI"]);
                                $path = $url["path"];
                                if(isset($url["query"])){
                                    $qs = parse_str($url["query"],$qsarray);
                                    unset($qsarray["cp"]);

                                    $url = $path."?".(count($qsarray) > 0 ? http_build_query($qsarray)."&" : "")."cp=";
                                }else{
                                    $url = $path."?cp=";
                                }
                            ?>
                            <a class="fluid button button--light-outline js-loadmore-btn" data-loadmore-id="1" href="<?= $url.($Captions->getPage()+1) ?>">
                                <span class="icon sli sli-refresh"></span>
                                <?= __("Load More") ?>
                            </a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        <?php endif ?>