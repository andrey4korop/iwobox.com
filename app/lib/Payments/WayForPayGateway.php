<?php 
namespace Payments;

use Payments\WayForPay;
use Carbon\Carbon;

/**
 * Paypal Payment Gateway
 */
class WayForPayGateway extends AbstractGateway
{   
    public $wayForPay;
    private $_merchant_account = 'iwobox_com'; //Merchant Account
    private $_merchant_password = '1ecade01f602c12196fc0d4c0fb013a07a8a05d0'; //Your unique sign
    private $_merchant_domain = 'iwobox.com'; //Domain name
    private $authCode = "541963"; //authorization code

    public function __construct()
    {
        $this->wayForPay = new WayForPay($this->_merchant_account,$this->_merchant_password);
    }

    /**
     * Place Order
     *
     * Generate payment page url here and return it
     * @return string URL of the payment page
     */
    public function placeOrder($params = [])
    {
        $Order = $this->getOrder();
        if (!$Order) {
            throw new \Exception('Set order before calling AbstractGateway::placeOrder()');
        }

        if ($Order->get("status") != "payment_processing") {
            throw new \Exception('Order status must be payment_processing to place it');
        }

        // Check if the order currency is zero decimal currency
        $iszdc = isZeroDecimalCurrency($Order->get("currency"));
        $orderNum = uniqid(); //Unique ID generation
        $orderNum = "ZAKAZ_" . $orderNum;

        $fields = array(
            "merchantAccount" => $this->_merchant_account,
            "merchantDomainName" => $this->_merchant_domain,
            "orderReference" => $orderNum,
            "merchantSignature" => $this->_merchant_password,
            "orderDate" => Carbon::now()->format('Ymd'),
            "productName" => json_decode($Order->get('data'))->package->title,
            "productCount" => "1",
            "productPrice" => $Order->get('total'),
            "merchantTransactionSecureType" => "AUTO",
            "amount" => $Order->get('total'),
            "currency" => $Order->get('currency'),
            //"authCode" => $authCode,
            "returnUrl" => APPURL."/checkout/".$Order->get("id").".".sha1($Order->get("id").NP_SALT)."?paymentId=".$orderNum,
            "email" => $this->getUser()->get('email'),
            "phone" => "380501234567",
            "createdDate" => Carbon::now()->format('Ymd'),
        );

            $url = $this->wayForPay->generatePurchaseUrl($fields);


        return $url;
    }


    /**
     * Payment callback
     * @return boolean [description]
     */
    public function callback($params = [])
    {
        if (empty($params["paymentId"])) {
            throw new \Exception(__('System detected logical error').": invalid_payment_id");
        }

        $paymentId = $params["paymentId"];

        $Order = $this->getOrder();
        if (!$Order) {
            throw new \Exception('Set order before calling AbstractGateway::placeOrder()');
        }

        try {
            $fields = array(
                "merchantAccount" => $this->_merchant_account,
                "orderReference" => $params["paymentId"],
                "merchantSignature" => $this->_merchant_password,
                "orderDate" => Carbon::now()->format('Ymd'),
                "apiVersion" => "1"
            );

            $payment_custom_data = $this->wayForPay->checkStatus($fields);
        } catch (\Exception $e) {
            throw new \Exception(__("Couldn't get payment information"));
        }

/*
        if ($payment_custom_data->orderReference != $Order->get("id")) {
            throw new \Exception(__('System detected logical error').": invalid_order_id");
        }*/


        if (!$Order->get("payment_id")) {
            // If payment_id is not empty
            // then it means that, order is already completed


            if($payment_custom_data['reasonCode'] != "1100") {
                // Couldn't pay, removing order...
                $Order->remove();
                throw new \Exception($payment_custom_data['reason']);
            }


            if ($Order->get("status") == "payment_processing") {
                $Order->finishProcessing();

                // Updating order...
                $Order->set("status","paid")
                      ->set("payment_id", $payment_custom_data['orderReference'])
                      ->set("paid", $payment_custom_data['amount'])
                      ->update();

                try {
                    // Send notification emails to admins
                    \Email::sendNotification("new-payment", ["order" => $Order]);
                } catch (\Exception $e) {
                    // Failed to send notification email to admins
                    // Do nothing here, it's not critical error
                }
            }
        }

        return true;
    }
}
