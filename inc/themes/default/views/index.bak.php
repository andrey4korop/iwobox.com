<?php if (!defined('APP_VERSION')) die("Yo, what's up?"); ?>
<!DOCTYPE html>
<html lang="<?= ACTIVE_LANG ?>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta name="theme-color" content="#fff">

        <meta name="description" content="<?= site_settings("site_description") ?>">
        <meta name="keywords" content="<?= site_settings("site_keywords") ?>">

        <link rel="icon" href="<?= site_settings("logomark") ? site_settings("logomark") : APPURL."/assets/img/logomark.png" ?>" type="image/x-icon">
        <link rel="shortcut icon" href="<?= site_settings("logomark") ? site_settings("logomark") : APPURL."/assets/img/logomark.png" ?>" type="image/x-icon">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.3.5/cropper.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.3.5/cropper.min.css" />
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

        <link rel="stylesheet" type="text/css" href="<?= active_theme("url")."/assets/css/plugins.css?v=".VERSION ?>">
        <link rel="stylesheet" type="text/css" href="<?= active_theme("url")."/assets/css/core.css?v=".VERSION ?>">
        <meta name="msvalidate.01" content="1375CF06D08B2F0D2503A431783E2AC5" />

        <title><?= site_settings("site_name") ?></title>
        <style>
            .slick-slide .sm {
                display: none;
            }
            .package-list-item .best{display: none}
            .package-list-item:nth-child(2) .best{
                display: block;
                position: absolute;
                margin: auto;
                width: 100%;
                box-sizing: border-box;
                padding: 10px;
                border-radius: 5px;
                background-image: linear-gradient(to left top, #ff1776, #ff3c67, #ff6d50, #ff824a);
                color: #fff;
                font-weight: 500;
                letter-spacing: 3px;
            }
            .package-list-item{
                position: relative;
            }
            @media screen and (max-width: 768px){
                .slick-slide .sm {
                    display: none;
                }

            }
        </style>
    </head>

    <body>
        <header>
            <div class="container-1100">
                <div class="row clearfix">
                    <a href="<?= APPURL ?>" class="header-logo">
                        <img src="<?= site_settings("logotype") ? site_settings("logotype") : APPURL."/assets/img/logo.png" ?>" 
                             alt="<?= site_settings("site_name") ?>">
                    </a>
                    <div class="menu-toggle">
                        <div class="trigger"></div>
                    </div>
                    <?php if (!$AuthUser): ?>
                        <div class="language dropdown dropdown-click">
                            <div class="dropdown-list">
                                <ul class="language-list">
                                    <?php foreach (Config::get("applangs") as $l): ?>
                                        <li data-value="<?= $l["code"] ?>">
                                            <a href="#" data-value="<?= $l["code"] ?>" ><?= $l["localname"] ?></a>
                                        </li>
                                        <?php
                                        if($l["code"] == ACTIVE_LANG){
                                            $hh = $l["localname"];
                                        }
                                        ?>
                                    <?php endforeach ?>
                                </ul>
                            </div>
                            <span class="active">
                                <?= $hh ?>
                                <i class="icon icon-arrow-down sli sli--arrow-down"></i>
                            </span>
                        </div>
                    <?php endif; ?>
                    <nav class="header-nav clearfix">
                        <ul class="clearfix menu">
                            <li><a href="<?= APPURL."/#about" ?>" class="scroll_btn"><?= __("About") ?></a></li>
                            <li><a href="<?= APPURL."/#features" ?>" class="scroll_btn"><?= __("Features") ?></a></li>
                            <li><a href="<?= APPURL."/#packages" ?>" class="scroll_btn"><?= __("Prices") ?></a></li>
                            <?php if ($AuthUser): ?>
                            <li> <a class="fw-600" href="<?= APPURL."/profile/" ?>"><?= __("Hi, %s", htmlchars($AuthUser->get("firstname"))) ?></a></li>
                            <?php else: ?>
                            <li><a href="<?= APPURL."/login" ?>"><?= __("Login") ?></a></li>
                            <li><a href="<?= APPURL."/signup" ?>" class="button button--small button--outline"><?= __("Sign up") ?></a></li>
                            <?php endif ?>
                        </ul>







                    </nav>

                </div>
            </div>
        </header>



        <div class="cover">
            <div class="lines">
                <span class="line" data-toward="horizontal"></span>
                <span class="line" data-toward="vertical"></span>
            </div>
            <div class="container-1100">
                <div class="row clearfix" style="margin: 3% 0;">
                    <h1 class="cover-title">
                        <!--<?= __("A Smarter way to <br> Auto Post on Instagram") ?>-->
                        IwoBox – мощный, удобный и самый простой сервис для максимального облегчения работы с Instagram.
                    </h1>

                    <p class="cover-summary">
                        <!--<?= __("Save time managing your Instagram accounts, publish and analyze all your posts in one panel.") ?>-->
                        Устали от сложных и однотипных задач?  Тогда делегируйте эти задачи  нам! Наш сервис работает без остановки, перерыва и выходных! Станьте вместе с IwoBox свободнее, а значит счастливее! Мы экономим Ваше время, а значит деньги!
                    </p>

                    <a href="<?= APPURL."/signup" ?>" class="button button--oval" style="background-image: linear-gradient(60deg, #4eb9a6,  #4eb9a6, #902eff,  #902eff);"><?= __("Отдохнуть"); ?> </a>

                    <?php if ($TrialPackage->get("data.size") > 0): ?>
                        <span class="trial">*
                            <?php 
                                $days = $TrialPackage->get("data.size"); 
                                if ($days > 45) {
                                    echo __("Free trial available");
                                } else {
                                    echo n__("%s day free trial", "%s days free trial", $days, $days);
                                }
                            ?>
                        </span>
                    <?php endif ?>
                </div>
            </div>
        </div>
        
        <div class="our_plus" id="about">
            <h1>Наши преимущества</h1>
            <div class="container-1100">
                <div class="text_block">
                    <h2>IwoBox</h2>
                    <p style="font-size: 1.1em;line-height: 1.5em">
                        Instagram не позволяет настроить выкладку постов без участия автора, именно поэтому сервис IwoBox выступает в роли будильника. Данный сервис позволяет заготавливать контент наперед. Вы можете создать посты с фотографиями, описанием и хештегами в Instagram (на день, на неделю, на месяц) и указать, когда они должны быть опубликованы. Сервис IwoBox предоставляет возможность работы с аудиторией конкурентов, лайками, комментариями и директом. Наш сервис позволяет загружать фотографии, используя алгоритм мозаики (3х3 и 3х4).
                    </p>
                </div>
                <div class="right_block">
                    <div class="wrapper_block">
                        <div class="item">
                            <div class="img"><img src="<?= active_theme_url() . "/assets/img/features/26.png" ?>" " alt=""></div>
                            <div class="text">Мозаика</div>
                        </div>
                        <div class="item">
                            <div class="img"><img src="<?= active_theme_url() . "/assets/img/features/10.png" ?>" " alt=""></div>
                            <div class="text">Адаптивность</div>
                        </div>
                        <div class="item">
                            <div class="img"><img src="<?= active_theme_url() . "/assets/img/features/24.png" ?>" " alt=""></div>
                            <div class="text">Auto Подписка</div>
                        </div>
                        <div class="item">
                            <div class="img"><img src="<?= active_theme_url() . "/assets/img/features/11.png" ?>" " alt=""></div>
                            <div class="text">Без установки на ПК</div>
                        </div>
                        <div class="item">
                            <div class="img"><img src="<?= active_theme_url() . "/assets/img/features/23.png" ?>" " alt=""></div>
                            <div class="text">Auto Отписка</div>
                        </div>
                        <div class="item">
                            <div class="img"><img src="<?= active_theme_url() . "/assets/img/features/12.png" ?>" " alt=""></div>
                            <div class="text">Интуитивно понятный интерфейс</div>
                        </div>
                        <div class="item">
                            <div class="img"><img src="<?= active_theme_url() . "/assets/img/features/22.png" ?>" " alt=""></div>
                            <div class="text">Auto Коментарии</div>
                        </div>
                        <div class="item">
                            <div class="img"><img src="<?= active_theme_url() . "/assets/img/features/13.png" ?>" " alt=""></div>
                            <div class="text">Продвижение нескольких аккаунтов</div>
                        </div>
                        <div class="item">
                            <div class="img"><img src="<?= active_theme_url() . "/assets/img/features/21.png" ?>" " alt=""></div>
                            <div class="text">Auto Лайки</div>
                        </div>
                        <div class="item">
                            <div class="img"><img src="<?= active_theme_url() . "/assets/img/features/14.png" ?>" " alt=""></div>
                            <div class="text">Безопасность</div>
                        </div>
                        <div class="item">
                            <div class="img"><img src="<?= active_theme_url() . "/assets/img/features/20.png" ?>" " alt=""></div>
                            <div class="text">Auto Репост</div>
                        </div>
                        <div class="item">
                            <div class="img"><img src="<?= active_theme_url() . "/assets/img/features/15.png" ?>" " alt=""></div>
                            <div class="text">Поддержка пользователей</div>
                        </div>
                        <div class="item">
                            <div class="img"><img src="<?= active_theme_url() . "/assets/img/features/19.png" ?>" " alt=""></div>
                            <div class="text">Рассылка в Директ</div>
                        </div>
                        <div class="item">
                            <div class="img"><img src="<?= active_theme_url() . "/assets/img/features/16.png" ?>" " alt=""></div>
                            <div class="text">AI Технологии</div>
                        </div>
                        <div class="item">
                            <div class="img"><img src="<?= active_theme_url() . "/assets/img/features/18.png" ?>" " alt=""></div>
                            <div class="text">Умный отложеный пост</div>
                        </div>
                        <div class="item">
                            <div class="img"><img src="<?= active_theme_url() . "/assets/img/features/17.png" ?>" " alt=""></div>
                            <div class="text">Настройка не более 10 минут</div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="img"><img src="<?= active_theme_url() . "/assets/img/features/25.png" ?>" " alt=""></div>
                        <div class="text">Будущие функции над которыми мы работаем</div>
                    </div>
                </div>
            </div>
        </div>

        <div id="features" class="section">
            <div class="container-1100">
                <div class="row">
                    <h2 class="text-c"><?= __("Here is features for you") ?></h2>

                    <div class="feature-list clearfix">
                        <div class="feature-list-item">
                            <div>
                                <div class="imgbox">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M462.3 62.6C407.5 15.9 326 24.3 275.7 76.2L256 96.5l-19.7-20.3C186.1 24.3 104.5 15.9 49.7 62.6c-62.8 53.6-66.1 149.8-9.9 207.9l193.5 199.8c12.5 12.9 32.8 12.9 45.3 0l193.5-199.8c56.3-58.1 53-154.3-9.8-207.9z"/></svg>
                                </div>

                                <span class="label"><?= __("Unique Design") ?></span>
                            </div>
                        </div>

                        <div class="feature-list-item">
                            <div>
                                <div class="imgbox">


                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 32C114.6 32 0 125.1 0 240c0 49.6 21.4 95 57 130.7C44.5 421.1 2.7 466 2.2 466.5c-2.2 2.3-2.8 5.7-1.5 8.7S4.8 480 8 480c66.3 0 116-31.8 140.6-51.4 32.7 12.3 69 19.4 107.4 19.4 141.4 0 256-93.1 256-208S397.4 32 256 32z"/></svg>

                                </div>

                                <span class="label"><?= __("Multi Accounts") ?></span>
                            </div>
                        </div>

                        <div class="feature-list-item">
                            <div>
                                <div class="imgbox">


                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 457.508 457.508" xml:space="preserve">
                                                <g>
                                                    <path d="M199.671,242.677c66.985,0,121.319-54.353,121.319-121.271C320.99,54.363,266.666,0,199.671,0
                                                        C132.638,0,78.38,54.373,78.38,121.406C78.38,188.324,132.628,242.677,199.671,242.677z"/>
                                                    <path d="M353.006,275.974c-6.407-13.722-14.478-27.521-24.786-40.688c-5.546-7.057-15.663-14.803-25.445-20.109
                                                        c-25.36,28.419-62.118,46.416-103.104,46.416c-40.956,0-77.705-17.997-103.036-46.35c-9.562,5.432-19.412,13.178-24.776,20.215
                                                        c-17.404,22.711-44.265,66.728-48.549,126.044c-0.899,12.518,5.059,31.633,15.52,38.929
                                                        c19.852,13.895,62.672,31.671,151.833,31.671c30.868,0,56.878-1.903,78.862-4.867c-8.874-14.965-14.066-32.369-14.066-50.997
                                                        C255.449,321.836,298.968,277.504,353.006,275.974z"/>
                                                    <path d="M353.006,294.946c-44.887,0-81.281,36.395-81.281,81.281c0,44.886,36.395,81.28,81.281,81.28s81.281-36.395,81.281-81.28
                                                        C434.287,331.341,397.901,294.946,353.006,294.946z M393.13,391.183h-31.355v31.365c0,5.192-4.217,9.399-9.409,9.399
                                                        s-9.41-4.207-9.41-9.399v-31.355l-31.354-0.01c-5.202,0.01-9.41-4.197-9.4-9.391c0-5.201,4.207-9.409,9.4-9.409h31.364v-31.355
                                                        c0-5.192,4.208-9.399,9.4-9.391c5.183,0,9.399,4.198,9.409,9.4v31.346h31.346c5.192,0,9.4,4.208,9.41,9.41
                                                        C402.53,386.976,398.322,391.183,393.13,391.183z"/>
                                                </g>
                                                </svg>


                                </div>

                                <span class="label"><?= __("Auto Post Content") ?></span>
                            </div>
                        </div>

                        <div class="feature-list-item">
                            <div>
                                <div class="imgbox">

                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                            viewBox="0 0 457.508 457.508" xml:space="preserve">
                                                    <g>
                                                        <path d="M78.377,121.405c0,66.918,54.258,121.271,121.291,121.271c66.984,0,121.319-54.353,121.319-121.271
                                                            C320.988,54.363,266.663,0,199.668,0C132.625,0,78.377,54.363,78.377,121.405z"/>
                                                        <path d="M38.817,400.439c19.852,13.895,62.673,31.67,151.833,31.67c30.868,0,56.877-1.902,78.861-4.867
                                                            c-8.874-14.965-14.066-32.369-14.066-50.996c0-54.4,43.52-98.732,97.566-100.264c-6.407-13.721-14.478-27.52-24.786-40.688
                                                            c-5.546-7.057-15.663-14.803-25.445-20.101c-25.36,28.42-62.118,46.407-103.103,46.407c-40.957,0-77.705-17.997-103.036-46.35
                                                            c-9.562,5.432-19.412,13.177-24.776,20.215c-17.404,22.711-44.265,66.727-48.549,126.043
                                                            C22.399,374.018,28.356,393.133,38.817,400.439z"/>
                                                        <path d="M353.002,294.945c-44.886,0-81.281,36.395-81.281,81.281s36.396,81.281,81.281,81.281
                                                            c44.887,0,81.281-36.395,81.281-81.281S397.899,294.945,353.002,294.945z M393.126,391.182h-31.355l-18.818,0.01l-31.355-0.01
                                                            c-5.202,0.01-9.41-4.197-9.4-9.389c0-5.203,4.208-9.41,9.4-9.41h31.365h18.809h31.347c5.192,0,9.399,4.207,9.409,9.41
                                                            C402.527,386.975,398.319,391.182,393.126,391.182z"/>
                                                    </g>
                                                    </svg>

                                 
                                </div>

                                <span class="label"><?= __("Schedule Calendar") ?></span>
                            </div>
                        </div>

                        <div class="feature-list-item">
                            <div>
                                <div class="imgbox">


                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M224 256c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm89.6 32h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-74.2-60.2-134.4-134.4-134.4z"/></svg>

                                </div>

                                <span class="label"><?= __("Free Trial") ?></span>
                            </div>
                        </div>

                        <div class="feature-list-item">
                            <div>
                                <div class="imgbox">

                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M224 256c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm89.6 32h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-74.2-60.2-134.4-134.4-134.4z"/></svg>

                                </div>

                                <span class="label"><?= __("Safe to use") ?></span>
                            </div>
                        </div>




                    </div>
                    <a href="<?= APPURL."/signup" ?>" class="button button--oval" style="background-image: linear-gradient(60deg,  #902eff,  #902eff,  #4eb9a6, #4eb9a6);"><?= __("Начать"); ?></a>
                </div>
            </div>
        </div>
        <div id="some-block" class="section">
            <div class="container-1100">
                <div class="left">
                    <div class="sliders2">
                        <div class="img">
                            <img class="pc" src="<?= active_theme_url() . "/assets/img/1.png?ver=2" ?>">
                            <img class="sm" src="<?= active_theme_url() . "/assets/img/4.png?ver=2" ?>">
                        </div>
                        <div class="img">
                            <img class="pc" src="<?= active_theme_url() . "/assets/img/22.png?ver=2" ?>">
                            <img class="sm" src="<?= active_theme_url() . "/assets/img/55.png?ver=2" ?>">
                        </div>
                        <div class="img">
                            <img class="pc" src="<?= active_theme_url() . "/assets/img/33.png?ver=2" ?>">
                            <img class="sm" src="<?= active_theme_url() . "/assets/img/66.png?ver=2" ?>">
                        </div>
                    </div>
                </div>
                <div class="right">
                    <i class="fas fa-angle-double-left"></i>
                    <div class="sliders">
                        <div class="slider">
                            <div class="photo_block">
                                <div class="img">
                                    <div style="background: url(https://pp.userapi.com/c624323/v624323495/2bab7/UWA00lmQ9Q4.jpg) no-repeat center center; background-size: cover; width: 80px; height: 80px; margin: auto; border-radius: 50%;" ></div>
                                </div>
                                <p class="name">Денис Миончинский</p>

                            </div>
                            <div class="text_block">
                                <div class="stars">
                                    <span class="star fas fa-star"></span>
                                    <span class="star fas fa-star"></span>
                                    <span class="star fas fa-star"></span>
                                    <span class="star fas fa-star"></span>
                                    <span class="star far fa-star"></span>
                                </div>
                                <p class="date">20 февраля 2018 11:23</p>
                                <p class="text">IWOBOX — крутой. Перед тем как остановиться на нём, я перепробовал едва ли не все существующие аналоги, поэтому за свой выбор спокоен </p>
                            </div>
                        </div>
                        <div class="slider">
                            <div class="photo_block">
                                <div class="img">
                                    <div class="img">
                                        <div style="background: url(https://pp.userapi.com/c625519/v625519416/4bf7d/MSPdUxJuN_U.jpg) no-repeat center center; background-size: cover; width: 80px; height: 80px; margin: auto; border-radius: 50%;" ></div>
                                    </div>
                                </div>
                                <p class="name">Никита Михеенков</p>

                            </div>
                            <div class="text_block">
                                <div class="stars">
                                    <span class="star fas fa-star"></span>
                                    <span class="star fas fa-star"></span>
                                    <span class="star fas fa-star"></span>
                                    <span class="star far fa-star"></span>
                                    <span class="star far fa-star"></span>
                                </div>
                                <p class="date">22 марта 2018 20:47</p>
                                <p class="text">Подключили все наши социальные аккаунты к IWOBOX и с интересом следим за статистикой групп. Выявили посты, которые бесят аудиторию и вызывают отписки — больше не ошибаемся</p>
                            </div>
                        </div>
                        <div class="slider">
                            <div class="photo_block">
                                <div class="img">
                                       <div style="background: url(https://pp.userapi.com/c604427/v604427880/25076/sM3pAPy_-YY.jpg) no-repeat center center; background-size: cover; width: 80px; height: 80px; margin: auto; border-radius: 50%;" ></div>
                                </div>
                                <p class="name">Сергей Бурчаков</p>

                            </div>
                            <div class="text_block">
                                <div class="stars">
                                    <span class="star fas fa-star"></span>
                                    <span class="star fas fa-star"></span>
                                    <span class="star fas fa-star"></span>
                                    <span class="star fas fa-star"></span>
                                    <span class="star fas fa-star"></span>
                                </div>
                                <p class="date">23 апреля 2018 10:15</p>
                                <p class="text">Пользуюсь Вашим Сервисом уже Полтора Года, познакомил с Сервисом больше 10 клиентов! Вы Лучшие!</p>
                            </div>
                        </div>
                        <div class="slider">
                            <div class="photo_block">
                                <div class="img">
                                    <div style="background: url(https://pp.userapi.com/c824201/v824201076/1ab30/kVwhNkGko0s.jpg) no-repeat center center; background-size: cover; width: 80px; height: 80px; margin: auto; border-radius: 50%;" ></div>
                                </div>
                                <p class="name">Aurora Borealis</p>

                            </div>
                            <div class="text_block">
                                <div class="stars">
                                    <span class="star fas fa-star"></span>
                                    <span class="star fas fa-star"></span>
                                    <span class="star fas fa-star"></span>
                                    <span class="star fas fa-star"></span>
                                    <span class="star fas fa-star"></span>
                                </div>
                                <p class="date">17 мая 2018 12:36</p>
                                <p class="text">Чудо-сервис! Отличная поддержка! Спасибо, что вы есть ))</p>
                            </div>
                        </div>
                    </div>
                    <i class="fas fa-angle-double-right"></i>
                </div>
            </div>
        </div>
        <?php 
            $currency = $Settings->get("data.currency");
            $iszdc = isZeroDecimalCurrency($currency);
        ?>
        <div id="packages" class="section">
            <div class="container-1100">
                <div class="row clearfix pos-r">
                    <h2 class="section-title text-c"><?= __("Relevant prices for you") ?></h2>

                    <div class="package-list clearfix">
                        <?php 
                            $show_modules = false;

                            $available_modules = [];
                            foreach ($Plugins->getDataAs("Plugin") as $p) {
                                $available_modules[] = $p->get("idname");
                            }

                            foreach ($Packages->getDataAs("Package") as $p) {
                                $package_modules = $p->get("settings.modules");
                                if ($package_modules &&
                                    is_array($package_modules) &&
                                    array_intersect($package_modules, $available_modules)) 
                                {
                                    $show_modules = true;
                                    break;
                                }
                            }
                        ?>

                        <?php foreach ($Packages->getDataAs("Package") as $p): ?>
                            <div class="package-list-item">
                                <div class="best">Лучший выбор</div>
                                <div>
                                    <div class="price">
                                        <span class="number"><?= format_price($p->get("monthly_price"), $iszdc) ?></span>
                                        <span class="per">
                                            <?= htmlchars($currency) ?>/<?= __("per month") ?>    
                                        </span>       
                                    </div>

                                    <div class="title"><?= htmlchars($p->get("title")) ?></div>

                                    <div class="annual">
                                        <?= __("Annual Price") ?>:
                                        <?php 
                                            if ($p->get("annual_price") > 0) {
                                                $annual = $p->get("annual_price");
                                            } else {
                                                $annual = 12 * $p->get("monthly_price");
                                            }
                                        ?>
                                        <?= format_price($annual, $iszdc) ?>
                                        <?= htmlchars($currency) ?>

                                        <div class="save">
                                            <?php 
                                                if ($iszdc) {
                                                    $total = 12 * round($p->get("monthly_price"));
                                                    $dif = $total - round($annual);
                                                } else {
                                                    $total = 12 * $p->get("monthly_price");
                                                    $dif = $total - $annual; 
                                                }

                                                if ($dif > 0) {
                                                    echo __("You save %s", "<span>" . format_price($dif, $iszdc) ." ".htmlchars($currency)."</span>");
                                                }
                                            ?>
                                        </div>
                                    </div>

                                    <div class="features">
                                        <div class="feature">
                                            <?php 
                                                $max = (int)$p->get("settings.max_accounts");
                                                if ($max > 0) {
                                                    echo n__("Only 1 account", "Up to %s accounts", $max, $max);
                                                } else if ($max == "-1") {
                                                    echo __("Unlimited accounts");
                                                } else {
                                                    echo __("Zero accounts");
                                                }
                                            ?>
                                        </div>

                                        <div class="feature">
                                            <div class="feature-title"><?= __("Post Types") ?>:</div>

                                            <div>
                                                <span class="<?= $p->get("settings.post_types.timeline_photo") ? "" : "not" ?>"><?= __("Photo") ?></span>, 
                                                <span class="<?= $p->get("settings.post_types.timeline_video") ? "" : "not" ?>"><?= __("Video") ?></span>, 

                                                <br>
                                                
                                                <?php 
                                                    $story_photo = $p->get("settings.post_types.story_photo");
                                                    $story_video = $p->get("settings.post_types.story_video");
                                                ?>
                                                <?php if ($story_photo && $story_video): ?>
                                                    <span><?= __("Story")." (".__("Photo+Video").")" ?></span>,
                                                <?php elseif ($story_photo): ?>
                                                    <span><?= __("Story")." (".__("Photo only").")" ?></span>,
                                                <?php elseif ($story_video): ?>
                                                    <span><?= __("Story")." (".__("Video only").")" ?></span>,
                                                <?php else: ?>
                                                    <span class="not"><?= __("Story")." (".__("Photo+Video").")" ?></span>,
                                                <?php endif ?>

                                                <br>

                                                <?php 
                                                    $album_photo = $p->get("settings.post_types.album_photo");
                                                    $album_video = $p->get("settings.post_types.album_video");
                                                ?>
                                                <?php if ($album_photo && $album_video): ?>
                                                    <span><?= __("Album")." (".__("Photo+Video").")" ?></span>
                                                <?php elseif ($album_photo): ?>
                                                    <span><?= __("Album")." (".__("Photo only").")" ?></span>
                                                <?php elseif ($album_video): ?>
                                                    <span><?= __("Album")." (".__("Video only").")" ?></span>
                                                <?php else: ?>
                                                    <span class="not"><?= __("Album")." (".__("Photo+Video").")" ?></span>
                                                <?php endif ?>
                                            </div>
                                        </div>

                                        <div class="feature">
                                            <div class="feature-title"><?= __("Cloud Import") ?>:</div>

                                            <div style="height: 35px;">
                                                <?php $none = true; ?>
                                                <?php if ($p->get("settings.file_pickers.google_drive")): ?>
                                                    <?php $none = false; ?>
                                                    <span class="icon m-5 mdi mdi-google-drive" title="Google Drive"></span>
                                                <?php endif ?>

                                                <?php if ($p->get("settings.file_pickers.dropbox")): ?>
                                                    <?php $none = false; ?>
                                                    <span class="icon m-5 mdi mdi-dropbox" title="Dropbox"></span>
                                                <?php endif ?>

                                                <?php if ($p->get("settings.file_pickers.onedrive")): ?>
                                                    <?php $none = false; ?>
                                                    <span class="icon m-5 mdi mdi-onedrive" title="OneDrive"></span>
                                                <?php endif ?>

                                                <?php if ($none): ?>
                                                    <span class="m-5 inline-block" style="line-height: 24px;"><?= __("Not available") ?></span>
                                                <?php endif ?>
                                            </div>
                                        </div>

                                        <?php if ($show_modules): ?>
                                            <div class="feature">
                                                <div class="feature-title"><?= __("Modules") ?></div>
                                                <div class="modules clearfix">
                                                    <?php $package_modules = $p->get("settings.modules") ?>
                                                    <?php foreach ($Plugins->getDataAs("Plugin") as $m): ?>
                                                        <?php 
                                                            $idname = $m->get("idname");

                                                            $config_path = PLUGINS_PATH . "/" . $idname . "/config.php"; 
                                                            if (!file_exists($config_path)) {
                                                                continue;
                                                            }
                                                            $config = include $config_path;
                                                            $name = empty($config["plugin_name"]) ? $idname : $config["plugin_name"];
                                                        ?>
                                                        <span class="module tooltip tippy <?= in_array($m->get("idname"), $package_modules) ? "" : "disabled" ?>"
                                                              style="<?= empty($config["icon_style"]) ? "" : $config["icon_style"] ?>"
                                                              title="<?= htmlchars($name) ?>"
                                                              data-size="small">
                                                                
                                                            <?php if (in_array($m->get("idname"), ["auto-follow", "auto-unfollow"])): ?>
                                                                <?php 
                                                                    $name = empty($config["plugin_name"]) ? $idname : $config["plugin_name"];
                                                                    echo textInitials($name, 2);
                                                                ?>
                                                            <?php elseif($m->get("idname") == "auto-like") : ?>
                                                                <span class="mdi mdi-heart"></span>
                                                            <?php elseif($m->get("idname") == "auto-comment") : ?>
                                                                <span class="mdi mdi-comment-processing"></span>
                                                            <?php elseif($m->get("idname") == "welcomedm") : ?>
                                                                <span class="sli sli-paper-plane"></span>
                                                            <?php elseif($m->get("idname") == "auto-repost") : ?>
                                                                <span class="sli sli-reload"></span>
                                                            <?php endif ?>
                                                        </span>
                                                    <?php endforeach ?>
                                                </div>
                                            </div>
                                        <?php endif ?>

                                        <div class="feature">
                                            <span class="<?= $p->get("settings.spintax") ? "" : "not" ?>"><?= __("Spintax Support") ?></span>
                                        </div>

                                        <div class="feature">
                                            <?= __("Storage") ?>:
                                            <span class="color-primary fw-500">
                                                <?= readableFileSize($p->get("settings.storage.total") * 1024 * 1024) ?>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="choose">
                                        <a href="<?= APPURL."/".($AuthUser ? "renew" : "signup")."?package=".$p->get("id") ?>" class="button button--dark button--oval" style="    background-image: linear-gradient(60deg, #b800ff, #b800ff, #7800ea, #0f00d3, #0f00d3);"><?= __("Get started") ?></a>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="ss-block">
            <div class="text">
                IwoBox - это мощный облачный инструмент отложенного постинга, продвижения, мониторинга и управления аккаунтом Instagram. Сервис не нуждается в дополнительных установках программ, расширений, либо приложений. В его использовании - всё просто, легко, удобно, а самое главное – безопасно!
                Мы заботимся и любим своих клиентов! Только сейчас, акция, - успей зарегистрироваться, и получи три дня в подарок c полной поддержкой всех модулей!

            </div>
        </div>
        <div class="sss-block">
            <h1>SMM 5.0</h1>
            <div class="text">
                Продвижение в Instagram – очень сложный и затратный процесс. Специалисты IwoBox работали и продолжают работу над решением подобных проблем! Высота финансовой планки зависит от количества задач, которые выполняет smm-специалист, либо аккаунт-менеджер. Изучив алгоритм, который выполняет человек, мы обучили IwoBox всем необходимым процессам. Технологии, процессы и алгоритмы улучшаются с каждым днём. По нашей внутренней road map, в скором времени, сервис IwoBox будет с легкостью сам подбирать контент план и находить именно “Вашу” аудиторию.

            </div>
            <a href="<?= APPURL."/signup" ?>" class="button button--oval" style="background-image: linear-gradient(60deg, #5300b1, #5300b1, #ea5283, #fa7e83);
"><?= __("Стать популярнее"); ?></a>
        </div>

        <footer>
            <div class="container-1100">
                <div class="row clearfix">
                    <a href="<?= APPURL ?>" class="footer-logo">
                        <img src="<?= site_settings("logotype") ? site_settings("logotype") : APPURL."/assets/img/logo.png" ?>" 
                             alt="<?= site_settings("site_name") ?>">
                    </a>

                    <?php if (!$AuthUser): ?>
                        <select class="footer-lang-selector" data-base="<?= APPURL ?>">
                            <?php foreach (Config::get("applangs") as $l): ?>
                                <option value="<?= $l["code"] ?>" <?= $l["code"] == ACTIVE_LANG ? "selected" : "" ?>><?= $l["localname"] ?></option>
                            <?php endforeach ?>
                        </select>
                    <?php endif; ?>

                    <div class="copyright">
                        &copy; <?= __("Copyright")." ".date("Y") ?> 
                    </div>
                </div>
            </div>
        </footer>
        
        
        <script type="text/javascript" src="<?= active_theme("url")."/assets/js/plugins.js?v=".VERSION ?>"></script>
        <script type="text/javascript" src="<?= active_theme("url")."/assets/js/core.js?v=".VERSION ?>"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
        <script type="text/javascript" charset="utf-8">

            $(function(){
                $('.scroll_btn').on('click', function scroll(e) {
                    $('html, body').animate({
                        scrollTop: $(e.target.hash).offset().top - 90,
                    }, 2000);
                    if($('body').width()<780) {
                        $('.menu-toggle').click();
                    }
                });
                $('.sliders').slick({
                    prevArrow:"#some-block .fa-angle-double-left",
                    nextArrow:"#some-block .fa-angle-double-right",
                });
                $('.sliders2').slick({
                    prevArrow:"",
                    nextArrow:"",
                    autoplay:true,
                });
            })
        </script>
        <!-- Yandex.Metrika counter -->
        <script type="text/javascript" >
            (function (d, w, c) {
                (w[c] = w[c] || []).push(function() {
                    try {
                        w.yaCounter49414933 = new Ya.Metrika2({
                            id:49414933,
                            clickmap:true,
                            trackLinks:true,
                            accurateTrackBounce:true,
                            webvisor:true,
                            trackHash:true,
                            ecommerce:"dataLayer"
                        });
                    } catch(e) { }
                });

                var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function () { n.parentNode.insertBefore(s, n); };
                s.type = "text/javascript";
                s.async = true;
                s.src = "https://cdn.jsdelivr.net/npm/yandex-metrica-watch/tag.js";

                if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", f, false);
                } else { f(); }
            })(document, window, "yandex_metrika_callbacks2");
        </script>
        <!-- /Yandex.Metrika counter -->
        <?php require_once(APPPATH.'/views/fragments/google-analytics.fragment.php'); ?>
    </body>
</html>