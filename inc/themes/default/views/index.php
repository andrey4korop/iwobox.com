<?php if (!defined('APP_VERSION')) die("Yo, what's up?"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <meta name="description" content="<?= site_settings("site_description") ?>">
    <meta name="keywords" content="<?= site_settings("site_keywords") ?>">
    <link rel="icon" href="<?= site_settings("logomark") ? site_settings("logomark") : APPURL."/assets/img/logomark.png" ?>" type="image/x-icon">
    <link rel="shortcut icon" href="<?= site_settings("logomark") ? site_settings("logomark") : APPURL."/assets/img/logomark.png" ?>" type="image/x-icon">
    <title><?= site_settings("site_name") ?></title>
    <link rel="stylesheet" href="/dist/css/main.css?v=<?= VERSION ?>">
    <meta name="msvalidate.01" content="1375CF06D08B2F0D2503A431783E2AC5" />
    <style>
        section.title.twelve_bg{
            background: none;
        }
    </style>
</head>
<body>
<div id="app">
    <header>
        <a href="/" class="logo">
            <logo></logo>
        </a>
        <a class="pc" v-scroll-to="{ el:'#why'}">Почему IWOBOX</a>
        <a class="pc" v-scroll-to="{ el:'#who'}">Кому</a>
        <a class="pc" v-scroll-to="{ el:'#how'}">Как начать</a>
        <a class="pc" v-scroll-to="{ el:'#what'}">Что получишь</a>
        <a class="pc" v-scroll-to="{ el:'#price'}">Цены</a>
        <a class="pc" v-scroll-to="{ el:'#about'}">О продукте</a>
        <?php if ($AuthUser): ?>
            <a class="pc button" href="<?= APPURL."/profile/" ?>"><?= __("Hi, %s", htmlchars($AuthUser->get("firstname"))) ?></a>
        <?php else: ?>
        <a class="pc button" href="/signup">Начать</a>
        <?php endif ?>
        <a @click="showmenuclick" class="menu sm">
            <menu1></menu1>
        </a>
        <transition name="menu">
            <div class="sm-menu" v-if="showmenu">
                <a  v-scroll-to="{
              el: '#why',
              offset: -70,
              onDone: showmenuclick,
          }" href="#why">Почему IWOBOX</a>
                <a v-scroll-to="{
              el: '#who',
              offset: -70,
              onDone: showmenuclick,
          }" class="" href="#who">Кому</a>
                <a v-scroll-to="{
              el: '#how',
              offset: -70,
              onDone: showmenuclick,
          }" class="" href="#how">Как начать</a>
                <a v-scroll-to="{
              el: '#what',
              offset: -70,
              onDone: showmenuclick,
          }" class="" href="#what">Что получишь</a>
                <a v-scroll-to="{
              el: '#price',
              offset: -70,
              onDone: showmenuclick,
          }" class="" href="#price">Цены</a>
                <a v-scroll-to="{
              el: '#about',
              offset: -70,
              onDone: showmenuclick,
          }" class="" href="#about">О продукте</a>
                <?php if ($AuthUser): ?>
                    <a class="button" href="<?= APPURL."/profile/" ?>"><?= __("Hi, %s", htmlchars($AuthUser->get("firstname"))) ?></a>
                <?php else: ?>
                    <a class="button" href="/signup">Начать</a>
                <?php endif ?>

            </div>
        </transition>
    </header>

    <section class="first">
        <div class="bg">
            <div class="circle el1"></div>
            <div class="circle el2"></div>
            <div class="ug3 el3"></div>
            <div class="ug3 el4"></div>
        </div>
        <div class="wrap">
            <div class="left">
                <h1><span class="bold">IwoBox</span> твой  <span class="bold">лучший помошник</span> в работе с <span class="bold">Instagram</span></h1>
                <p class="text_1">Сервис IwoBox заинтересован в результате</p>
                <p class="text_1">Вашего акаунта - <span class="b24">нет результата вернём деньги.</span></p>
                <a href="/signup" class="button">Начать</a>
                <p class="text_1"><span class="bold">Получи 3 дня бесплатного сервиса IWOBOX!</span></p>
                <p class="text_1">Ты поймешь что тебе это нужно!</p>
            </div>
            <div class="right">
            <notifys></notifys>
            </div>
        </div>
    </section>
    <section class="second">
        <div class="wrap">
            <div class="block">
                <p>Привлекайте до</p>
                <p class="big">300 КЛИЕНТОВ В ДЕНЬ</p>
                <div class="orange_circle el1"></div>
            </div>
            <div class="block">
                <p>Набирайте в месяц до</p>
                <p class="big">6800 ПОДПИСЧИКОВ</p>
                <div class="orange_circle el2"></div>
            </div>
            <div class="block">
                <p>Автоматизируйте действия</p>
                <p class="big">НАЖАТИЕМ 1 КНОПКИ</p>
                <div class="orange_circle el3"></div>
            </div>
            <div class="block">
                <p>Занимайтесь</p>
                <p class="big">АРБИТРАЖЕМ ТРАФИКА</p>
                <div class="orange_circle el4"></div>
            </div>
        </div>
    </section>
    <section class="title title_first" id="why">
        <p> Почему  <heart-red></heart-red>     IWOBOX?</p>
    </section>
    <section class="third">
        <div class="bg">
            <div class="circle el1"></div>
            <div class="ug3 el2"></div>
        </div>
        <blue-button @clickbtn="prevSlideThird"></blue-button>
        <div class="wrap">
            <swiper :options="{loop:true, autoplay:{delay: 5000}}" ref="third1" >
                <swiper-slide>
                    <div class="slide">
                        <div class="left">
                            <div class="white_padding">
                                <p class="orange">Мы помогаем брэндам
                                    расти в Instagram</p>
                                <p>Instagram — это больше, чем сервис, в котором выкладывают фото еды и selfie. Он входит в число наиболее перспективных социальных платформ для продвижения бизнеса и его популярность уверенно растет с каждым днем.
                                    Сервис позволяет публиковать и потреблять визуальный контент, который, по статистике, воспринимается пользователями в десятки раз быстрее по сравнению с текстовым.
                                    Количество пользователей Instagram превысило 500 миллионов, при этом 300 миллионов человек заходят в социальную сеть ежедневно.</p>
                            </div>
                        </div>
                        <div class="right">
                            <img src="/dist/img/slide_img1.jpg" alt="">
                        </div>
                    </div>
                </swiper-slide>
                <swiper-slide>
                    <div class="slide">
                        <div class="left">
                            <div class="white_padding">
                                <p class="orange">Автоматический сервис раскрутки в IwoBox</p>
                                <p>Сервис IwoBox помогает раскручивать Instagram быстро, эффективно и без Вашего вмешательства.</p>
                                <p>Продвижение Instagram с нами - это легко и просто: сервис сам ставит лайки, подписывается и комментирует профили в соответствии с заданными параметрами.</p>
                                <p>Помогаем раскручивать Instagram эффективно - в IwoBox есть функции аналитики, автоотписки и автоочистки от ботов и коммерческих пользователей.</p>
                            </div>
                        </div>
                        <div class="right">
                            <img src="/dist/img/slide_img1.jpg" alt="">
                        </div>
                    </div>
                </swiper-slide>
                <swiper-slide>
                    <div class="slide">
                        <div class="left">
                            <div class="white_padding">
                                <p class="orange">Лайки</p>
                                <p>Автоматические лайки в Инстаграме с IwoBox- это эффективная накрутка пользователей в вашем аккаунте.</p>
                                <p>Накрутка лайков в Инстаграме привлекает внимание и стимулирует новые подписки.</p>
                                <p>Накрутка лайков в Инстаграме онлайн через IwoBox позволяет работать по выбранным аккаунтам, оставлять взаимные лайки и ставить лайки своим подписчикам, чтобы взаимодействовать со своей аудиторией.</p>
                            </div>
                        </div>
                        <div class="right">
                            <img src="/dist/img/slide_img1.jpg" alt="">
                        </div>
                    </div>
                </swiper-slide>
                <swiper-slide>
                    <div class="slide">
                        <div class="left">
                            <div class="white_padding">
                                <p class="orange">Подписки</p>
                                <p>Подписки в Инстаграм, полученные с помощью IwoBox- это заинтересованные и живые пользователи в вашем аккаунте.</p>
                                <p>Сервис позволяет подписываться на аккаунты по заданным критериям, в результате чего накрутка подписчиков в Инстаграме привлекает только активные профили.</p>
                                <p>Функционал позволяет оставлять взаимные подписки в Инстаграм в автоматическом режиме.</p>
                                <p>IwoBox привлечёт новых подписчиков в Инстаграм, которым действительно будет интересен ваш аккаунт.</p>
                            </div>
                        </div>
                        <div class="right">
                            <img src="/dist/img/slide_img1.jpg" alt="">
                        </div>
                    </div>
                </swiper-slide>
                <swiper-slide>
                    <div class="slide">
                        <div class="left">
                            <div class="white_padding">
                                <p class="orange">Отписки</p>
                                <p> Функция для отписок помогает в автоматическом режиме отписаться от пользователей, когда достигнут лимит подписок.</p>
                                <p> Отписка в Инстаграме используется, чтобы отписаться от нецелевых и неактивных пользователей и освободить место для новых заинтересованных клиентов.</p>
                                <p>Программа для отписки в Инстаграме позволит начать автоматическую отписку по достижению лимита.</p>
                                <p>В IwoBox работает взаимная отписка подписок: вы будете отписываться от тех, кто отписался от вас.</p>
                            </div>
                        </div>
                        <div class="right">
                            <img src="/dist/img/slide_img1.jpg" alt="">
                        </div>
                    </div>
                </swiper-slide>
                <swiper-slide>
                    <div class="slide">
                        <div class="left">
                            <div class="white_padding">
                                <p class="orange">Комментарии</p>
                                <p> Комментарии в Инстаграм помогают привлечь внимание новых пользователей и коммуницировать со своей аудиторией, но комментарии занимают очень много времени.</p>
                                <p>В  IwoBox накрутка комментариев в Инстаграм происходит в автоматическом режиме, поэтому не нужно постоянно следить за публикациями пользователей. </p>
                            </div>
                        </div>
                        <div class="right">
                            <img src="/dist/img/slide_img1.jpg" alt="">
                        </div>
                    </div>
                </swiper-slide>
            </swiper>
        </div>
        <blue-button @clickbtn="nextSlideThird"></blue-button>
    </section>
    <section class="bntSliders lg wrap">
        <mob-btn-blue @sliderprev="prevSlideThird" @slidernext="nextSlideThird" ></mob-btn-blue>
    </section>
    <section class="title" id="who">
        Кому стоит попробовать?
    </section>
    <section class="fourth">
        <div class="wrap">
            <div class="element">
                <p class="title">Брэндам</p>
                <div class="icon"><brand></brand></div>
            </div>
            <div class="element">
                <p class="title">Рестoранам</p>
                <div class="icon"><restoran></restoran></div>
            </div>
            <div class="element">
                <p class="title">Бизнесменам и предпринимателям</p>
                <div class="icon"><badge></badge></div>
            </div>
            <div class="element">
                <p class="title">SMM</p>
                <div class="icon"><smm></smm></div>
            </div>
            <div class="element">
                <p class="title">Интернет
                    магазинам</p>
                <div class="icon"><badge2></badge2></div>
            </div> <div class="element">
                <p class="title">Блоггерам</p>
                <div class="icon"><messange></messange></div>
            </div>
            <div class="element">
                <p class="title">Арбитражникам трафика</p>
                <div class="icon"><peoples></peoples></div>
            </div>
            <div class="element">
                <p class="title">Стартап проэктам</p>
                <div class="icon"><raketa-blue></raketa-blue></div>
            </div>
        </div>
    </section>
    <section class="title bg_second" id="how">
        Как начать использовать IWOBOX?
    </section>
    <section class="fifth">
        <div class="bg">
            <div class="circle el1"></div>
            <div class="ug3 el2"></div>
            <div class="circle el3"></div>
            <div class="ug3 el4"></div>
        </div>
        <div class="wrap">
            <div class="element">
                <div class="icon"><notebook></notebook></div>
                <div class="text">Зарегистрируйся</div>
            </div>
            <div class="element">
                <div class="icon"><two-people></two-people></div>
                <div class="text">Выбери свою<br/> аудиторию</div>
            </div>
            <div class="element">
                <div class="icon"><finger></finger></div>
                <div class="text">Выбери способ<br/>
                    взаимодействия<br/>
                    с аудиторией</div>
            </div>
            <div class="element">
                <div class="icon"><rate></rate></div>
                <div class="text">Получай результаты<br/>
                    и статистику</div>
            </div>
        </div>
    </section>
    <section class="title title_with_raketa" id="what">
        <raketa_with_circle></raketa_with_circle>
        Что ты получишь?
    </section>
    <section class="sixth">
        <div class="wrap">
            <div class="element">
                <div class="number">01</div>
                <div class="text">
                    <p class="bold">Отложенный постинг </p>
                    <p>без ограничений</p>
                </div>
            </div>
            <div class="element">
                <div class="number">02</div>
                <div class="text">
                    <p class="bold">Графический редактор</p>
                    <p>фотографий</p>
                </div>
            </div>
            <div class="element">
                <div class="number">03</div>
                <div class="text">
                    <p class="bold">Мозаику</p>
                    <p>(insta lending, 2x2, 3x2, 3x3, 4x3)</p>
                </div>
            </div>
            <div class="element">
                <div class="number">04</div>
                <div class="text">
                    <p class="bold">Статистику по работе</p>
                    <p class="bold">сервиса</p>
                </div>
            </div>
            <div class="element" :class="(sixhidden) ? 'sm-hidden' : ''">
                <div class="number">05</div>
                <div class="text">
                    <p class="bold">Автолайки</p>
                    <p>(по местам, аккаунтам, аккаунтам конкурентов, хеш-тегам) </p>
                </div>
            </div>
            <div class="element" :class="(sixhidden) ? 'sm-hidden' : ''">
                <div class="number">06</div>
                <div class="text">
                    <p class="bold">Автоподписку</p>
                    <p>опция умного ожидания</p>
                </div>
            </div>
            <div class="element" :class="(sixhidden) ? 'sm-hidden' : ''">
                <div class="number">07</div>
                <div class="text">
                    <p class="bold">Автоотписка с белым списком</p>
                    <p>(не трогать аккаунты которые</p>
                    <p>были до работы сервиса) </p>
                </div>
            </div>
            <div class="element" :class="(sixhidden) ? 'sm-hidden' : ''">
                <div class="number">08</div>
                <div class="text">
                    <p class="bold">Сообщение приветствие</p>
                    <p>Новым подписчикам</p>
                </div>
            </div>
            <div class="element" :class="(sixhidden) ? 'sm-hidden' : ''">
                <div class="number">09</div>
                <div class="text">
                    <p class="bold">Рассылка в Директ</p>
                    <p>с возможностью</p>
                    <p class="bold">Указать реагирование </p>
                </div>
            </div>
            <div class="element" :class="(sixhidden) ? 'sm-hidden' : ''">
                <div class="number">10</div>
                <div class="text">
                    <p class="bold">Автолайки, автоподписки и отписки</p>
                    <p>имеет опцию работы со списками
                        и фильтр от ботов. </p>
                </div>
            </div>
            <div class="element" :class="(sixhidden) ? 'sm-hidden' : ''">
                <div class="number">11</div>
                <div class="text">
                    <p class="bold">Автокомментарий</p>
                </div>
            </div>
            <div class="element" :class="(sixhidden) ? 'sm-hidden' : ''">
                <div class="number">12</div>
                <div class="text">
                    <p class="bold">Система умной подписки (АI)</p>
                    <p>подбор под оптимальные цели</p>
                </div>
            </div>
            <div class="element" :class="(sixhidden) ? 'sm-hidden' : ''">
                <div class="number">13</div>
                <div class="text">
                    <p class="bold">Трекер</p>
                    <p>с уведомлением на почту</p>
                </div>
            </div>
            <div class="element" :class="(sixhidden) ? 'sm-hidden' : ''">
                <div class="number">14</div>
                <div class="text">
                    <p class="bold">Biolink профиля с кастомизацией</p>
                    <p>и возможностью задать задачи</p>
                </div>
            </div>
            <div class="element" :class="(sixhidden) ? 'sm-hidden' : ''">
                <div class="number">15</div>
                <div class="text">
                    <p class="bold">Личный IG SMM -</p>
                    <p>         аудит консультация и
                        рекомендации</p>
                </div>
            </div>
            <div class="element" :class="(sixhidden) ? 'sm-hidden' : ''">
                <div class="number">16</div>
                <div class="text">
                    <p class="bold">Возможность собирать
                        списки юзеров</p>
                </div>
            </div>
            <div class="see_all" :class="(!sixhidden) ? 'sm-hidden' : ''" @click="sixhiddenclick">Смотреть всё</div>
        </div>
    </section>
    <section class="button">
        <button @click.prevent="goTo('/signup')">Начать</button>
    </section>
    <section class="title" id="price">
        У нас есть пакет для тебя
    </section>
    <section class="seventh pc">
        <div class="wrap">
            <div class="order sale">
                <div class="title">Low 3</div>
                <div class="saleText">SALE</div>
                <div class="saleOldPrice">26,38$</div>
                <div class="price">9,83$<span class="red">*</span></div>
                <div class="in_mounth">в месяц</div>
                <button @click.prevent="goTo('/signup?package=1')">Начать</button>
            </div>
            <div class="order sale">
                <div class="title">Mid 6</div>
                <div class="saleText">SALE</div>
                <div class="saleOldPrice">39,83$</div>
                <div class="price">26,86$<span class="red">*</span></div>
                <div class="in_mounth">в месяц</div>
                <button @click.prevent="goTo('/signup?package=2')">Начать</button>
            </div>
            <div class="order sale">
                <div class="title">High 8</div>
                <div class="saleText">SALE</div>
                <div class="saleOldPrice">299,36$</div>
                <div class="price">186,68$<span class="red">*</span></div>
                <div class="in_mounth">в месяц</div>
                <button @click.prevent="goTo('/signup?package=3')">Пристегнулись?</button>
            </div>

        </div>
    </section>
    <section class="seventh sm">
        <div class="wrap">
            <swiper :options="{loop:true, autoplay:{delay: 5000}}" ref="seventh" >
                <swiper-slide>
                    <div class="order sale">
                        <div class="title">SMALL</div>
                        <div class="saleText">SALE</div>
                        <div class="saleOldPrice">26,38$</div>
                        <div class="price">9,83$<span class="red">*</span></div>
                        <div class="in_mounth">в месяц</div>
                        <button @click.prevent="goTo('/signup?package=1')">Начать</button>
                    </div>
                </swiper-slide>
                <swiper-slide>
                    <div class="order sale">
                        <div class="title">MEDIUM</div>
                        <div class="saleText">SALE</div>
                        <div class="saleOldPrice">39,83$</div>
                        <div class="price">26,86$<span class="red">*</span></div>
                        <div class="in_mounth">в месяц</div>
                        <button @click.prevent="goTo('/signup?package=2')">Начать</button>
                    </div>
                </swiper-slide>
                <swiper-slide>
                    <div class="order sale">
                        <div class="title">XXL</div>
                        <div class="saleText">SALE</div>
                        <div class="saleOldPrice">299,36$</div>
                        <div class="price">186,68$<span class="red">*</span></div>
                        <div class="in_mounth">в месяц</div>
                        <button @click.prevent="goTo('/signup?package=3')">Пристегнулись?</button>
                    </div>
                </swiper-slide>
            </swiper>
        </div>
    </section>
    <section class="wrap text_for_bank">
        <span class="red">*</span> - цена в долларах указана в рекламных целях, списание средств будет происходить по курсу Вашего банка в национальной валюте Вашей страны.
    </section>
    <section class="bntSliders sm wrap">
        <mob-btn-blue @sliderprev="prevSlideSeventh" @slidernext="nextSlideSeventh" ></mob-btn-blue>
    </section>
    <section class="under">
        <div class="element">
            <heart-blue></heart-blue>
            <div class="title">Лайки+</div>
        </div>
        <div class="element">
            <coment></coment>
            <div class="title">Коментарии+</div>
        </div>
        <div class="element">
            <user></user>
            <div class="title">Подписчики+</div>
        </div>
        <div class="element">
            <rate2></rate2>
            <div class="title">Рост+</div>
        </div>
        <div class="element">
            <sale></sale>
            <div class="title">Проодажи+</div>
        </div>
    </section>
    <section class="title" id="about">
        Довольны ли нашим сервисом?
    </section>
    <section class="eight">
        <div class="bg">
            <div class="circle el1"></div>
            <div class="ug3 el2"></div>
            <div class="circle el3"></div>
        </div>
        <div class="wrap">
            <eight-before></eight-before>
            <eight-after></eight-after>
        </div>
    </section>
    <section class="nine">
        <orange-button1 @clickbtn="prevSlideNine"></orange-button1>
        <div class="wrap">
            <div class="nine_slider">
                <swiper :options="{loop:true, autoplay:{delay: 5000}}" ref="nine" >
                    <swiper-slide>
                        <div class="nine_sl">
                            <raketa></raketa>
                            <div class="nine_slide">
                                <div class="img_block">
                                    <div class="img" style='background: url("https://instagram.fdnk1-2.fna.fbcdn.net/vp/d65b5d983ddabd2faf653b5c2df8629a/5C379D74/t51.2885-15/sh0.08/e35/s640x640/35928077_201216907247179_7258642176467271680_n.jpg") center center / cover no-repeat'></div>

                                </div>
                                <div class="text_block">
                                    <p class="text_row1">jm.life</p>
                                    <p class="text_row2">Блогер, ведущая марафона желаний</p>
                                    <p class="text_row3">Я знаю, как исполнять мечты и двигаться навстречу к цели. Мой проект - марафон желаний, в котором я могу научить каждого правильно формулировать цели и реализовывать задуманное. Продвижение в IwoBox помогает мне постоянно набирать активную аудиторию, готовую к участию в моем проекте. Запустить продвижение на IwoBox очень просто - один раз настраиваешь, запускаешь и все подписчики и заявки на участие начинают приходить сами собой. Подписываться люди начали уже на следующий день после запуска бесплатного периода.  Теперь я занимаюсь любимыми делом, а профиль продвигает IwoBox!:)</p>
                                </div>
                            </div>
                        </div>
                    </swiper-slide>
                    <swiper-slide>
                        <div class="nine_sl">
                            <raketa></raketa>
                            <div class="nine_slide">
                                <div class="img_block">
                                    <div class="img" style='background: url("https://instagram.fdnk1-2.fna.fbcdn.net/vp/ad7fa04dde1b63017cd3890d355c0b7e/5C3140CB/t51.2885-15/sh0.08/e35/s640x640/17818434_272786246500506_8346409267711967232_n.jpg") center center / cover no-repeat'></div>

                                </div>
                                <div class="text_block">
                                    <p class="text_row1">ruslankovalchuk</p>
                                    <p class="text_row2">SMM-специалист</p>
                                    <p class="text_row3">Меня зовут Руслан Ковальчук. Я занимаюсь продвижением в Инстаграме. Сервис IwoBox использую активно уже на протяжении 4 месяцев.
                                        На этой платформе можно не просто накрутить себе подписчиков в Инстаграм, но и найти новых клиентов и сделать реальные продажи. Потому как накрутка на этом сервисе только по целевой живой аудитории Инстаграм, без ботов. Более того, можно запустить функцию налайкивания ленты новостей ваших подписчиков в Инстаграм, и даже комментирования их постов на полном автомате! Все работает четко, быстро и без перебоев. Сервис IwoBox отличный, всем рекомендую!</p>
                                </div>
                            </div>
                        </div>
                    </swiper-slide>
                    <swiper-slide>
                        <div class="nine_sl">
                            <raketa></raketa>
                            <div class="nine_slide">
                                <div class="img_block">
                                    <div class="img" style='background: url("https://instagram.fdnk1-2.fna.fbcdn.net/vp/e6c8840d4ae9201474bb1647f24cd56e/5C1A413F/t51.2885-19/s150x150/40360554_747729318908671_1601864866870591488_n.jpg") center center / cover no-repeat'></div>

                                </div>
                                <div class="text_block">
                                    <p class="text_row1">denpanov</p>
                                    <p class="text_row2">Фотограф</p>
                                    <p class="text_row3">Я начал фотографировать с момента появления у меня смартфона.
                                        Поначалу это были просто фотографии для себя, но с появлением инстаграма хобби стало занимать всё больше свободного времени.
                                        Со временем начало получаться, и, создавая что-то, хочется этим поделиться, показать. И раскрутка профиля инстаграм в IwoBox стала отличным способом для этого.
                                        Но, чтобы тебя заметили и оценили твои работы, нужно помимо усилий в плане работы над фотографией, подготовить свой аккаунт.
                                        Сервис IwoBox как раз даёт эту возможность, помогая показать твоё творчество другим. Очистка от ботов позволяет чаще показывать мои фотографии другим пользователям. Имея гибкие и простые настройки подписок, комментариев, сайт легко может дать возможность пробится начинающему фотографу в мир большой фотографии. Однако, не забывайте о важном, о самом контенте. Без крутых работ, функционал IwoBox не раскроется в полной мере. Удачи!</p>
                                </div>
                            </div>
                        </div>
                    </swiper-slide>
                </swiper>
            </div>
        </div>
        <orange-button1 @clickbtn="nextSlideNine"></orange-button1>
    </section>
    <section class="bntSliders lg wrap">
        <mob-btn-orange @sliderprev="prevSlideNine" @slidernext="nextSlideNine" ></mob-btn-orange>
    </section>
    <section class="title">Кто убедился в нас?</section>
    <section class="ten">
        <div class="wrap">
            <div class="element">
                <img src="https://instagram.fdnk1-2.fna.fbcdn.net/vp/19a40ff72a784f8ba3359349bd5846c9/5C1E4AD0/t51.2885-19/s150x150/39172234_280416069229416_4578834041213550592_n.jpg" alt="">
            </div>
            <div class="element">
                <img src="https://instagram.fdnk1-2.fna.fbcdn.net/vp/442945fbc8e0504e1f19a2396126d234/5C1FA16A/t51.2885-19/s150x150/19050447_446120199098754_2822643251276677120_a.jpg" alt="">
            </div>
            <div class="element">
                <img src="https://instagram.fdnk1-2.fna.fbcdn.net/vp/01490e2c762ab5b186b28122afbd9346/5C301E46/t51.2885-19/s150x150/36476357_242708896329337_265590821850447872_n.jpg" alt="">
            </div>
            <div class="element">
                <img src="https://instagram.fdnk1-2.fna.fbcdn.net/vp/3fe0106393e6cff2d50329132c617cf1/5C256B26/t51.2885-19/s150x150/30084745_1207774332698548_2665854881725677568_n.jpg" alt="">
            </div>
            <div class="element">
                <img src="https://instagram.fdnk1-2.fna.fbcdn.net/vp/c4c29120f3877c35ca6d64609dd134ed/5C2B4454/t51.2885-19/s150x150/40034196_231383627537366_8239765040773726208_n.jpg" alt="">
            </div>
            <div class="element">
                <img src="https://instagram.fdnk1-2.fna.fbcdn.net/vp/87e2c37e0740383a3221aca898f480c6/5C3993FC/t51.2885-19/s150x150/35000571_651956498486102_3288428108020973568_n.jpg" alt="">
            </div>
        </div>
    </section>
    <!--<section class="title eleven_bg">Остались вопросы?</section>
    <section class="eleven">
        <div class="bg">
            <div class="circle el1"></div>
            <div class="ug3 el2"></div>
        </div>
        <div class="wrap">
            <accordion>
                <p slot="heading">Plain Accordion</p>
                <div slot="content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam autem culpa, dolores ex hic necessitatibus non, officiis repudiandae suscipit tempora velit vero voluptatem. Alias atque consequuntur cupiditate in ipsa iste labore minus quam qui saepe. Accusantium aperiam delectus dolor dolorum enim, hic neque nobis repellat sapiente, sequi veniam veritatis voluptatum.</div>
            </accordion>
            <accordion>
                <p slot="heading">Plain Accordion</p>
                <div slot="content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur delectus doloremque porro possimus praesentium quae ratione tenetur! Adipisci architecto corporis culpa cupiditate dicta dignissimos, ducimus error esse eum facere hic, impedit iusto minus nisi pariatur perspiciatis tenetur unde voluptate voluptatibus voluptatum. Beatae ea est fugiat, ipsa laborum maxime nam quis.</div>
            </accordion>
            <accordion>
                <p slot="heading">Plain Accordion</p>
                <div slot="content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium ad adipisci assumenda consectetur eius illum ipsam iste, iusto laudantium modi molestias neque nesciunt officiis repellat sit sunt tempora? Accusantium animi architecto atque, dicta ex exercitationem facilis fugit ipsam ipsum iste itaque molestias nesciunt odio odit omnis quod rem vitae. Natus.</div>
            </accordion>
            <accordion>
                <p slot="heading">Plain Accordion</p>
                <div slot="content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem cumque deserunt eaque eius eum exercitationem minima quod repudiandae ut. Accusamus aliquam assumenda autem beatae consectetur distinctio eius esse eum eveniet explicabo inventore iusto laudantium minus molestias mollitia nam nulla perspiciatis porro quaerat quas quis repellat reprehenderit similique, veniam vero voluptate.</div>
            </accordion>
            <accordion>
                <p slot="heading">Plain Accordion</p>
                <div slot="content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet minima non obcaecati sit vel, voluptatum? Ab aliquid aperiam, aut consequatur cumque dicta dolorem eum explicabo fugit hic ipsum iste laborum maxime non officia optio placeat praesentium quam reiciendis repellendus tempore voluptatibus voluptatum. Deserunt ea esse ex itaque placeat sit voluptate.</div>
            </accordion>
        </div>
    </section>-->
    <section class="title twelve_bg">Еще пару слов о нас?</section>
    <section class="twelve">
        <div class="wrap">
            Когда мы начинали IwoBox, мы делали сервис для себя.
            Мы хотели получить больше подписчиков в «Инстаграме», не тратя много времени вручную. Мы разработали специальные инструменты для того, чтобы сделать этот процесс простым и эффективным.
            С помощью IwoBox вы получаете настоящих подписчиков.
            Которым реально нравятся ваши фото и то, что вы делаете.
            И это будут жители вашего города! Мы работаем с людьми, с брендами, с агентствами... со всеми, кому нужны подписчики в «Инстаграме».
            С IwoBox вы быстро станете популярны!

        </div>
    </section>
    <footer>
        <a href="/" class="logo">
            <logo></logo>
        </a>
        <a v-scroll-to="{ el:'#why', offset: -70}" href="#why">Почему IWOBOX</a>
        <a v-scroll-to="{ el:'#who', offset: -70}" href="#who">Кому</a>
        <a v-scroll-to="{ el:'#how', offset: -70}" href="#how">Как начать</a>
        <a v-scroll-to="{ el:'#what', offset: -70}" href="#what">Что получишь</a>
        <a v-scroll-to="{ el:'#price', offset: -70}" href="#price">Цены</a>
        <a v-scroll-to="{ el:'#about', offset: -70}" href="#about">О продукте</a>
        <a href="/signup" class="button">Начать</a>
    </footer>
    <section class="copyright">
        <div class="bg">
            <div class="halfcircle el1"></div>
            <div class="halfug3 el2"></div>
        </div>
        <a href="">Made with <img alt="❤" class="emojioneemoji" width="20" height="20" src="https://cdnjs.cloudflare.com/ajax/libs/emojione/2.1.4/assets/svg/2764.svg"> for people <img alt="✌" width="20" height="20" class="emojioneemoji" src="https://cdnjs.cloudflare.com/ajax/libs/emojione/2.1.4/assets/svg/270c.svg"></a>
    </section>
</div>
<script src="/dist/home.js?v=<?= VERSION ?>"></script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter49414933 = new Ya.Metrika2({
                    id:49414933,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true,
                    ecommerce:"dataLayer"
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://cdn.jsdelivr.net/npm/yandex-metrica-watch/tag.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks2");
</script>
<!-- /Yandex.Metrika counter -->
<?php require_once(APPPATH.'/views/fragments/google-analytics.fragment.php'); ?>
</body>
</html>