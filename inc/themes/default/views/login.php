<?php if (!defined('APP_VERSION')) die("Yo, what's up?"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta name="description" content="<?= site_settings("site_description") ?>">
        <meta name="keywords" content="<?= site_settings("site_keywords") ?>">
        <link rel="icon" href="<?= site_settings("logomark") ? site_settings("logomark") : APPURL."/assets/img/logomark.png" ?>" type="image/x-icon">
        <link rel="shortcut icon" href="<?= site_settings("logomark") ? site_settings("logomark") : APPURL."/assets/img/logomark.png" ?>" type="image/x-icon">
        <meta name="msvalidate.01" content="1375CF06D08B2F0D2503A431783E2AC5" />
        <title><?= __("Login") ?></title>
        <link rel="stylesheet" href="/dist/css/main.css?v=<?=VERSION ?>">
    </head>
</head>
<body>
<div id="app">
    <form  action="<?= APPURL."/login" ?>" method="POST" autocomplete="off">
        <input type="hidden" name="action" value="login">
        <section class="login">
            <div class="form">
                <div class="left">
                    <a href="/"><logo class="logo"></logo></a>
                    <p class="p1">Введите данные для входа</p>
                    <?php if($Integrations->get("data.facebook.app_id") && $Integrations->get("data.facebook.app_secret")): ?>
                        <a class="fbloginbtn" @click="fblogin1">
                            <svg class="fb" viewBox="0 0 60.734 60.733" style="enable-background:new 0 0 60.734 60.733;" xml:space="preserve">
<g><path d="M57.378,0.001H3.352C1.502,0.001,0,1.5,0,3.353v54.026c0,1.853,1.502,3.354,3.352,3.354h29.086V37.214h-7.914v-9.167h7.914
		v-6.76c0-7.843,4.789-12.116,11.787-12.116c3.355,0,6.232,0.251,7.071,0.36v8.198l-4.854,0.002c-3.805,0-4.539,1.809-4.539,4.462
		v5.851h9.078l-1.187,9.166h-7.892v23.52h15.475c1.852,0,3.355-1.503,3.355-3.351V3.351C60.731,1.5,59.23,0.001,57.378,0.001z"/>
</g></svg>
                            <?= __("Login with Facebook") ?>
                        </a>
                    <?php endif; ?>
                    <input name="username" type="text" class="input" placeholder="Ваш E-mail" value="<?= htmlchars(Input::Post("username")) ?>">
                    <input name="password" type="password" class="input" placeholder="Ваш Пароль">
                    <input type="hidden" name="remember" value="1" <?= Input::post("remember") ? "checked" :"" ?>>
                    <div class="row">
                        <button class="purpure" type="submit">Войти</button>
                        <button type="button" @click.prevent="goTo('/signup')">Начать</button>
                    </div>
                    <p class="un">Нет аккаунта? Нажмите "Начать"</p>
                </div>
                <div class="right">
                    <div class="slider">
                        <swiper :options="{loop:true, autoplay:{delay: 5000}, pagination: {el: '.dots',  type: 'custom', clickable: true, renderCustom: pagination}}" ref="loginslider" >
                            <swiper-slide>
                                <div class="slide">
                                    <div class="img">
                                        <img src="/dist/img/sl2.png" alt="">
                                    </div>
                                </div>
                            </swiper-slide>
                            <swiper-slide>
                                <div class="slide">
                                    <notifys></notifys>
                                </div>
                            </swiper-slide>
                            <swiper-slide>
                                <div class="slide">
                                    <div class="img">
                                        <img src="/dist/img/sl3.png" alt="">
                                    </div>
                                </div>
                            </swiper-slide>
                        </swiper>
                    </div>
                    <div class="dots"></div>
                    <div class="text">
                        <p class="title">Развитие с IWOBOX: </p>
                        <p>апрпоарполв вопалпр оаывпроларолв ыраолврыоларволыар олвыраолвраолывраовы ларолыварол ывоапраывра оывлар овыаролвыраол орывларолвыр.</p>
                    </div>
                </div>
            </div>
        </section>
    </form>
</div>
<script src="/dist/home.js?v=<?=VERSION ?>"></script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter49414933 = new Ya.Metrika2({
                    id:49414933,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true,
                    ecommerce:"dataLayer"
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://cdn.jsdelivr.net/npm/yandex-metrica-watch/tag.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks2");
</script>
<!-- /Yandex.Metrika counter -->
<?php require_once(APPPATH.'/views/fragments/google-analytics.fragment.php'); ?>
<script>
    function toslide(i) {
        let mySwiper = document.querySelector('.swiper-container').swiper;
        mySwiper.slideTo(i);
    }
</script>
</body>
</html>