<?php if (!defined('APP_VERSION')) die("Yo, what's up?"); ?>

<div class="skeleton skeleton--full">
    <div class="clearfix">
        <aside class="skeleton-aside hide-on-medium-and-down">
            <div class="aside-list js-loadmore-content" data-loadmore-id="1"></div>

            <div class="loadmore pt-20 none">
                <a class="fluid button button--light-outline js-loadmore-btn js-autoloadmore-btn" data-loadmore-id="1" href="<?= APPURL."/e/".$idname."?aid=".$Account->get("id")."&ref=schedule" ?>">
                    <span class="icon sli sli-refresh"></span>
                    <?= __("Load More") ?>
                </a>
            </div>
        </aside>

        <section class="skeleton-content">
            <form class="js-auto-like-schedule-form"
                  action="<?= APPURL."/e/".$idname."/".$Account->get("id") ?>"
                  method="POST">

                <input type="hidden" name="action" value="save">

                <div class="section-header clearfix">
                    <h2 class="section-title">
                        <?= htmlchars($Account->get("username")) ?>
                        <?php if ($Account->get("login_required")): ?>
                            <small class="color-danger ml-15">
                                <span class="mdi mdi-information"></span>    
                                <?= __("Re-login required!") ?>
                            </small>
                        <?php endif ?>    
                    </h2>
                </div>

                <div class="al-tab-heads clearfix">
                    <a href="<?= APPURL."/e/".$idname."/".$Account->get("id") ?>" class="active"><?= __("Цели и Настройки") ?></a>
                    <a href="<?= APPURL."/e/".$idname."/".$Account->get("id")."/log" ?>"><?= __("Сводка активности") ?></a>
                </div>

                <div class="section-content" style="max-width: 100%;">
                    <div class="form-result mb-25" style="display:none;"></div>

                    <div class="clearfix">
                        <div class="col s12 m10 l9">
                            <div class="mb-5 clearfix">
                                <label class="inline-block mr-50 mb-15">
                                    <input class="radio" name='type' type="radio" value="hashtag" checked>
                                    <span>
                                        <span class="icon"></span>
                                        #<?= __("Hashtags") ?>
                                    </span>
                                </label>

                                <label class="inline-block mr-50 mb-15">
                                    <input class="radio" name='type' type="radio" value="location">
                                    <span>
                                        <span class="icon"></span>
                                        <?= __("Места") ?>
                                    </span>
                                </label>

                                <label class="inline-block mr-50 mb-15">
                                    <input class="radio" name='type' type="radio" value="people">
                                    <span>
                                        <span class="icon"></span>
                                        <?= __("Люди") ?>
                                    </span>
                                </label>
                                <label class="inline-block mr-50 mb-15">
                                    <input class="radio" name='type' type="radio" value="peoplecon">
                                    <span>
                                        <span class="icon"></span>
                                        <?= __("Подписчики конкурента") ?>
                                    </span>
                                </label>
                                <label class="inline-block mb-15 rad" >
                                    <input class="checkbox" name='filters.myFollorers' type="checkbox" value="1" <?= $Schedule->get("filters.myFollorers") ? "checked" : "" ?>>
                                    <span>
                                                <span class="icon unchecked">
                                                    <span class="mdi mdi-check"></span>
                                                </span>
                                        <?= __('Мои подписчики') ?>
                                            </span>
                                </label>
                            </div>

                            <div class="clearfix mb-20 pos-r">
                                <label class="form-label"><?= __('Search') ?></label>
                                <input class="input rightpad" name="search"  type="text" value="" 
                                       data-url="<?= APPURL."/e/".$idname."/".$Account->get("id") ?>">
                                <span class="field-icon--right pe-none none js-search-loading-icon">
                                    <img src="<?= APPURL."/assets/img/round-loading.svg" ?>" alt="Loading icon">
                                </span>
                            </div>

                            <div class="tags clearfix mt-20 mb-20">
                                <?php 
                                    $targets = $Schedule->isAvailable()
                                             ? json_decode($Schedule->get("target")) 
                                             : []; 
                                    $icons = [
                                        "hashtag" => "mdi mdi-pound",
                                        "location" => "mdi mdi-map-marker",
                                        "people" => "mdi mdi-instagram",
                                        "peoplecon" => "mdi mdi-account-multiple"
                                    ];
                                ?>
                                <?php foreach ($targets as $t): ?>
                                    <span class="tag pull-left"
                                          data-type="<?= htmlchars($t->type) ?>" 
                                          data-id="<?= htmlchars($t->id) ?>" 
                                          data-value="<?= htmlchars($t->value) ?>" 
                                          style="margin: 0px 2px 3px 0px;">
                                        <?php if (isset($icons[$t->type])): ?>
                                            <span class="<?= $icons[$t->type] ?>"></span>
                                        <?php endif ?>  

                                        <?= htmlchars($t->value) ?>
                                        <span class="mdi mdi-close remove"></span>
                                      </span>
                                <?php endforeach ?>
                            </div>
                            <style>
                                .rad > span{
                                    position: relative;
                                    display: inline-block;
                                    padding-left: 38px;
                                    font-size: 14px;
                                    line-height: 17px;
                                    cursor: pointer;
                                    -webkit-user-select: none;
                                    -moz-user-select: none;
                                    -ms-user-select: none;
                                    user-select: none;
                                    color: #212121;
                                }
                                .rad .checkbox + span .icon {
                                    color: #3b7cff;
                                    border-color: #3b7cff;
                                }
                                .vue-input-tag-wrapper .input-tag{
                                    position: relative;
                                    display: inline-block;
                                    padding: 2px 20px 2px 5px;
                                    max-width: 120px;
                                    height: 20px;
                                    font-size: 12px;
                                    line-height: 20px;
                                    background-color: #212121;
                                    color: #fff;
                                    white-space: nowrap;
                                    overflow: hidden;
                                    text-overflow: ellipsis;
                                    border-radius: 2px;
                                    transition: all ease 0.2s;
                                    border: none;
                                }
                                .vue-input-tag-wrapper .input-tag .remove{
                                    color: #fff;
                                    position: absolute;
                                    top: 2px;
                                    right: 5px;
                                    font-size: 10px;
                                    cursor: pointer;
                                    font-family: "Material Design Icons";
                                    font-weight: normal;
                                }
                                .vue-input-tag-wrapper .input-tag .remove:before {
                                    content: "\F156";
                                }
                                .mini-input{
                                    width: 50px;
                                    display: inline;
                                    padding: 0px 9px;
                                    height: 30px;
                                }
                                input.mini-input::-webkit-outer-spin-button,
                                input.mini-input::-webkit-inner-spin-button {
                                    /* display: none; <- Crashes Chrome on hover */
                                    -webkit-appearance: none;
                                    margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
                                }
                            </style>
                            <div class="clearfix mb-40 filters">
                                <div class="">
                                    <label>
                                        <input type="checkbox"
                                               class="checkbox"
                                               name="filters.hasAvatar"
                                               value="1"
                                            <?= $Schedule->get("filters.hasAvatar") ? "checked" : "" ?>>
                                        <span>
                                                <span class="icon unchecked">
                                                    <span class="mdi mdi-check"></span>
                                                </span>
                                            <?= __('Есть аватар') ?>
                                            </span>
                                    </label>
                                </div>
                            </div>
                            <div class="clearfix mb-40 filters">
                                <div class="">
                                    <label>
                                        <input type="checkbox"
                                               class="checkbox"
                                               name="filters.isNonPrivate"
                                               value="1"
                                            <?= $Schedule->get("filters.isNonPrivate") ? "checked" : "" ?>>
                                        <span>
                                                <span class="icon unchecked">
                                                    <span class="mdi mdi-check"></span>
                                                </span>
                                            <?= __('Не лайкать приватные акаунты') ?>
                                            </span>
                                    </label>
                                </div>
                            </div>
                            <div class="clearfix mb-40 filters">
                                <div class="">
                                    <label>
                                        <input type="checkbox"
                                               class="checkbox"
                                               name="filters.isNonDescription"
                                               value="1"
                                            <?= $Schedule->get("filters.isNonDescription") ? "checked" : "" ?>>
                                        <span>
                                                <span class="icon unchecked">
                                                    <span class="mdi mdi-check"></span>
                                                </span>
                                            <?= __('Не лайкать акаунты без описания') ?>
                                            </span>
                                    </label>
                                </div>
                            </div>
                            <div class="clearfix mb-40 filters">
                                <div class="">
                                    <label>
                                        <input type="checkbox"
                                               class="checkbox"
                                               name="filters.dontHaveLinkInBiography"
                                               value="1"
                                            <?= $Schedule->get("filters.dontHaveLinkInBiography") ? "checked" : "" ?>>
                                        <span>
                                                <span class="icon unchecked">
                                                    <span class="mdi mdi-check"></span>
                                                </span>
                                            <?= __('Без ссылок в описании') ?>
                                            </span>
                                    </label>
                                </div>
                            </div>
                            <div class="clearfix mb-40 filters">
                                <div class="">
                                    <label style="margin-left: 20px;">
                                        <input type="checkbox"
                                               class="checkbox"
                                               name="filters.canSocialLinkInBiography"
                                               value="1"
                                            <?= $Schedule->get("filters.canSocialLinkInBiography") ? "checked" : "" ?>>
                                        <span>
                                                <span class="icon unchecked">
                                                    <span class="mdi mdi-check"></span>
                                                </span>
                                            <?= __('Не учитывать ссылки на соцсети') ?>
                                            </span>
                                    </label>
                                </div>
                            </div>
                            <div class="clearfix mb-40 filters">
                                <div class="">
                                    <label>
                                        <input type="checkbox"
                                               class="checkbox"
                                               name="filters.isLastPublish"
                                               value="1"
                                            <?= $Schedule->get("filters.isLastPublish") ? "checked" : "" ?>>
                                        <span>
                                                <span class="icon unchecked">
                                                    <span class="mdi mdi-check"></span>
                                                </span>
                                            <?= __('Последняя публикация размещена более') ?>

                                            </span>
                                    </label>
                                    <input class="input mini-input" type="number"  name="filters.isLastPublishDay" value="<?= $Schedule->get("filters.isLastPublishDay") ? $Schedule->get("filters.isLastPublishDay") : 3 ?>">
                                    <span>дней назад</span>
                                </div>
                            </div>
                            <div class="clearfix mb-40 filters">
                                <div class="">
                                    <label>
                                        <input type="checkbox"
                                               class="checkbox"
                                               name="filters.isCountPosts"
                                               value="1"
                                            <?= $Schedule->get("filters.isCountPosts") ? "checked" : "" ?>>
                                        <span>
                                                <span class="icon unchecked">
                                                    <span class="mdi mdi-check"></span>
                                                </span>
                                            <?= __('Количество публикаций в которых от') ?>

                                            </span>
                                    </label>
                                    <input class="input mini-input" type="number"  name="filters.isCountPostsMin" value="<?= $Schedule->get("filters.isCountPostsMin") ? $Schedule->get("filters.isCountPostsMin") : 20 ?>">
                                    <span>до</span>
                                    <input class="input mini-input" type="number"  name="filters.isCountPostsMax" value="<?= $Schedule->get("filters.isCountPostsMax") ? $Schedule->get("filters.isCountPostsMax") : 500 ?>">
                                </div>
                            </div>
                            <div class="clearfix mb-40 filters">
                                <div class="">
                                    <label>
                                        <input type="checkbox"
                                               class="checkbox"
                                               name="filters.isCountFollowers"
                                               value="1"
                                            <?= $Schedule->get("filters.isCountFollowers") ? "checked" : "" ?>>
                                        <span>
                                                <span class="icon unchecked">
                                                    <span class="mdi mdi-check"></span>
                                                </span>
                                            <?= __('Количество подписчиков в которых от') ?>

                                            </span>
                                    </label>
                                    <input class="input mini-input" type="number"  name="filters.isCountFollowersMin" value="<?= $Schedule->get("filters.isCountFollowersMin") ? $Schedule->get("filters.isCountFollowersMin") : 50 ?>">
                                    <span>до</span>
                                    <input class="input mini-input" type="number"  name="filters.isCountFollowersMax" value="<?= $Schedule->get("filters.isCountFollowersMax") ? $Schedule->get("filters.isCountFollowersMax") : 500 ?>">
                                </div>
                            </div>
                            <div class="clearfix mb-40 filters">
                                <div class="">
                                    <label>
                                        <input type="checkbox"
                                               class="checkbox"
                                               name="filters.isCountFollowing"
                                               value="1"
                                            <?= $Schedule->get("filters.isCountFollowing") ? "checked" : "" ?>>
                                        <span>
                                                <span class="icon unchecked">
                                                    <span class="mdi mdi-check"></span>
                                                </span>
                                            <?= __('Количество подписок в которых') ?>

                                            </span>
                                    </label>
                                    <input class="input mini-input" type="number"  name="filters.isCountFollowingMin" value="<?= $Schedule->get("filters.isCountFollowingMin") ? $Schedule->get("filters.isCountFollowingMin") : 50 ?>">
                                    <span>до</span>
                                    <input class="input mini-input" type="number"  name="filters.isCountFollowingMax" value="<?= $Schedule->get("filters.isCountFollowingMax") ? $Schedule->get("filters.isCountFollowingMax") : 500?>">
                                </div>
                            </div>
                            <div class="clearfix mb-40 filters">
                                <div class="bad-wrapper" >
                                    <div class="">
                                        <label>
                                            <input type="checkbox"
                                                   class="checkbox"
                                                   name="filters.isNonWord"
                                                   value="1"
                                                   v-model="first"
                                                <?= $Schedule->get("filters.isNonWord") ? "checked" : "" ?>>
                                            <span>
                                                <span class="icon unchecked">
                                                    <span class="mdi mdi-check"></span>
                                                </span>
                                                <?= __('Не лайкать профили со словами в описании') ?>
                                            </span>
                                        </label>
                                        <select name="filters.isNonWordType" class="input" v-model="second" v-show="first">
                                            <option value="ai" <?= $Schedule->get('filters.isNonWordType') == "ai" ? 'selected' : ''?>>Список "Смарт AI"</option>
                                            <option value="my" <?= $Schedule->get('filters.isNonWordType') == "my" ? 'selected' : ''?>>Свой список</option>
                                        </select>
                                        <input-tag v-show="first && (second == 'my')"
                                                   :add-tag-on-keys="[13, 32, 9]"
                                                   :tags.sync="tags"
                                                   :placeholder="'Введите слова'"></input-tag>
                                        <?php
                                        $Settings = $this->getVariable("Settings");
                                        if($Schedule->get("filters") && $Schedule->get("filters")!='null' && property_exists(json_decode($Schedule->get("filters")),'badWords') && !!$Schedule->get("filters.badWords") && count(json_decode($Schedule->get("filters.badWords")))){
                                            $q = json_encode($Schedule->get("filters.badWords"));
                                        }else{
                                            $q = json_encode([]);
                                        }
                                        ?>

                                        <input type="hidden" name="l" value='<?= $q ?>'>
                                        <input type="hidden" name="filters.badWords" v-bind:value="badWords">
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix mb-40 filters">
                                <div class="good-wrapper">
                                    <div class="">
                                        <label>
                                            <input type="checkbox"
                                                   class="checkbox"
                                                   name="filters.isYesWord"
                                                   value="1"
                                                <?= $Schedule->get("filters.isYesWord") ? "checked" : "" ?>>
                                            <span>
                                                <span class="icon unchecked">
                                                    <span class="mdi mdi-check"></span>
                                                </span>
                                                <?= __('Лайкать профили со словами в описании') ?>
                                            </span>
                                        </label>
                                    <input-tag :add-tag-on-keys="[13, 32, 9]"
                                            :tags.sync="tags"
                                               :placeholder="'Введите слова'"></input-tag>
                                        <?php
                                        if($Schedule->get("filters") && $Schedule->get("filters")!='null' && property_exists(json_decode($Schedule->get("filters")), 'goodWords') && !!$Schedule->get("filters.goodWords") && count(json_decode($Schedule->get("filters.goodWords")))){
                                            $q = json_encode($Schedule->get("filters.goodWords"));
                                        }else{
                                            $q = json_encode($Settings->get("data.default.goodWords"));
                                            if($q == 'null'){
                                                $q = json_encode([]);
                                            }
                                        }
                                        ?>
                                        <input type="hidden" name="ll" value='<?= $q ?>'>
                                    <input type="hidden" name="filters.goodWords" v-bind:value="badWords">
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix mb-40">
                                <div class="col s6 m6 l6">
                                    <label class="form-label"><?= __("Speed") ?></label>

                                    <select class="input" name="speed">
                                        <option value="auto" <?= $Schedule->get("speed") == "auto" ? "selected" : "" ?>><?= __("Auto"). " (".__("Recommended").")" ?></option>
                                        <option value="very_slow" <?= $Schedule->get("speed") == "very_slow" ? "selected" : "" ?>><?= __("Very Slow") ?></option>
                                        <option value="slow" <?= $Schedule->get("speed") == "slow" ? "selected" : "" ?>><?= __("Slow") ?></option>
                                        <option value="medium" <?= $Schedule->get("speed") == "medium" ? "selected" : "" ?>><?= __("Medium") ?></option>
                                        <option value="fast" <?= $Schedule->get("speed") == "fast" ? "selected" : "" ?>><?= __("Fast") ?></option>
                                        <option value="very_fast" <?= $Schedule->get("speed") == "very_fast" ? "selected" : "" ?>><?= __("Very Fast") ?></option>
                                    </select>
                                </div>

                                <div class="col s6 s-last m6 m-last l6 l-last">
                                    <label class="form-label"><?= __("Status") ?></label>

                                    <select class="input" name="is_active">
                                        <option value="0" <?= $Schedule->get("is_active") == 0 ? "selected" : "" ?>><?= __("Deactive") ?></option>
                                        <option value="1" <?= $Schedule->get("is_active") == 1 ? "selected" : "" ?>><?= __("Active") ?></option>
                                    </select>
                                </div>
                            </div>

                            <div class="clearfix">
                                <div class="col s12 m6 l6">
                                    <div class="mb-20">
                                        <label>
                                            <input type="checkbox" 
                                                   class="checkbox" 
                                                   name="daily-pause" 
                                                   value="1"
                                                   <?= $Schedule->get("daily_pause") ? "checked" : "" ?>>
                                            <span>
                                                <span class="icon unchecked">
                                                    <span class="mdi mdi-check"></span>
                                                </span>
                                                <?= __('Ежедневная пауза в подписках') ?> ...
                                            </span>
                                        </label>
                                    </div>

                                    <div class="clearfix mb-20 js-daily-pause-range">
                                        <?php $timeformat = $AuthUser->get("preferences.timeformat") == "12" ? 12 : 24; ?>

                                        <div class="col s6 m6 l6">
                                            <label class="form-label"><?= __("From") ?></label>

                                            <?php 
                                                $from = new \DateTime(date("Y-m-d")." ".$Schedule->get("daily_pause_from"));
                                                $from->setTimezone(new \DateTimeZone($AuthUser->get("preferences.timezone")));
                                                $from = $from->format("H:i");
                                            ?>

                                            <select class="input" name="daily-pause-from">
                                                <?php for ($i=0; $i<=23; $i++): ?>
                                                    <?php $time = str_pad($i, 2, "0", STR_PAD_LEFT).":00"; ?>
                                                    <option value="<?= $time ?>" <?= $from == $time ? "selected" : "" ?>>
                                                        <?= $timeformat == 24 ? $time : date("h:iA", strtotime(date("Y-m-d")." ".$time)) ?>    
                                                    </option>
                                                    
                                                    <?php $time = str_pad($i, 2, "0", STR_PAD_LEFT).":30"; ?>
                                                    <option value="<?= $time ?>" <?= $from == $time ? "selected" : "" ?>>
                                                        <?= $timeformat == 24 ? $time : date("h:iA", strtotime(date("Y-m-d")." ".$time)) ?>    
                                                    </option>
                                                <?php endfor; ?>
                                            </select>
                                        </div>

                                        <div class="col s6 s-last m6 m-last l6 l-last">
                                            <label class="form-label"><?= __("To") ?></label>

                                            <?php 
                                                $to = new \DateTime(date("Y-m-d")." ".$Schedule->get("daily_pause_to"));
                                                $to->setTimezone(new \DateTimeZone($AuthUser->get("preferences.timezone")));
                                                $to = $to->format("H:i");
                                            ?>

                                            <select class="input" name="daily-pause-to">
                                                <?php for ($i=0; $i<=23; $i++): ?>
                                                    <?php $time = str_pad($i, 2, "0", STR_PAD_LEFT).":00"; ?>
                                                    <option value="<?= $time ?>" <?= $to == $time ? "selected" : "" ?>>
                                                        <?= $timeformat == 24 ? $time : date("h:iA", strtotime(date("Y-m-d")." ".$time)) ?>    
                                                    </option>
                                                    
                                                    <?php $time = str_pad($i, 2, "0", STR_PAD_LEFT).":30"; ?>
                                                    <option value="<?= $time ?>" <?= $to == $time ? "selected" : "" ?>>
                                                        <?= $timeformat == 24 ? $time : date("h:iA", strtotime(date("Y-m-d")." ".$time)) ?>    
                                                    </option>
                                                <?php endfor; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col s12 m6 m-last l6 l-last">
                                    <div class="mb-20">
                                        <label>
                                            <input type="checkbox" 
                                                   class="checkbox" 
                                                   name="timeline-feed" 
                                                   value="1"
                                                   <?= $Schedule->get("timeline_feed.enabled") ? "checked" : "" ?>>
                                            <span>
                                                <span class="icon unchecked">
                                                    <span class="mdi mdi-check"></span>
                                                </span>
                                                <?= __("Like the feed from the timeline") ?>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            </div>


                            <div class="clearfix mt-20">
                                <div class="col s12 m6 l6">
                                    <input class="fluid button" type="submit" value="<?= __("Save") ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div>


</div>