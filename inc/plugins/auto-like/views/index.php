<?php if (!defined('APP_VERSION')) die("Yo, what's up?");  ?>
<!DOCTYPE html>
<html lang="<?= ACTIVE_LANG ?>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta name="theme-color" content="#fff">

        <meta name="description" content="<?= site_settings("site_description") ?>">
        <meta name="keywords" content="<?= site_settings("site_keywords") ?>">

        <link rel="icon" href="<?= site_settings("logomark") ? site_settings("logomark") : APPURL."/assets/img/logomark.png" ?>" type="image/x-icon">
        <link rel="shortcut icon" href="<?= site_settings("logomark") ? site_settings("logomark") : APPURL."/assets/img/logomark.png" ?>" type="image/x-icon">

        <link rel="stylesheet" type="text/css" href="<?= APPURL."/assets/css/plugins.css?v=".VERSION ?>">
        <link rel="stylesheet" type="text/css" href="<?= APPURL."/assets/css/core.css?v=".VERSION ?>">
        <link rel="stylesheet" type="text/css" href="<?= PLUGINS_URL."/".$idname."/assets/css/core.css?v=20".VERSION ?>">
        <script src="https://wchat.freshchat.com/js/widget.js"></script>
        <title><?= __("Auto Like") ?></title>
    </head>

    <body>
        <?php 
            $Nav = new stdClass;
            $Nav->activeMenu = $idname;
            require_once(APPPATH.'/views/fragments/navigation.fragment.php');
        ?>

        <?php 
            $TopBar = new stdClass;
            $TopBar->title = __("Auto Like");
            $TopBar->btn = false;
            require_once(APPPATH.'/views/fragments/topbar.fragment.php'); 
        ?>

        <?php require_once(__DIR__.'/fragments/index.fragment.php'); ?>
        
        <script type="text/javascript" src="<?= APPURL."/assets/js/plugins.js?v=".VERSION ?>"></script>
        <?php require_once(APPPATH.'/inc/js-locale.inc.php'); ?>
        <script type="text/javascript" src="<?= APPURL."/assets/js/core.js?v=".VERSION ?>"></script>
        <script type="text/javascript" src="<?= PLUGINS_URL."/".$idname."/assets/js/core.js?v=20".VERSION ?>"></script>
        <script type="text/javascript" charset="utf-8">
            $(function(){
                AutoLike.Index();
            })
        </script>
        <script>
            window.fcWidget.init({
                token: "bc46838e-7dca-40ed-8a44-b6be18f0a0e5",
                host: "https://wchat.freshchat.com"
            });
        </script>
        <!-- GOOGLE ANALYTICS -->
        <?php require_once(APPPATH.'/views/fragments/google-analytics.fragment.php'); ?>
        <script src="https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js"></script>
        <script src="https://unpkg.com/vue-input-tag@1.0.4/dist/vue-input-tag.min.js"></script>
        <script>
            Vue.component('input-tag', InputTag);
            $(document).on('paste', '.bad-wrapper .new-tag', (e)=>{
                let text =  e.originalEvent.clipboardData.getData('text');
                e.preventDefault();
                e.stopPropagation();
                let arraytext = text.split(/(,|\n| |↵|"|\/n)/);
                let u = arraytext.map((item)=>{
                    return item.replace('[', '').replace(' ', '').replace(']', '').replace('.', '').replace(',', '').replace('"', '').replace("'", '').replace(":", '').replace(";", '').replace("↵", '').replace(/[\n\r]/g, '')
                });
                u = u.filter((item)=>{return !(item == '' || item == ' ' || item == ',' || item == '"' || item == '[' || item == '↵' )});
                u.forEach((item)=>{
                    if(window.bad.tags.indexOf(item) == -1){
                        window.bad.tags.push(item)
                    }
                });
            });
            $(document).on('paste', '.good-wrapper .new-tag', (e)=>{
                let text =  e.originalEvent.clipboardData.getData('text');
                e.preventDefault();
                e.stopPropagation();
                let arraytext = text.split(/(,|\n| |↵|"|\/n)/);
                let u = arraytext.map((item)=>{
                    return item.replace('[', '').replace(' ', '').replace(']', '').replace('.', '').replace(',', '').replace('"', '').replace("'", '').replace(":", '').replace(";", '').replace("↵", '').replace(/[\n\r]/g, '')
                });
                u = u.filter((item)=>{return !(item == '' || item == ' ' || item == ',' || item == '"' || item == '[' || item == '↵' )});
                u.forEach((item)=>{
                    if(window.good.tags.indexOf(item) == -1){
                        window.good.tags.push(item)
                    }
                });
            });
            function start(){
                var b = JSON.parse($('input[name=l]').val());
                if(typeof(b) == 'string'){
                    b = JSON.parse(b);
                }
                var g = JSON.parse($('input[name=ll]').val());
                if(typeof(g) == 'string'){
                    g = JSON.parse(g);
                }
                window.bad = new Vue({
                    el: '.bad-wrapper',
                    data: {
                        tags:b,
                        first:$('input[name="filters.isNonWord"]')[0].checked,
                        second:$('[name="filters.isNonWordType"]').val(),
                    },
                    computed:{
                        badWords(){
                            return JSON.stringify(this.tags);
                        }
                    },
                    methods:{
                        changeTag(newTags){
                            this.tags = newTags;
                        }
                    }
                });
                window.good = new Vue({
                    el: '.good-wrapper',
                    data: {
                        tags:g,
                    },
                    computed:{
                        badWords(){
                            return JSON.stringify(this.tags);
                        }
                    },
                    methods:{
                        changeTag(newTags){
                            this.tags = newTags;
                        }
                    }
                })
            }

            $(window).on('pushStateAPI', function(e) {
                start();
            });
            start();
        </script>
    </body>
</html>