<?php if (!defined('APP_VERSION')) die("Yo, what's up?"); ?>

<div class="skeleton skeleton--full" id="welcomedm-schedule">
    <div class="clearfix">
        <aside class="skeleton-aside hide-on-medium-and-down">
            <div class="aside-list js-loadmore-content" data-loadmore-id="1"></div>

            <div class="loadmore pt-20 none">
                <a class="fluid button button--light-outline js-loadmore-btn js-autoloadmore-btn" data-loadmore-id="1" href="<?= APPURL."/e/".$idname."?aid=".$Account->get("id") ?>">
                    <span class="icon sli sli-refresh"></span>
                    <?= __("Load More") ?>
                </a>
            </div>
        </aside>

        <section class="skeleton-content">
            <form class="js-welcomedm-schedule-form"
                  action="<?= APPURL."/e/".$idname."/".$Account->get("id") ?>"
                  method="POST">

                <input type="hidden" name="action" value="save">

                <div class="section-header clearfix">
                    <h2 class="section-title"><?= htmlchars($Account->get("username")) ?></h2>
                </div>

                <div class="wdm-tab-heads clearfix">
                    <a href="javascript:void(0)" class="active" data-id="settings"><?= __("Settings") ?></a>
                    <a href="javascript:void(0)" data-id="messages"><?= __("Messages") ?></a>
                </div>

                <div class="section-content" style="max-width: 100%;">
                    <div class="form-result mb-25"></div>
                    <style>
                        .vue-input-tag-wrapper .input-tag{
                            position: relative;
                            display: inline-block;
                            padding: 2px 20px 2px 5px;
                            max-width: 120px;
                            height: 20px;
                            font-size: 12px;
                            line-height: 20px;
                            background-color: #212121;
                            color: #fff;
                            white-space: nowrap;
                            overflow: hidden;
                            text-overflow: ellipsis;
                            border-radius: 2px;
                            transition: all ease 0.2s;
                            border: none;
                        }
                        .vue-input-tag-wrapper .input-tag .remove{
                            color: #fff;
                            position: absolute;
                            top: 2px;
                            right: 5px;
                            font-size: 10px;
                            cursor: pointer;
                            font-family: "Material Design Icons";
                            font-weight: normal;
                        }
                        .vue-input-tag-wrapper .input-tag .remove:before {
                            content: "\F156";
                        }
                        .mini-input{
                            width: 50px;
                            display: inline;
                            padding: 0px 9px;
                            height: 30px;
                        }
                        input.mini-input::-webkit-outer-spin-button,
                        input.mini-input::-webkit-inner-spin-button {
                            /* display: none; <- Crashes Chrome on hover */
                            -webkit-appearance: none;
                            margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
                        }
                    </style>
                    <div id="directPremium"></div>


                </div>
            </form>
        </section>
    </div>
</div>