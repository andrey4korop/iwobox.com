import Vue from 'vue'
import { Container, Draggable } from "vue-smooth-dnd";

import axios from 'axios'
import VueAxios from 'vue-axios'
import InputTag from 'vue-input-tag'
/*import Notifications from 'vue-notification'*/
window.querystring = require('querystring');

import App from './App.vue'

import listItem from './components/listItem.vue'
import editItem from './components/editItem.vue'
import deleteMessage from './components/deleteMessage.vue'
import deleteTask from './components/deleteTask.vue'
Vue.component('listItem', listItem);
Vue.component('editItem', editItem);
Vue.component('deleteMessage', deleteMessage);
Vue.component('deleteTask', deleteTask);
Vue.component('Container', Container);
Vue.component('Draggable', Draggable);

Vue.component('input-tag', InputTag);
Vue.use(VueAxios, axios);
/*Vue.use(Notifications);*/

var _ = require('lodash');
new Vue({
  el: '#directPremium',
  render: h => h(App)
})
