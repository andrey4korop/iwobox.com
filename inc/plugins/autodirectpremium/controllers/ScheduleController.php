<?php
namespace Plugins\AutoDirectPremium;

// Disable direct access
if (!defined('APP_VERSION')) 
    die("Yo, what's up?");

/**
 * Schedule Controller
 */
class ScheduleController extends \Controller
{
    /**
     * Process
     */
    public function process()
    {
        $AuthUser = $this->getVariable("AuthUser");
        $Route = $this->getVariable("Route");
        $this->setVariable("idname", "autodirectpremium");

        // Auth
        if (!$AuthUser){
            header("Location: ".APPURL."/login");
            exit;
        } else if ($AuthUser->isExpired()) {
            header("Location: ".APPURL."/expired");
            exit;
        }

        $user_modules = $AuthUser->get("settings.modules");
        if (!is_array($user_modules) || !in_array($this->getVariable("idname"), $user_modules)) {
            // Module is not accessible to this user
            header("Location: ".APPURL."/post");
            exit;
        }


        // Get account
        $Account = \Controller::model("Account", $Route->params->id);
        if (!$Account->isAvailable() || 
            $Account->get("user_id") != $AuthUser->get("id")) 
        {
            header("Location: ".APPURL."/e/".$this->getVariable("idname"));
            exit;
        }
        $this->setVariable("Account", $Account);

        // Get Schedule
        require_once PLUGINS_PATH."/".$this->getVariable("idname")."/models/ScheduleModel.php";
        require_once PLUGINS_PATH."/".$this->getVariable("idname")."/models/SchedulesModel.php";

        if (\Input::post("action") == "delete") {
            $this->delete();
        }
        $Schedule = new SchedulesModel();
        $Schedule->where("account_id" ,'=',$Account->get("id"))->orderBy("id","DESC")->fetchData();

        $this->setVariable("Schedule", $Schedule);

        if (\Input::request("action") == "search") {
            $this->search();
        }
        if(\Input::post('action') == 'getItem'){
            $this->getItem($Schedule);
        }

        if (\Input::post("action") == "save") {
            $this->save();
        }
        if (\Input::post("action") == "delete") {
            $this->delete();
        }
        $this->view(PLUGINS_PATH."/".$this->getVariable("idname")."/views/schedule.php", null);
    }


    /**
     * Save schedule
     * @return mixed 
     */
    private function save()
    {
        $this->resp->result = 0;
        $AuthUser = $this->getVariable("AuthUser");
        $Account = $this->getVariable("Account");
        //$Schedule = $this->getVariable("Schedule");
        $a = json_decode(\Input::post('object'));

        if(property_exists($a, 'id') && $a->id > 0) {
            $Schedule = new ScheduleModel([
                "id" => $a->id,
                "account_id" => $Account->get("id"),
                "user_id" => $Account->get("user_id")
            ]);
        }else{
            $Schedule = new ScheduleModel([
                "account_id" => $Account->get("id"),
                "user_id" => $Account->get("user_id")
            ]);
            $Schedule->set('account_id', $Account->get("id"));
            $Schedule->set('user_id', $Account->get("user_id"));
        }
        // Emojione Client
        $Emojione = new \Emojione\Client(new \Emojione\Ruleset());
        // Messages


        $valid_messages = [];
        if ($a->messages) {
            foreach ($a->messages as $m) {
                $valid_messages[] = trim(preg_replace('/\s{2,}/', ' ', $Emojione->toShort($m)));
            }
        }
        $Schedule->set('messages', json_encode($valid_messages));
        $Schedule->set('targets', json_encode($a->targets));
        // Speed
        $speed = (int)$a->speed;
        if ($speed < 0 || $speed > 5) {
            $speed = 0;
        }
        $Schedule->set('speed', $speed);
        $is_active = $a->is_active ? 1 : 0;
        $Schedule->set('is_active', $is_active);
        $Schedule->set('targets', json_encode($a->targets));
        $Schedule->set('filters', json_encode($a->filters));
        $end_date = count($valid_messages) > 0
            ? "2030-12-12 23:59:59" : date("Y-m-d H:i:s");
        $Schedule->set('end_date', $end_date);
        $Schedule->save();
        $this->resp->msg = __("Changes saved!");
        $this->resp->result = 1;
        $this->jsonecho();
    }

    private function delete()
    {
        $this->resp->result = 0;

        $Schedule = $Schedule = new ScheduleModel((int)\Input::post("id"));
        $Schedule->delete();
        $this->resp->msg = __("Deleted message!");
        $this->resp->result = 1;
        $this->jsonecho();
    }

    private function search()
    {
        $this->resp->result = 0;
        $AuthUser = $this->getVariable("AuthUser");
        $Account = $this->getVariable("Account");

        $query = \Input::request("q");
        if (!$query) {
            $this->resp->msg = __("Missing some of required data.");
            $this->jsonecho();
        }

        $type = \Input::request("type");
        if (!in_array($type, ["hashtag", "location", "people"])) {
            $this->resp->msg = __("Invalid parameter");
            $this->jsonecho();
        }

        // Login

        try {
            $Instagram = \InstagramController::login($Account);
        } catch (\Exception $e) {
            $this->resp->msg = $e->getMessage();
            $this->jsonecho();
        }

        $this->resp->items = [];

        // Get data
        try {
            if ($type == "hashtag") {
                $search_result = $Instagram->hashtag->search($query);
                if ($search_result->isOk()) {
                    foreach ($search_result->getResults() as $r) {
                        $this->resp->items[] = [
                            "value" => $r->getName(),
                            "data" => [
                                "sub" => n__("%s public post", "%s public posts", $r->getMediaCount(), $r->getMediaCount()),
                                "id" => str_replace("#", "", $r->getName())
                            ]
                        ];
                    }
                }
            } else if ($type == "location") {
                $search_result = $Instagram->location->findPlaces($query);
                if ($search_result->isOk()) {
                    foreach ($search_result->getItems() as $r) {
                        $this->resp->items[] = [
                            "value" => $r->getLocation()->getName(),
                            "data" => [
                                "sub" => false,
                                "id" => $r->getLocation()->getFacebookPlacesId()
                            ]
                        ];
                    }
                }
            } else if ($type == "people") {
                $search_result = $Instagram->people->search($query);
                if ($search_result->isOk()) {
                    foreach ($search_result->getUsers() as $r) {
                        $this->resp->items[] = [
                            "value" => $r->getUsername(),
                            "data" => [
                                "sub" => $r->getFullName(),
                                "id" => $r->getPk()
                            ]
                        ];
                    }
                }
            }
        } catch (\Exception $e) {
            $this->resp->msg = $e->getMessage();
            $this->jsonecho();
        }

        $this->resp->result = 1;
        $this->jsonecho();
    }


    private function getItem($Schedule){
        $list = $Schedule->getDataAs( [PLUGINS_PATH."/autodirectpremium/models/ScheduleModel.php", __NAMESPACE__."\ScheduleModel"]);
        $returnData = [];
        $Emojione = new \Emojione\Client(new \Emojione\Ruleset());
        foreach ($list as $item){
            $object = new \stdClass();
            $object->id = $item->get('id');
            $object->user_id = $item->get('user_id');
            $object->account_id = $item->get('account_id');

            $messages = [];
            $messagesbefore = json_decode($item->get('messages'));
            foreach ($messagesbefore as $m) {
                $messages[] = $Emojione->shortnameToUnicode($m);
            }
            $object->messages = $messages;
            $object->targets = json_decode($item->get('targets'));
            $object->filters = json_decode($item->get('filters'));
            $object->speed = $item->get('speed');
            $object->is_active = $item->get('is_active');
            $object->schedule_date = $item->get('schedule_date');
            $object->end_date = $item->get('end_date');
            $object->last_action_date = $item->get('last_action_date');
            array_push($returnData, $object);
        }
        $this->resp->object = $returnData;
        $this->jsonecho();
    }
}
