<?php if (!defined('APP_VERSION')) die("Yo, what's up?"); ?>
<?php 
    return [
        "idname" => "autodirectpremium",
        "plugin_name" => "Auto DM (Premium)",
        "plugin_uri" => "http://spaceweb.io",
        "author" => "Andrey4korop",
        "author_uri" => "http://spaceweb.io",
        "version" => "1.0.0",
        "desc" => "Premium module to send automated direct message to your all followers",
        "icon_style" => "background-color: #2F4858; background: linear-gradient(to right top, #ff0000, #d7357f, #2F4858); color: #fff; font-size: 18px;"
    ];
    