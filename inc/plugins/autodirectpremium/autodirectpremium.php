<?php 
namespace Plugins\AutoDirectPremium;
const IDNAME = "autodirectpremium";

// Disable direct access
if (!defined('APP_VERSION')) 
    die("Yo, what's up?"); 


/**
 * Event: plugin.install
 */
function install($Plugin)
{
    if ($Plugin->get("idname") != IDNAME) {
        return false;
    }

    $sql = "CREATE TABLE `".TABLE_PREFIX.IDNAME."_schedule` ( 
                `id` INT NOT NULL AUTO_INCREMENT , 
                `user_id` INT NOT NULL , 
                `account_id` INT NOT NULL , 
                `message` TEXT NOT NULL,
                `speed` VARCHAR(20) NOT NULL , 
                `is_active` BOOLEAN NOT NULL , 
                `schedule_date` DATETIME NOT NULL , 
                `end_date` DATETIME NOT NULL , 
                `last_action_date` DATETIME NOT NULL , 
                PRIMARY KEY (`id`), 
                INDEX (`user_id`), 
                INDEX (`account_id`)
            ) ENGINE = InnoDB;";

    $sql .= "ALTER TABLE `".TABLE_PREFIX.IDNAME."_schedule` 
                ADD CONSTRAINT `".IDNAME."_ibfk_1` FOREIGN KEY (`user_id`) 
                REFERENCES `".TABLE_PREFIX."users`(`id`) 
                ON DELETE CASCADE ON UPDATE CASCADE;";

    $sql .= "ALTER TABLE `".TABLE_PREFIX."welcomedm_schedule` 
                ADD CONSTRAINT `".IDNAME."_ibfk_2` FOREIGN KEY (`account_id`) 
                REFERENCES `".TABLE_PREFIX."accounts`(`id`) 
                ON DELETE CASCADE ON UPDATE CASCADE;";


    $sql .= "CREATE TABLE `".TABLE_PREFIX.IDNAME."_log` ( 
                `id` INT NOT NULL AUTO_INCREMENT , 
                `user_id` INT NOT NULL , 
                `account_id` INT NOT NULL , 
                `message_id` INT NOT NULL , 
                `status` VARCHAR(20) NOT NULL,
                `follower_id` VARCHAR(50) NOT NULL,
                `data` TEXT NOT NULL , 
                `date` DATETIME NOT NULL , 
                PRIMARY KEY (`id`), 
                INDEX (`user_id`), 
                INDEX (`account_id`),
                INDEX (`follower_id`)
            ) ENGINE = InnoDB;";

    $sql .= "ALTER TABLE `".TABLE_PREFIX.IDNAME."_log` 
                ADD CONSTRAINT `".IDNAME."2_ibfk_1` FOREIGN KEY (`user_id`) 
                REFERENCES `".TABLE_PREFIX."users`(`id`) 
                ON DELETE CASCADE ON UPDATE CASCADE;";

    $sql .= "ALTER TABLE `".TABLE_PREFIX."welcomedm_log` 
                ADD CONSTRAINT `".IDNAME."2_ibfk_2` FOREIGN KEY (`account_id`) 
                REFERENCES `".TABLE_PREFIX."accounts`(`id`) 
                ON DELETE CASCADE ON UPDATE CASCADE;";

    $pdo = \DB::pdo();
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
}
\Event::bind("plugin.install", __NAMESPACE__ . '\install');



/**
 * Event: plugin.remove
 */
function uninstall($Plugin)
{
    if ($Plugin->get("idname") != IDNAME) {
        return false;
    }

    $sql = "DROP TABLE `".TABLE_PREFIX.IDNAME."_schedule`;";
    $sql .= "DROP TABLE `".TABLE_PREFIX.IDNAME."_log`;";

    $pdo = \DB::pdo();
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
}
\Event::bind("plugin.remove", __NAMESPACE__ . '\uninstall');


/**
 * Add module as a package options
 * Only users with granted permission
 * Will be able to use module
 * 
 * @param array $package_modules An array of currently active 
 *                               modules of the package
 */
function add_module_option($package_modules)
{
    $config = include __DIR__."/config.php";
    ?>
        <div class="mt-15">
            <label>
                <input type="checkbox" 
                       class="checkbox" 
                       name="modules[]" 
                       value="<?= IDNAME ?>" 
                       <?= in_array(IDNAME, $package_modules) ? "checked" : "" ?>>
                <span>
                    <span class="icon unchecked">
                        <span class="mdi mdi-check"></span>
                    </span>
                    <?= __('Auto DM (Premium)') ?>
                </span>
            </label>
        </div>
    <?php
}
\Event::bind("package.add_module_option", __NAMESPACE__ . '\add_module_option');




/**
 * Map routes
 */
function route_maps($global_variable_name)
{
    $GLOBALS[$global_variable_name]->map("GET|POST", "/e/".IDNAME."/?", [
        PLUGINS_PATH . "/". IDNAME ."/controllers/IndexController.php",
        __NAMESPACE__ . "\IndexController"
    ]);
    $GLOBALS[$global_variable_name]->map("GET|POST", "/e/".IDNAME."/[i:id]/?", [
        PLUGINS_PATH . "/". IDNAME ."/controllers/ScheduleController.php",
        __NAMESPACE__ . "\ScheduleController"
    ]);
}
\Event::bind("router.map", __NAMESPACE__ . '\route_maps');



/**
 * Event: navigation.add_special_menu
 */
function navigation($Nav, $AuthUser)
{
    $idname = IDNAME;
    include __DIR__."/views/fragments/navigation.fragment.php";
}
\Event::bind("navigation.add_special_menu", __NAMESPACE__ . '\navigation');



/**
 * Add cron task to like new posts
 */
function addCronTask()
{
    // Get auto like schedules
    require_once __DIR__ . "/models/SchedulesModel.php";
    require_once __DIR__ . "/models/OtherTaskModel.php";
    $Emojione = new \Emojione\Client(new \Emojione\Ruleset());
    $Schedules = new SchedulesModel;
    $Schedules->where("is_active", "=", 1)
        ->where("schedule_date", "<=", date("Y-m-d H:i:s"))
        ->where("end_date", ">=", date("Y-m-d H:i:s"))
        ->orderBy("last_action_date", "ASC")
        ->setPageSize(10)// required to prevent server overload
        ->setPage(1)
        ->fetchData();
    if ($Schedules->getTotalCount() < 1) {
        // There is not any active schedule
        return false;
    }

    $as = [__DIR__ . "/models/ScheduleModel.php", __NAMESPACE__ . "\ScheduleModel"];
    foreach ($Schedules->getDataAs($as) as $sc) {
        // Set next schedule date
        $reqcount = [
            // speed => number of requests per hour
            "1" => 1, // Very Slow
            "2" => 2, // Slow
            "3" => 3, // Medium
            "4" => 4, // Fast
            "5" => 5  // Very Fast
        ];
        $speed = (int)$sc->get("speed");
        $delta = isset($reqcount[$speed]) && $reqcount[$speed] > 0
            ? round(3600 / $reqcount[$speed]) : rand(720, 7200);
        $next_schedule_timestamp = time() + $delta;
        $sc->set("schedule_date", date("Y-m-d H:i:s", $next_schedule_timestamp))
            ->set("last_action_date", date("Y-m-d H:i:s"))
            ->save();


        // Start data validation
        $Account = \Controller::model("Account", $sc->get("account_id"));
        if (!$Account->isAvailable() || $Account->get("login_required")) {
            // Account is either removed (unexected, external factors)
            // Or login required for this account
            // Deactivate schedule
            $sc->set("is_active", 0)->save();
            continue;
        }

        $User = \Controller::model("User", $sc->get("user_id"));
        if (!$User->isAvailable() || !$User->get("is_active") || $User->isExpired()) {
            // User is not valid
            // Deactivate schedule
            $sc->set("is_active", 0)->save();
            continue;
        }

        if ($User->get("id") != $Account->get("user_id")) {
            // Unexpected, data modified by external factors
            // Deactivate schedule
            $sc->set("is_active", 0)->save();
            continue;
        }

        // Create a action log (not save yet)
        require_once __DIR__ . "/models/LogModel.php";
        $Log = new LogModel;
        $Log->set("user_id", $User->get("id"))
            ->set("account_id", $Account->get("id"))
            ->set("message_id", $sc->get("id"))
            ->set("status", "error");


        // Login into the account
        try {
            $Instagram = \InstagramController::login($Account);
        } catch (\Exception $e) {
            // Couldn't login into the account
            $Account->refresh();

            if ($Account->get("login_required")) {
                // Deactivate schedule
                $sc->set("is_active", 0)->save();
            }

            // Log data
            $Log->set("data", $e->getMessage())
                ->save();

            continue;
        }


        // Logged in successfully


        // Find user to send welcome DM.
        $follower_id = null;
        $follower_username = null;
        $follower_fullname = null;
        $follower_profile_pic = null;

        $rank_token = \InstagramAPI\Signatures::generateUUID();

        $targets = @json_decode($sc->get("targets"));
        if (!$targets) {
            // Unexpected, data modified by external factors
            // Deactivate schedule
            $sc->set("is_active", 0)->save();
            continue;
        }
        $i = rand(0, count($targets) - 1);
        $target = $targets[$i];

        if ($target->type == "hashtag") {
            try {
                $feed = $Instagram->hashtag->getFeed(
                    str_replace("#", "", trim($target->id)),
                    $rank_token);
            } catch (\Exception $e) {
                // Couldn't get instagram feed related to the hashtag

                // Log data
                $Log->set("data.error.msg", "Couldn't get the feed")
                    ->set("data.error.details", $e->getMessage())
                    ->save();
                continue;
            }

            if (count($feed->getItems()) < 1) {
                // Invalid
                continue;
            }


            foreach ($feed->getItems() as $item) {
                if ($item->getUser()->getPk() != $Account->get("instagram_id")/* &&
                    hasFilter($Instagram, $sc, $item)*/) {
                    $_log = new LogModel([
                        "user_id" => $User->get("id"),
                        "account_id" => $Account->get("id"),
                        "follower_id" => $item->getUser()->getPk(),
                        "status" => "success"
                    ]);
                    if (!$_log->isAvailable()) {
                        // Found new user
                        $follower_id = $item->getUser()->getPk();
                        $follower_username = $item->getUser()->getUsername();
                        $follower_fullname = $item->getUser()->getFullName();
                        $follower_profile_pic = $item->getUser()->getProfilePicUrl();
                        break 1;
                    }
                }
            }
        } else if ($target->type == "location") {
            try {
                $feed = $Instagram->location->getFeed(
                    $target->id,
                    $rank_token);
            } catch (\Exception $e) {
                // Couldn't get instagram feed related to the location id

                // Log data
                $Log->set("data.error.msg", "Couldn't get the feed")
                    ->set("data.error.details", $e->getMessage())
                    ->save();
                continue;
            }

            if (count($feed->getItems()) < 1) {
                // Invalid
                continue;
            }

            foreach ($feed->getItems() as $item) {
                if ($item->getUser()->getPk() != $Account->get("instagram_id") /*&&
                    hasFilter($Instagram, $sc, $item)*/) {
                    $_log = new LogModel([
                        "user_id" => $User->get("id"),
                        "account_id" => $Account->get("id"),
                        "follower_id" => $item->getUser()->getPk(),
                        "status" => "success"
                    ]);
                    if (!$_log->isAvailable()) {
                        // Found new user
                        $follower_id = $item->getUser()->getPk();
                        $follower_username = $item->getUser()->getUsername();
                        $follower_fullname = $item->getUser()->getFullName();
                        $follower_profile_pic = $item->getUser()->getProfilePicUrl();
                        break 1;
                    }
                }
            }
        } else if ($target->type == "people") {
            $round = 1;
            $loop = true;
            $next_max_id = null;

            while ($loop) {
                try {
                    $feed = $Instagram->people->getFollowers(
                        $target->id,
                        $rank_token,
                        null,
                        $next_max_id);
                } catch (\Exception $e) {
                    // Couldn't get instagram feed related to the user id
                    $loop = false;

                    if ($round == 1) {
                        // Log data
                        $Log->set("data.error.msg", "Couldn't get the feed")
                            ->set("data.error.details", $e->getMessage())
                            ->save();
                    }
                    continue 2;
                }

                if (count($feed->getUsers()) < 1) {
                    // Invalid
                    $loop = false;
                    continue 2;
                }

                // Get friendship statuses
                $user_ids = [];
                foreach ($feed->getUsers() as $user) {
                    $user_ids[] = $user->getPk();
                }

                try {
                    $friendships = $Instagram->people->getFriendships($user_ids);
                } catch (\Exception $e) {
                    // Couldn't get instagram friendship statuses
                    $loop = false;

                    if ($round == 1) {
                        // Log data
                        $Log->set("data.error.msg", "Couldn't get the friendship statuses")
                            ->set("data.error.details", $e->getMessage())
                            ->save();
                    }
                    continue 2;
                }

                $followings = [];
                foreach ($friendships->getFriendshipStatuses()->getData() as $pk => $fs) {
                    if ($fs->getOutgoingRequest() || $fs->getFollowing()) {
                        $followings[] = $pk;
                    }
                }


                foreach ($feed->getUsers() as $user) {
                    if (!in_array($user->getPk(), $followings) &&
                        $user->getPk() != $Account->get("instagram_id")/* &&
                        hasFilter($Instagram, $sc, null, $user)*/) {
                        $_log = new LogModel([
                            "user_id" => $User->get("id"),
                            "account_id" => $Account->get("id"),
                            "follower_id" => $user->getPk(),
                            "status" => "success"
                        ]);

                        if (!$_log->isAvailable()) {
                            // Found new user
                            $follower_id = $user->getPk();
                            $follower_username = $user->getUsername();
                            $follower_fullname = $user->getFullName();
                            $follower_profile_pic = $user->getProfilePicUrl();
                            break 2;
                        }
                    }
                }

                $round++;
                $next_max_id = $feed->getNextMaxId();
                if ($round >= 5 || !empty($follow_pk) || $next_max_id === null) {
                    $loop = false;
                }
            }
            //$count_iteretion = 10;
        }

        if (empty($follower_id)) {
            $Log->set("data.error.msg", "Couldn't find new user to follow")
                ->save();
            continue;
        }

        $d = new \DateTime();
        $dt = new \DateInterval('PT1M');
        $messages = json_decode($sc->get("messages"));
        for ($i = 0; $i < count($messages); $i++) {
            $d = date_add($d, $dt);
            $task = new OtherTaskModel;

            $task->set("status", "wait");
            $task->set("user_id", $User->get("id"));
            $task->set("account_id", $Account->get("id"));
            $task->set("type", "direct");
            $task->set("create_date", date("Y-m-d H:i:s"));
            $task->set("schedule_date", $d->format("Y-m-d H:i:s"));
            $task->set("worked_date", date("Y-m-d H:i:s"));

            $message = $Emojione->shortnameToUnicode($messages[$i]);
            $task->set("data", json_encode([
                "pk" => $follower_id,
                "username" => $follower_username,
                "message" => $message,
            ]));
            $task->save();
        }

        $Log->set("status", "success")
            ->set("data.followed", [
                "pk" => $follower_id,
                "username" => $follower_username,
                "full_name" => $follower_fullname,
                "profile_pic_url" => $follower_profile_pic
            ])
            ->set("follower_id", $follower_id)
            ->save();

    }


/*
        // Set variables in message
        $search = ["{{username}}", "{{fullname}}"];
        $replace = ["@".$follower_username, 
                    $follower_fullname ? $follower_fullname : "@".$follower_username];
        $message = str_replace($search, $replace, $message);

        // Check spintax permission
        if ($User->get("settings.spintax")) {
            $message = \Spintax::process($message);
        }


        // New folloer found
        // Send DM
        try {
            $res = $Instagram->direct->sendText(["users" => [$follower_id]], $message);
        } catch (\Exception $e) {
            $Log->set("data", $e->getMessage())->save();
            continue;
        }


        // Send DM successfully
        // Save log
        $data = [
            "sendto" => [
                "follower_id" => $follower_id,
                "follower_username" => $follower_username,
                "follower_fullname" => $follower_fullname,
                "follower_profile_pic" => $follower_profile_pic
            ],
            "message" => $message
        ];


        $Log->set("status", isset($res->status) && $res->status == "ok" ?  "success" : "error")
            ->set("data", json_encode($data))
            ->set("follower_id", $follower_id)
            ->save();
    }
    echo "yes ";*/
}
\Event::bind("cron.add", __NAMESPACE__."\addCronTask");
