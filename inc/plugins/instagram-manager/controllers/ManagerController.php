<?php
namespace Plugins\InstagramManager;

// Disable direct access
if (!defined('APP_VERSION')) 
    die("Yo, what's up?");

/**
 * Comments Controller
 */
class ManagerController extends \Controller
{
    /**
     * Process
     */
    protected $Instagram;

    public function process()
    {
        $AuthUser = $this->getVariable("AuthUser");
        $Route = $this->getVariable("Route");
        $this->setVariable("idname", "instagram-manager");

        // Auth
        if (!$AuthUser){
            header("Location: ".APPURL."/login");
            exit;
        } else if ($AuthUser->isExpired()) {
            header("Location: ".APPURL."/expired");
            exit;
        }

        $user_modules = $AuthUser->get("settings.modules");
        if (!is_array($user_modules) || !in_array($this->getVariable("idname"), $user_modules)) {
            // Module is not accessible to this user
            header("Location: ".APPURL."/post");
            exit;
        }


        // Get account
        $Account = \Controller::model("Account", $Route->params->id);
        if (!$Account->isAvailable() || 
            $Account->get("user_id") != $AuthUser->get("id")) 
        {
            header("Location: ".APPURL."/e/".$this->getVariable("idname"));
            exit;
        }
        $this->setVariable("Account", $Account);
        $this->Instagram = \InstagramController::login($Account);

        switch (\Input::post('comand')){
            case 'getMedia':
                return $this->getPage();
            case 'saveCaption':
                return $this->saveCaption();
            case 'deleteMedia':
                return $this->deleteMedia();
            case 'checkUsername':
                return $this->checkUsername();
            case 'saveUserInfo':
                return $this->saveUserInfo();
        }

        $this->view(PLUGINS_PATH."/".$this->getVariable("idname")."/views/manager.php", null);
    }


    /**
     * Save schedule
     * @return mixed 
     */
    private function getPage(){
        $this->resp->userInfo = $this->getUserInfo($this->Instagram);
        $this->resp->media = $this->getMedia($this->Instagram);
        $this->jsonecho();
    }
    private function getMedia($Instagram){
        $timeline = $Instagram->timeline->getSelfUserFeed(\Input::post('next_max_id'));
        $list = $timeline->getItems();
        $returnList = [];
        foreach ($list as $element){
            $item = new \stdClass();
            if($element->getMedia_type()==1 || $element->getMedia_type()==2) {
                $item->imgUrl = $element->getImage_versions2()->getCandidates()[0]->getUrl();
            }else  if ($element->getMedia_type() == 8) {//TODO доделать єтот момент
                $item->imgUrl = $element->getCarouselMedia()[0]->getImageVersions2()->getCandidates()[0]->getUrl();
            }
            $item->media_type = $element->getMedia_type();
            $item->comment_count = $element->getComment_count();
            $item->caption ='';
            if($element->getCaption()) {
                $item->caption = $element->getCaption()->getText();
            }
            $item->like_count = $element->getLike_count();
            $item->pk = $element->getPk();
            $item->id = $element->getId();
            $item->device_timestamp = $element->getDevice_timestamp();
            $item->code = $element->getCode();

            $returnList[] = $item;
        }
        $systemInfo = new \stdClass();
        $systemInfo->next_max_id = $timeline->getNext_max_id();
        $systemInfo->more_available = $timeline->getMore_available();
        $this->resp->systemInfo = $systemInfo;
        return $returnList;
    }
    private function getUserInfo($Instagram){
        $userAc = $Instagram->account->getCurrentUser()->getUser();
        $user = $Instagram->people->getSelfInfo()->getUser();
        $userInfo = new \stdClass();
        $userInfo->username = $Instagram->username;
        $userInfo->full_name = $user->getFull_name();
        $userInfo->biography = $user->getBiography();
        $userInfo->pk = $user->getPk();
        $userInfo->external_url = $user->getExternal_url();
        $userInfo->profile_img = $user->getHd_profile_pic_url_info()->getUrl();
        $userInfo->phone_number = $userAc->getPhone_number();
        $userInfo->media_count = $user->getMedia_count();
        $userInfo->following_count = $user->getFollowing_count();
        $userInfo->follower_count = $user->getFollower_count();
        $userInfo->email = $userAc->getEmail();
        $userInfo->gender = $userAc->getGender();
        return $userInfo;
    }
    private function saveCaption(){
        $caption = $this->_nomalizeText(\Input::post('caption'));

        $media_id = \Input::post('media_id');
        $media_type = \Input::post('mediaType');
        $this->Instagram->media->edit($media_id, $caption, null, $media_type);
        $this->resp->status=1;
        $this->jsonecho();
    }
    private function _nomalizeText($text, $needBefore = true){
        $Emojione = new \Emojione\Client(new \Emojione\Ruleset());

        $return = $Emojione->shortnameToUnicode($text);
        $return = mb_substr($return, 0, 2200);
        $spec1 = ' 
';
        $spec = '
';
        $spec3 = '⠀
';
        while (strpos($return, '⠀⠀')>-1){
            $return = str_replace('⠀⠀', '⠀', $return);
        }
        while (strpos($return, $spec1)){
            $return = str_replace($spec1, $spec, $return);
        }
        $return = str_replace($spec, $spec3, $return);

        if($needBefore){
            $return = $spec3 . $return;
        }

        return $return;
    }
    private function deleteMedia(){
        $media_id = \Input::post('media_id');
        $media_type = \Input::post('mediaType');
        $this->Instagram->media->delete($media_id, $media_type);
        $this->resp->status=1;
        $this->jsonecho();
    }
    private function checkUsername(){
        $this->resp->status = $this->Instagram->account->checkUsername(\Input::post('username'))->getAvailable();
        $this->jsonecho();
    }
    private function saveUserInfo(){
        $user = $this->getUserInfo($this->Instagram);

        $user->external_url = \Input::post('external_url');
        $user->phone_number = \Input::post('phone_number');
        $user->full_name = \Input::post('full_name');
        $user->biography = $this->_nomalizeText(\Input::post('biography'), false);
        $user->email = \Input::post('email');
        $user->gender = \Input::post('gender');
        if($this->Instagram->account->checkUsername(\Input::post('username'))->getAvailable()) {
            $user->newUsername = \Input::post('username');
        }else if(\Input::post('username')==$user->username){
            $user->newUsername = null;
        }else{
            $this->resp->status = 2;
            $this->jsonecho();
        }
        try {
            $r = $this->Instagram->account->editProfile($user->external_url, $user->phone_number, $user->full_name, $user->biography, $user->email, $user->gender, $user->newUsername);
            $this->resp->userInfo = $this->getUserInfo($this->Instagram);
            $this->resp->status = 1;
            $this->jsonecho();
        }catch (\Exception $e){
            $this->resp->status = 0;
            $this->jsonecho();
        }
    }
}
