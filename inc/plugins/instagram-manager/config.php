<?php if (!defined('APP_VERSION')) die("Yo, what's up?"); ?>
<?php 
    return [
        "idname" => "instagram-manager",
        "plugin_name" => "Instagram Manager",
        "plugin_uri" => "",
        "author" => "Andrey4korop",
        "author_uri" => "",
        "version" => "1.0",
        "desc" => "",
        "icon_style" => "background-color: #A077FF; background-image: linear-gradient(to right bottom, #00ff21, #00e1af, #00b9ff, #0086ff, #0028ed); color: #fff; font-size: 18px;",
    ];
    