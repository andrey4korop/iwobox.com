import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import Notifications from 'vue-notification'
window.querystring = require('querystring');

import App from './App.vue'
import top from './components/top.vue'
import itemBlock from './components/itemBlock.vue'
import editMedia from './components/editMedia.vue'
import deleteMedia from './components/deleteMedia.vue'
import editUser from './components/editUser.vue'

Vue.use(VueAxios, axios);
Vue.use(Notifications);
Vue.component('top', top);
Vue.component('itemBlock', itemBlock);
Vue.component('editMedia', editMedia);
Vue.component('deleteMedia', deleteMedia);
Vue.component('editUser', editUser);
new Vue({
  el: '#managerApp',
  render: h => h(App)
})
