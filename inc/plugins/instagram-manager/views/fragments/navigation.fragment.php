<?php 
    if (!defined('APP_VERSION')) 
        die("Yo, what's up?");  

    if (!isset($GLOBALS["_PLUGINS_"][$idname]["config"]))
          return null;

    $config = $GLOBALS["_PLUGINS_"][$idname]["config"]; 
    $user_modules = $AuthUser->get("settings.modules");
    if (empty($user_modules)) {
        $user_modules = [];
    }

    if (!in_array($idname, $user_modules)) {
        return null;
    }
?>
<li class="<?= $Nav->activeMenu == $idname ? "active" : "" ?>">
    <a href="<?= APPURL."/e/".$idname ?>">
        <span class="special-menu-icon" style="<?= empty($config["icon_style"]) ? "" : $config["icon_style"] ?>">
            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
<path style="fill:#FF3997;" d="M512,106l-41.1,41.023l-51.301-8.683L396.5,115.283l-23.1-23.058l-8.699-51.202L406.101,0
	C429.5,6.287,454.1,20.062,473,38.926C503.301,69.17,510.5,102.706,512,106z"/>
<path style="fill:#F50B75;" d="M512,106l-41.1,41.023l-51.301-8.683L396.5,115.283L473,38.926C503.301,69.17,510.5,102.706,512,106z
	"/>
<polygon style="fill:#EDE9E8;" points="470.9,146.435 428.301,188.736 377.3,180.035 354.2,156.936 331.101,134.136 322.4,82.835
	364.701,40.236 417.8,93.336 "/>
<polygon style="fill:#DFD7D5;" points="470.9,146.435 428.301,188.736 377.3,180.035 354.2,156.936 417.8,93.336 "/>
<polygon style="fill:#FED2A4;" points="155.799,460.899 0,512 50.799,356.199 130.6,381.099 137.8,383.2 "/>
<polygon style="fill:#FFBD86;" points="137.8,383.2 155.799,460.899 0,512 130.6,381.099 "/>
<g>
	<polygon style="fill:#25D9F8;" points="266.499,351.606 155.799,460.899 103.299,408.497 50.799,356.396 160.3,245.904
		213.401,298.605 	"/>
	<polygon style="fill:#25D9F8;" points="428.301,189.244 351.4,266.865 245.2,160.865 322.4,83.542 	"/>
</g>
<g>
	<polygon style="fill:#00ABE9;" points="213.401,298.605 266.499,351.606 155.799,460.899 103.299,408.497 	"/>
	<polygon style="fill:#00ABE9;" points="375.5,136.543 428.301,189.244 351.4,266.865 298.299,213.864 	"/>
</g>
<polygon style="fill:#FFD400;" points="512,405.799 405.801,512 362.5,468.699 374.801,443.8 341.201,447.4 319.9,426.099
	341.201,392.5 298.9,404.799 277.599,383.8 289.9,358.9 256.3,362.5 235.3,341.199 256.3,307.599 213.999,320.2 202.899,309.099
	192.7,298.9 205.3,273.999 171.4,277.599 150.399,256.3 171.4,222.7 129.101,235.3 107.8,213.999 120.399,189.099 86.8,192.7
	65.501,171.7 86.8,137.8 44.2,150.399 0,105.901 105.899,0 308.8,202.899 "/>
<g>
	<polygon style="fill:#FDBF00;" points="512,405.799 405.801,512 362.5,468.699 374.801,443.8 341.201,447.4 319.9,426.099
		341.201,392.5 298.9,404.799 277.599,383.8 289.9,358.9 256.3,362.5 235.3,341.199 256.3,307.599 213.999,320.2 202.899,309.099
		308.8,202.899 	"/>

		<rect x="46.073" y="124.752" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 228.6817 184.7769)" style="fill:#FDBF00;" width="59.999" height="29.997"/>

		<rect x="84.104" y="156.564" transform="matrix(-0.707 0.7072 -0.7072 -0.707 341.7115 201.5549)" style="fill:#FDBF00;" width="89.999" height="29.997"/>

		<rect x="130.932" y="209.604" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 433.541 269.6256)" style="fill:#FDBF00;" width="59.993" height="29.997"/>

		<rect x="168.965" y="241.441" transform="matrix(-0.7072 0.707 -0.707 -0.7072 546.5861 286.5208)" style="fill:#FDBF00;" width="89.999" height="29.997"/>
</g>
<g>

		<rect x="215.793" y="294.436" transform="matrix(-0.707 0.7072 -0.7072 -0.707 638.4037 354.3806)" style="fill:#FF9F00;" width="59.999" height="29.997"/>

		<rect x="253.823" y="326.281" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 751.4505 371.2968)" style="fill:#FF9F00;" width="90.009" height="29.997"/>

		<rect x="300.635" y="379.344" transform="matrix(-0.7072 0.707 -0.707 -0.7072 843.2646 439.4631)" style="fill:#FF9F00;" width="59.999" height="29.997"/>

		<rect x="338.669" y="411.134" transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 956.2842 456.1589)" style="fill:#FF9F00;" width="89.999" height="29.997"/>
</g>
</svg>
        </span>

        <?php $name = empty($config["plugin_name"]) ? $idname : $config["plugin_name"]; ?>
        <span class="label"><?= $name ?></span>

        <span class="tooltip tippy" 
              data-position="right"
              data-delay="100" 
              data-arrow="true"
              data-distance="-1"
              title="<?= $name ?>"></span>
    </a>
</li>