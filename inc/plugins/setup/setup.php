<?php 
namespace Plugins\Setup;
const IDNAME = "setup";

// Disable direct access
if (!defined('APP_VERSION')) 
    die("Yo, what's up?"); 


/**
 * Event: plugin.install
 */
function install($Plugin)
{
    if ($Plugin->get("idname") != IDNAME) {
        return false;
    }

    $sql  = "DROP TABLE IF EXISTS `".TABLE_PREFIX."setup_settings`; ";
    $sql .= "CREATE TABLE `".TABLE_PREFIX."setup_settings` ( 
                `id` INT NOT NULL AUTO_INCREMENT , 
                `data` TEXT NOT NULL ,
                `info` TEXT NOT NULL ,
                PRIMARY KEY (`id`)
            ) ENGINE = InnoDB; ";
  
    $sql .= "INSERT INTO `np_setup_settings` (`data`, `info`) VALUES ('{\"show_speed\":true,\"default_speed\":\"auto\",\"daily_pause_from\":\"03:00:00\",\"daily_pause_to\":\"09:00:00\",\"show_pause\":true,\"show_text\":null,\"daily_pause\":\"1\",\"min_source\":3,\"plugin_status\":{\"auto-comment\":\"1\",\"auto-follow\":\"1\",\"auto-unfollow\":\"1\",\"auto-like\":\"1\",\"welcomedm\":\"1\"},\"block_duplicate\":0}', '');";

    $pdo = \DB::pdo();
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
}
\Event::bind("plugin.install", __NAMESPACE__ . '\install');



/**
 * Event: plugin.remove
 */
function uninstall($Plugin)
{
    if ($Plugin->get("idname") != IDNAME) {
        return false;
    }

    $sql = "DROP TABLE `".TABLE_PREFIX."setup_settings`;";

    $pdo = \DB::pdo();
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
}
\Event::bind("plugin.remove", __NAMESPACE__ . '\uninstall');


/**
 * Add module as a package options
 * Only users with granted permission
 * Will be able to use module
 * 
 * @param array $package_modules An array of currently active 
 *                               modules of the package
 */
function add_module_option($package_modules)
{
    $config = include __DIR__."/config.php";
    ?>
        <div class="mt-15">
            <label>
                <input type="checkbox" 
                       class="checkbox" 
                       name="modules[]" 
                       value="<?= IDNAME ?>" 
                       <?= in_array(IDNAME, $package_modules) ? "checked" : "" ?>>
                <span>
                    <span class="icon unchecked">
                        <span class="mdi mdi-check"></span>
                    </span>
                    <?= __('Setup') ?>
                </span>
            </label>
        </div>
    <?php
}
\Event::bind("package.add_module_option", __NAMESPACE__ . '\add_module_option');




/**
 * Map routes
 */
function route_maps($global_variable_name)
{
    // Settings (admin only)
    $GLOBALS[$global_variable_name]->map("GET|POST", "/e/".IDNAME."/settings/?", [
        PLUGINS_PATH . "/". IDNAME ."/controllers/SettingsController.php",
        __NAMESPACE__ . "\SettingsController"
    ]);

    // Index
    $GLOBALS[$global_variable_name]->map("GET|POST", "/e/".IDNAME."/?", [
        PLUGINS_PATH . "/". IDNAME ."/controllers/IndexController.php",
        __NAMESPACE__ . "\IndexController"
    ]);

    // Schedule
    $GLOBALS[$global_variable_name]->map("GET|POST", "/e/".IDNAME."/[i:id]/?", [
        PLUGINS_PATH . "/". IDNAME ."/controllers/SetupController.php",
        __NAMESPACE__ . "\SetupController"
    ]);
}
\Event::bind("router.map", __NAMESPACE__ . '\route_maps');


/**
 * Event: navigation.add_special_menu
 */
function navigation($Nav, $AuthUser)
{
    $idname = IDNAME;
    include __DIR__."/views/fragments/navigation.fragment.php";
}
\Event::bind("navigation.add_menu", __NAMESPACE__ . '\navigation');


/**
 * check if account is added in the system
 * @param string $username
 * @return void
 */
function checkAccountInstall($username = '')
{
  $username = trim(strtolower($username));

  require_once PLUGINS_PATH."/".IDNAME."/models/SettingsModel.php";
  $Settings = new \Plugins\Setup\SettingsModel();
  if (!$username || !$Settings->isAvailable() || !$Settings->get("data.block_duplicate"))
  {
    return;
  }
  

  $account = \DB::table(TABLE_PREFIX.TABLE_ACCOUNTS)->where("username", "=", $username)->limit(1)->get();
  if (!$account)
  {
    return;
  }

  $res = new \stdClass();
  $res->result = 0;
  $res->msg = __("This account is already registred in our system. Try another account or if you think this is a mistake, talk with our support team!");
  
  echo \Input::get("callback") ? \Input::get("callback")."(".json_encode($res).")" : json_encode($res);
  exit;
  
  
}
\Event::bind("account.befereAdd", __NAMESPACE__ . '\checkAccountInstall');


/**
 * redirect user to setup page
 * @param Object $Account
 * @return void
 */
function redirectSetup($Account = null)
{
  if (!$Account || !$Account->isAvailable() || $Account->get("login_required") || !$Account->get("id"))
  {
    return;
  }
  
  $res = new \stdClass();
  $res->result = 1;
  $res->redirect = APPURL."/e/".IDNAME."/".$Account->get("id");
  
  echo \Input::get("callback") ? \Input::get("callback")."(".json_encode($res).")" : json_encode($res);
  exit;
  
}
\Event::bind("account.added", __NAMESPACE__ . '\redirectSetup');

/**
 * Check NextPost plugins
 * @param Object $AuthUser
 * @return array $plugins
 */
function checkPlugins($AuthUser = false)
{
    $userModules = $AuthUser ? $AuthUser->get('settings.modules') : [];
    $plugins = [
        'auto-comment' => [
          'title'     => __('Auto Comment'),
          'checkTable'=> TABLE_PREFIX . 'auto_comment_schedule',
          'installed' => isset($GLOBALS['_PLUGINS_']['auto-comment']) && $GLOBALS['_PLUGINS_']['auto-comment'],
          'hasAccess' => in_array('auto-comment', $userModules),
          'show'      => true,
        ],
        'auto-follow' => [
          'title'       => __('Auto Follow'),
          'checkTable' => TABLE_PREFIX . 'auto_follow_schedule',
          'installed' => isset($GLOBALS['_PLUGINS_']['auto-follow']) && $GLOBALS['_PLUGINS_']['auto-follow'],
          'hasAccess' => in_array('auto-follow', $userModules),
          'show'      => true,
        ],
        'auto-unfollow' => [
          'title'       => __('Auto Unfollow'),
          'checkTable' => TABLE_PREFIX . 'auto_unfollow_schedule',
          'installed' => isset($GLOBALS['_PLUGINS_']['auto-unfollow']) && $GLOBALS['_PLUGINS_']['auto-unfollow'],
          'hasAccess' => in_array('auto-unfollow', $userModules),
          'show'      => true,
        ],
        'auto-like' => [
          'title'       => __('Auto Like'),
          'checkTable' => TABLE_PREFIX . 'auto_like_schedule',
          'installed' => isset($GLOBALS['_PLUGINS_']['auto-like']) && $GLOBALS['_PLUGINS_']['auto-like'],
          'hasAccess' => in_array('auto-like', $userModules),
          'show'      => true,
        ],
        'auto-repost' => [
          'title'       => __('Auto Repost'),
          'checkTable' => TABLE_PREFIX . 'auto_repost_schedule',
          'installed' => isset($GLOBALS['_PLUGINS_']['auto-repost']) && $GLOBALS['_PLUGINS_']['auto-repost'],
          'hasAccess' => in_array('auto-repost', $userModules),
          'show'      => false,
        ],
        'welcomedm' => [
          'title'       => __('Welcome DM'),
          'checkTable' => TABLE_PREFIX . 'welcomedm_schedule',
          'installed' => isset($GLOBALS['_PLUGINS_']['welcomedm']) && $GLOBALS['_PLUGINS_']['welcomedm'],
          'hasAccess' => in_array('welcomedm', $userModules),
          'show'      => true,
        ],
        'post' => [
          'title'       => __('Posts'),
          'checkTable' => TABLE_PREFIX . TABLE_POSTS,
          'installed' => true,
          'hasAccess' => true,
          'show'      => false,
        ]
      ];
  return $plugins;
}

