<?php if (!defined('APP_VERSION')) die("Yo, what's up?"); ?>
<?php 
    return [
        "idname" => "setup",
        "plugin_name" => "Quick Setup",
        "author" => "Dan Developer",
        "author_uri" => "dandeveloper.br@gmail.com ",
        "version" => "1.0.1",
        "desc" => "Setup all modules at once",
        "icon_style" => "background-color: #001f8e; background: linear-gradient(-135deg, #3b7bff 0%, #00050e 100%); color: #fff; font-size: 18px;",
        "settings_page_uri" => APPURL . "/e/setup/settings"
    ];
    