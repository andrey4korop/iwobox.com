/**
 * Setup Namespane
 */
var Setup = {};

/**
 * Setup Schedule Form
 */
Setup.ScheduleForm = function()
{
  
    var $form = $(".js-setup-schedule-form");
    var $searchinp = $form.find("#setup-source :input[name='search']");
    var query;
    var icons = {};
        icons.hashtag = "mdi mdi-pound";
        icons.location = "mdi mdi-map-marker";
        icons.people = "mdi mdi-instagram";
    var target = [];

    // Current tags
    $form.find("#setup-source .tag").each(function() {
        target.push($(this).data("type") + "-" + $(this).data("id"));
    });

    // Search input
    $searchinp.devbridgeAutocomplete({
        serviceUrl: $searchinp.data("url"),
        type: "GET",
        dataType: "jsonp",
        minChars: 2,
        deferRequestBy: 200,
        appendTo: $form,
        forceFixPosition: true,
        paramName: "q",
        params: {
            action: "search",
            type: $form.find("#setup-source :input[name='type']:checked").val(),
        },
        onSearchStart: function() {
            $form.find("#setup-source .js-search-loading-icon").removeClass('none');
            query = $searchinp.val();
        },
        onSearchComplete: function() {
            $form.find("#setup-source .js-search-loading-icon").addClass('none');
        },

        transformResult: function(resp) {
            return {
                suggestions: resp.result == 1 ? resp.items : []
            };
        },

        beforeRender: function (container, suggestions) {
            for (var i = 0; i < suggestions.length; i++) {
                var type = $form.find("#setup-source :input[name='type']:checked").val();
                if (target.indexOf(type + "-" + suggestions[i].data.id) >= 0) {
                    container.find("#setup-source .autocomplete-suggestion").eq(i).addClass('none')
                }
            }
        },

        formatResult: function(suggestion, currentValue){
            var pattern = '(' + currentValue.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&") + ')';
            var type = $form.find("#setup-source :input[name='type']:checked").val();

            return suggestion.value
                        .replace(new RegExp(pattern, 'gi'), '<strong>$1<\/strong>')
                        .replace(/&/g, '&amp;')
                        .replace(/</g, '&lt;')
                        .replace(/>/g, '&gt;')
                        .replace(/"/g, '&quot;')
                        .replace(/&lt;(\/?strong)&gt;/g, '<$1>') + 
                    (suggestion.data.sub ? "<span class='sub'>"+suggestion.data.sub+"<span>" : "");
        },

        onSelect: function(suggestion){ 
            $searchinp.val(query);
            var type = $form.find("#setup-source :input[name='type']:checked").val();

            if (target.indexOf(type+"-"+suggestion.data.id) >= 0) {
                return false;
            }
            
            var $tag = $("<span style='margin: 0px 2px 3px 0px' title='"+suggestion.data.sub+"'></span>");
                $tag.addClass("tag pull-left preadd");
                $tag.attr({
                    "data-type": type,
                    "data-id": suggestion.data.id,
                    "data-value": suggestion.value,
                });
                $tag.text(suggestion.value);

                $tag.prepend("<span class='icon "+icons[type]+"'></span>");
                $tag.append("<span class='mdi mdi-close remove'></span>");

            $tag.appendTo($form.find("#setup-source .tags"));

            setTimeout(function(){
                $tag.removeClass("preadd");
            }, 50);

            target.push(type+ "-" + suggestion.data.id);
        }
    });


    // Change search source
    $form.find("#setup-source :input[name='type']").on("change", function() {
        var type = $form.find("#setup-source :input[name='type']:checked").val();

        $searchinp.autocomplete('setOptions', {
            params: {
                action: "search",
                type: type
            }
        });

        $searchinp.trigger("blur");
        setTimeout(function(){
            $searchinp.trigger("focus");
        }, 200)
    });

    // Remove target
    $form.on("click", "#setup-source .tag .remove", function() {
        var $tag = $(this).parents(".tag");

        var index = target.indexOf($tag.data("type") + "-" + $tag.data("id"));
        if (index >= 0) {
            target.splice(index, 1);
        }

        $tag.remove();
    });

    // Daily pause
    $form.find(":input[name='daily-pause']").on("change", function() {
        if ($(this).is(":checked")) {
            $form.find(".js-daily-pause-range").css("opacity", "1");
            $form.find(".js-daily-pause-range").find(":input").prop("disabled", false);
        } else {
            $form.find(".js-daily-pause-range").css("opacity", "0.25");
            $form.find(".js-daily-pause-range").find(":input").prop("disabled", true);
        }
    }).trigger("change");
  
  
    //unfollow whitelist
    var $searchinpWhitelist = $form.find("#setup-unfollow :input[name='search']");
    var whitelist = [];

    // Current tags
    $form.find("#setup-unfollow .tag").each(function() {
        whitelist.push('' + $(this).data("id"));
    });


    // Search input
    $searchinpWhitelist.devbridgeAutocomplete({
        serviceUrl: $searchinp.data("url"),
        type: "GET",
        dataType: "jsonp",
        minChars: 2,
        deferRequestBy: 200,
        appendTo: $("body"),
        forceFixPosition: true,
        paramName: "q",
        params: {
            action: "search",
            type: "people"
        },
        onSearchStart: function() {
            $form.find("#setup-unfollow .js-search-loading-icon").removeClass('none');
            query = $searchinpWhitelist.val();
        },
        onSearchComplete: function() {
            $form.find("#setup-unfollow .js-search-loading-icon").addClass('none');
        },

        transformResult: function(resp) {
            return {
                suggestions: resp.result == 1 ? resp.items : []
            };
        },

        beforeRender: function (container, suggestions) {
            for (var i = 0; i < suggestions.length; i++) {
                if (whitelist.indexOf('' + suggestions[i].data.id) >= 0) {
                    container.find("#setup-unfollow .autocomplete-suggestion").eq(i).addClass('none')
                }
            }
        },

        formatResult: function(suggestion, currentValue){
            var pattern = '(' + currentValue.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&") + ')';

            return suggestion.value
                        .replace(new RegExp(pattern, 'gi'), '<strong>$1<\/strong>')
                        .replace(/&/g, '&amp;')
                        .replace(/</g, '&lt;')
                        .replace(/>/g, '&gt;')
                        .replace(/"/g, '&quot;')
                        .replace(/&lt;(\/?strong)&gt;/g, '<$1>') + 
                    (suggestion.data.sub ? "<span class='sub'>"+suggestion.data.sub+"<span>" : "");
        },

        onSelect: function(suggestion){ 
            $searchinpWhitelist.val(query);

            if (whitelist.indexOf('' + suggestion.data.id) >= 0) {
                return false;
            }
            
            var $tag = $("<span style='margin: 0px 2px 3px 0px' title='"+suggestion.data.sub+"'></span>");
                $tag.addClass("tag pull-left preadd");
                $tag.attr({
                    "data-id": ''+suggestion.data.id,
                    "data-value": suggestion.value,
                });
                $tag.text(suggestion.value);

                $tag.prepend("<span class='icon mdi mdi-instagram'></span>");
                $tag.append("<span class='mdi mdi-close remove'></span>");

            $tag.appendTo($form.find("#setup-unfollow .whitelist"));

            setTimeout(function(){
                $tag.removeClass("preadd");
            }, 50);

            whitelist.push('' + suggestion.data.id);
        }
    });


    // Remove tag
    $form.on("click", "#setup-unfollow .tag .remove", function() {
        var $tag = $(this).parents(".tag");

        var index = whitelist.indexOf(''+$tag.data("id"));
        if (index >= 0) {
            whitelist.splice(index, 1);
        }

        $tag.remove();
    });


    // Submit form
    $form.on("submit", function() {
        $("body").addClass("onprogress");

        var target    = [];
        var whitelist = [];
        var comments  = [];
        var messages  = [];
        var modules = [];
      

        $form.find("#setup-actions .stats_plugin_list").each(function() {
            var t = {};
                t.module = $(this).data("plugin");
                t.value = $(this).attr("checked") ? 1 : 0;
            modules.push(t);
        });


        $form.find("#setup-source .tags .tag").each(function() {
            var t = {};
                t.type = $(this).data("type");
                t.id = $(this).data("id").toString();
                t.value = $(this).data("value");

            target.push(t);
        });
      
        $form.find("#setup-unfollow .whitelist .tag").each(function() {
            var t = {};
                t.id = $(this).data("id").toString();
                t.value = $(this).data("value");

            whitelist.push(t);
        });
      
        $form.find(".ac-comment-list-item").each(function() {
            comments.push($(this).data("comment"));
        });
      
        $form.find(".wdm-message-list-item .message").each(function() {
            messages.push($(this).text());
        })

        $.ajax({
            url: $form.attr("action"),
            type: $form.attr("method"),
            dataType: 'jsonp',
            data: {
                action: "save",
                is_active: 1,
                target: JSON.stringify(target),
                speed: $form.find(":input[name='speed']").val(),
                daily_pause: $form.find(":input[name='daily-pause']").is(":checked") ? 1 : 0,
                daily_pause_from: $form.find(":input[name='daily-pause-from']").val(),
                daily_pause_to: $form.find(":input[name='daily-pause-to']").val(),
                timeline_feed: $form.find(":input[name='timeline-feed']").is(":checked") ? 1 : 0,
                modules: JSON.stringify(modules),
                whitelist: JSON.stringify(whitelist),
                keep_followers: $form.find("#setup-unfollow :input[name='keep-followers']").is(":checked") ? 1 : 0,
                source: $form.find("#setup-unfollow :input[name='source']").val(),
              
                comments: JSON.stringify(comments),
                messages: JSON.stringify(messages)
            },
            error: function() {
                $("body").removeClass("onprogress");
                NextPost.DisplayFormResult($form, "error", __("Oops! An error occured. Please try again later!"));
            },

            success: function(resp) {
                if (resp.result == 1) {
                    NextPost.DisplayFormResult($form, "success", resp.msg);
                } else {
                    NextPost.DisplayFormResult($form, "error", resp.msg);
                }

                $("body").removeClass("onprogress");
            }
        });

        return false;
    });
}


/**
 * Setup Index
 */
Setup.Index = function()
{
    $(document).ajaxComplete(function(event, xhr, settings) {
        var rx = new RegExp("(setup\/[0-9]+(\/)?)$");
        if (rx.test(settings.url)) {
            Setup.ScheduleForm();
        }
    })
}



/**
 * Lnky users and tags
 */
Setup.LinkyComments = function()
{
    $(".ac-comment-list-item .comment").not(".js-linky-done")
        .addClass("js-linky-done")
        .linky({
            mentions: true,
            hashtags: true,
            urls: false,
            linkTo:"instagram"
        });
}



Setup.CommentsForm = function()
{
    var $form = $(".js-setup-schedule-form");

    // Emoji
    var emoji = $("#setup-comments .new-comment-input").emojioneArea({
        saveEmojisAs      : "unicode", // unicode | shortname | image
        imageType         : "svg", // Default image type used by internal CDN
        pickerPosition: 'bottom',
        buttonTitle: __("Use the TAB key to insert emoji faster")
    });

    // Emoji area input filter
    emoji[0].emojioneArea.on("drop", function(obj, event) {
        event.preventDefault();
    });

    emoji[0].emojioneArea.on("paste keyup input emojibtn.click", function() {
        $form.find(":input[name='new-comment-input']").val(emoji[0].emojioneArea.getText());
    });

    // Linky
    Setup.LinkyComments();

    // Add comment
    $("#setup-comments .js-add-new-comment-btn").on("click", function() {
        var comment = $.trim(emoji[0].emojioneArea.getText());

        if (comment) {
            $comment = $("<div class='ac-comment-list-item'></div>");
            $comment.append('<a href="javascript:void(0)" class="remove-comment-btn mdi mdi-close-circle"></a>');
            $comment.append("<span class='comment'></span>");
            $comment.find(".comment").text(comment);

            $comment.data("comment", comment);

            // Replace new lines to <br>
            comment = $comment.find(".comment").text();
            comment = comment.replace(/(?:\r\n|\r|\n)/g, '<br>');
            $comment.find(".comment").html(comment);

            $comment.prependTo(".ac-comment-list");

            Setup.LinkyComments();

            emoji[0].emojioneArea.setText("");
        }
    });
  
    // Remove message
    $("body").on("click", "#setup-comments .remove-message-btn", function() {
        $(this).parents(".wdm-message-list-item").remove();
    });

}


/**
 * Lnky users and tags
 */
Setup.LinkyDM = function()
{
    $(".wdm-message-list-item .message").not(".js-linky-done")
        .addClass("js-linky-done")
        .linky({
            mentions: true,
            hashtags: true,
            urls: false,
            linkTo:"instagram"
        });
}

Setup.MessagesForm = function()
{
    var $form = $(".js-setup-schedule-form");

    // Linky
    Setup.LinkyDM();

    // Emoji
    var emoji = $("#setup-welcome .new-message-input").emojioneArea({
        saveEmojisAs      : "unicode", // unicode | shortname | image
        imageType         : "svg", // Default image type used by internal CDN
        pickerPosition: 'bottom',
        buttonTitle: __("Use the TAB key to insert emoji faster")
    });

    // Emoji area input filter
    emoji[0].emojioneArea.on("drop", function(obj, event) {
        event.preventDefault();
    });

    emoji[0].emojioneArea.on("paste keyup input emojibtn.click", function() {
        $form.find("#setup-welcome :input[name='new-message-input']").val(emoji[0].emojioneArea.getText());
    });

    // Add message
    $("#setup-welcome .js-add-new-message-btn").on("click", function() {
        var message = $.trim(emoji[0].emojioneArea.getText());

        if (message) {
            $message = $("<div class='wdm-message-list-item'></div>");
            $message.append('<a href="javascript:void(0)" class="remove-message-btn mdi mdi-close-circle"></a>');
            $message.append("<span class='message'></span>");
            $message.find(".message").html(message.replace(/(?:\r\n|\r|\n)/g, '<br>\n'));

            $message.prependTo(".wdm-message-list");

            Setup.LinkyDM();

            emoji[0].emojioneArea.setText("");
        }
    });
  
    // Remove message
    $("body").on("click", "#setup-welcome .remove-message-btn", function() {
        $(this).parents(".wdm-message-list-item").remove();
    });
}

