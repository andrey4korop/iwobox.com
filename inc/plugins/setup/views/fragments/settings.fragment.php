<?php if (!defined('APP_VERSION')) die("Yo, what's up?"); ?>

<div class='skeleton' id="account">
    <form class="js-ajax-form" 
          action="<?= APPURL . "/e/" . $idname . "/settings" ?>"
          method="POST">
        <input type="hidden" name="action" value="save">

        <div class="container-1200">
            <div class="row clearfix">
                  <div class="form-result"></div>
                  <div class="col s12 m12 l12 m-last l-last last mb-20">
                      <section class="section">
                        <div class="section-header clearfix">
                            <h2 class="section-title"><?= __("Page Text") ?></h2>
                        </div>
                        <div class="section-content">
                          <div class="mb-20">
                              <label>
                                  <input type="checkbox" 
                                         class="checkbox" 
                                         name="show_text" 
                                         value="1" 
                                         <?= $Settings->get("data.show_text") ? "checked" : "" ?>>
                                  <span>
                                      <span class="icon unchecked">
                                          <span class="mdi mdi-check"></span>
                                      </span>
                                      <?= __('Show Info text in Setup Page?') ?>

                                      <ul class="field-tips">
                                          <li><?= __("If you enable this option, the text below will be showed at top of the page.") ?></li>
                                          <li><?= __("Enable here and edit in setup page!") ?></li>
                                      </ul>
                                  </span>
                              </label>
                            </div>
                          <textarea name="info" id="info"><?= $Settings->get('info'); ?></textarea>
                          <script>
                            CKEDITOR.replace( 'info' );
                          </script>
                        </div>
                      </section>
                    </div>
                </div>
            <div class="row clearfix">
                <div class="col s12 m4 l4">
                  <section class="section mb-20">
                        <div class="section-header clearfix">
                            <h2 class="section-title"><?= __("Speeds") ?></h2>
                        </div>

                        <div class="section-content">
                            <div class="mb-10 clearfix">
                                <div class="col s12 m12 l12">
                                    <label>
                                      <input type="checkbox" 
                                           class="checkbox" 
                                           name="show_speed" 
                                           value="1" 
                                           <?= $Settings->get("data.show_speed") ? "checked" : "" ?>>
                                      <span>
                                        <span class="icon unchecked">
                                            <span class="mdi mdi-check"></span>
                                        </span>
                                        <?= __('Allow user select the speed?') ?>
                                        <ul class="field-tips">
                                            <li><?= __("If you enable this option, user will be able to select the speed.") ?></li>
                                            <li><?= __("If you disabel this option, the speed bellow will be setted by default.") ?></li>
                                        </ul>
                                    </span>
                                  </label>
                                </div>

                                <div class="col s12 s-last m12 m-last l12 l-last mb-20">
                                    <label class="form-label"><?= __("Default Value") ?></label>

                                    <select name="default_speed" class="input">
                                        <?php $s = $Settings->get("data.default_speed"); ?>
                                        <option value="auto" <?= $s == "auto" ? "selected" : "" ?>><?= __("Auto"). " (".__("Recommended").")" ?></option>
                                        <option value="very_slow" <?= $s == "very_slow" ? "selected" : "" ?>><?= __("Very Slow") ?></option>
                                        <option value="slow" <?= $s == "slow" ? "selected" : "" ?>><?= __("Slow") ?></option>
                                        <option value="medium" <?= $s == "medium" ? "selected" : "" ?>><?= __("Medium") ?></option>
                                        <option value="fast" <?=$s == "fast" ? "selected" : "" ?>><?= __("Fast") ?></option>
                                        <option value="very_fast" <?= $s == "very_fast" ? "selected" : "" ?>><?= __("Very Fast") ?></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
              
                <div class="col s12 m4 l4">
                  <section class="section mb-20">
                        <div class="section-header clearfix">
                            <h2 class="section-title"><?= __("Default Values") ?></h2>
                        </div>

                        <div class="section-content">
                            <div class="mb-10 clearfix">
                                <div class="col s12 m12 l12">
                                  <?php foreach($Plugins as $k => $v) : if ($v["installed"] && $v["show"]):?>
                                    <label style="display: inline-block; margin: 5px;">
                                      <input type="checkbox" 
                                           class="checkbox" 
                                           name="plugin_status[<?=$k?>]" 
                                           value="1" 
                                           <?= $Settings->get("data.plugin_status.{$k}") ? "checked" : "" ?>>
                                      <span>
                                        <span class="icon unchecked">
                                            <span class="mdi mdi-check"></span>
                                        </span>
                                        <?= $v["title"] ?>
                                    </span>
                                  </label>
                                  <?php endif; endforeach;?>
                                  <ul class="field-tips">
                                      <li><?= __("Choose if these modules should be checked by default.") ?></li>
                                  </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
              
                <div class="col s12 m4 l4 last s-last m-last l-last">
                  <section class="section">
                        <div class="section-header clearfix">
                            <h2 class="section-title"><?= __("Other Settings") ?></h2>
                        </div>

                        <div class="section-content">
                          <div class="mb-20">
                            <label class="form-label"><?= __("Min of source") ?></label>
                              <label>
                                <input type="number" 
                                     class="input" 
                                     name="min_source" 
                                     value="<?= (int) $Settings->get("data.min_source");?>">
                                <span>
                                  <ul class="field-tips">
                                      <li><?= __("Min of People, Hashtags or Places that user should select.") ?></li>
                                      <li><?= __("Use 0 to no limit.") ?></li>
                                  </ul>
                              </span>
                            </label>
                          </div>
                          
                      </div>
                    </section>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col s12 m4 l4">
                  <section class="section mb-20">
                        <div class="section-header clearfix">
                            <h2 class="section-title"><?= __("Pause Actions") ?></h2>
                        </div>

                        <div class="section-content">
                            <div class="mb-10 clearfix">
                                <div class="col s12 m12 l12 mb-20">
                                    <label>
                                      <input type="checkbox" 
                                           class="checkbox" 
                                           name="show_pause" 
                                           value="1" 
                                           <?= $Settings->get("data.show_pause") ? "checked" : "" ?>>
                                      <span>
                                        <span class="icon unchecked">
                                            <span class="mdi mdi-check"></span>
                                        </span>
                                        <?= __('Allow user set daily pause?') ?>
                                        <ul class="field-tips">
                                            <li><?= __("Allow user set daily pause?") ?></li>
                                            <li><?= __("If you disable this option, the time bellow will be setted by default.") ?></li>
                                        </ul>
                                    </span>
                                  </label>
                                </div>

                                <div class="col s12 s-last m12 m-last l12 l-last mb-20">
                                    <label class="form-label"><?= __("Default Values") ?></label>
                                  <div class="mb-20">
                                        <label>
                                            <input type="checkbox" 
                                                   class="checkbox" 
                                                   name="daily_pause" 
                                                   value="1"
                                                   <?= $Settings->get("data.daily_pause") ? "checked" : "" ?>>
                                            <span>
                                                <span class="icon unchecked">
                                                    <span class="mdi mdi-check"></span>
                                                </span>
                                                <?= __('Pause actions everyday') ?> ...
                                            </span>
                                        </label>
                                    </div>

                                    <div class="clearfix mb-20 js-daily-pause-range">
                                            <?php $timeformat = $AuthUser->get("preferences.timeformat") == "12" ? 12 : 24; ?>

                                            <div class="col s6 m6 l6">
                                                <label class="form-label"><?= __("From") ?></label>

                                                <?php 
                                                    $from = new \DateTime(date("Y-m-d")." ".$Settings->get("data.daily_pause_from"));
                                                    $from->setTimezone(new \DateTimeZone($AuthUser->get("preferences.timezone")));
                                                    $from = $from->format("H:i");
                                                ?>

                                                <select class="input" name="daily_pause_from">
                                                    <?php for ($i=0; $i<=23; $i++): ?>
                                                        <?php $time = str_pad($i, 2, "0", STR_PAD_LEFT).":00"; ?>
                                                        <option value="<?= $time ?>" <?= $from == $time ? "selected" : "" ?>>
                                                            <?= $timeformat == 24 ? $time : date("h:iA", strtotime(date("Y-m-d")." ".$time)) ?>    
                                                        </option>

                                                        <?php $time = str_pad($i, 2, "0", STR_PAD_LEFT).":30"; ?>
                                                        <option value="<?= $time ?>" <?= $from == $time ? "selected" : "" ?>>
                                                            <?= $timeformat == 24 ? $time : date("h:iA", strtotime(date("Y-m-d")." ".$time)) ?>    
                                                        </option>
                                                    <?php endfor; ?>
                                                </select>
                                            </div>

                                            <div class="col s6 s-last m6 m-last l6 l-last">
                                                <label class="form-label"><?= __("To") ?></label>

                                                <?php 
                                                    $to = new \DateTime(date("Y-m-d")." ".$Settings->get("data.daily_pause_to"));
                                                    $to->setTimezone(new \DateTimeZone($AuthUser->get("preferences.timezone")));
                                                    $to = $to->format("H:i");
                                                ?>

                                                <select class="input" name="daily_pause_to">
                                                    <?php for ($i=0; $i<=23; $i++): ?>
                                                        <?php $time = str_pad($i, 2, "0", STR_PAD_LEFT).":00"; ?>
                                                        <option value="<?= $time ?>" <?= $to == $time ? "selected" : "" ?>>
                                                            <?= $timeformat == 24 ? $time : date("h:iA", strtotime(date("Y-m-d")." ".$time)) ?>    
                                                        </option>

                                                        <?php $time = str_pad($i, 2, "0", STR_PAD_LEFT).":30"; ?>
                                                        <option value="<?= $time ?>" <?= $to == $time ? "selected" : "" ?>>
                                                            <?= $timeformat == 24 ? $time : date("h:iA", strtotime(date("Y-m-d")." ".$time)) ?>    
                                                        </option>
                                                    <?php endfor; ?>
                                                </select>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col s12 m4 l4">
                  <section class="section mb-20">
                          <div class="section-header clearfix">
                              <h2 class="section-title"><?= __("Block duplicate accounts?") ?></h2>
                          </div>

                          <div class="section-content">
                              <div class="mb-10 clearfix">
                                  <div class="col s12 m12 l12">
                                      <label>
                                        <input type="checkbox" 
                                             class="checkbox" 
                                             name="block_duplicate" 
                                             value="1" 
                                             <?= $Settings->get("data.block_duplicate") ? "checked" : "" ?>>
                                        <span>
                                          <span class="icon unchecked">
                                              <span class="mdi mdi-check"></span>
                                          </span>
                                          <?= __('Disallow user add same account more then once?') ?>
                                          <ul class="field-tips">
                                              <li><?= __("NextPost allows adding an acount even if is was already added (by the same or other user).") ?></li>
                                              <li><?= __("Enable this option to avoid it") ?></li>
                                              <li><?= __("ATENTION! In order to use this feature, you need to add the events in your code, as described in docs!") ?></li>
                                          </ul>
                                      </span>
                                    </label>
                                  </div>
                              </div>
                          </div>
                      </section>
                </div>
              <div class="col s12 m4 l4 last m-last l-last s-last">
                  <section class="section mb-20">
                      <div class="section-header clearfix">
                          <h2 class="section-title"><?= __("Atention!") ?></h2>
                      </div>

                      <div class="section-content">
                          <div class="mb-10 clearfix">
                              <div class="col s12 m12 l12">
                                <p><?= __("You need to edit your code and add more event triggers in order to use all features of this plugin") ?></p>
                                <code style="font-size: 10px">
                                  <strong>File:</strong> app/controllers/AccountController.php
                                  <strong>Add Triggers:</strong><br>
                                  \Event::trigger("account.added", $Account);<br><br>
                                  \Event::trigger("account.befereAdd", Input::post("username"));
                                </code>
                              </div>
                          </div>
                      </div>
                    <input class="fluid button button--footer" type="submit" value="<?= __("Save") ?>">
                  </section>
                </div>
            </div>
        </div>
    </form>
</div>