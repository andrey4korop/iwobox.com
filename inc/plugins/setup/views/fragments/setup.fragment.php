<?php if (!defined('APP_VERSION')) die("Yo, what's up?"); ?>

<div class="skeleton skeleton--full">
    <div class="clearfix">
        <aside class="skeleton-aside hide-on-medium-and-down">
            <div class="aside-list js-loadmore-content" data-loadmore-id="1"></div>

            <div class="loadmore pt-20 none">
                <a class="fluid button button--light-outline js-loadmore-btn js-autoloadmore-btn" data-loadmore-id="1" href="<?= APPURL."/e/".$idname."?aid=".$Account->get("id")."&ref=schedule" ?>">
                    <span class="icon sli sli-refresh"></span>
                    <?= __("Load More") ?>
                </a>
            </div>
        </aside>

        <section class="skeleton-content">
          <?php if ($Settings->get("data.show_text") && strip_tags($Settings->get("info"))) : ?>
          <div class="clearfix">
            <div class="section-content">
              <div class="col s12 m12 l12">
                <div>
                  <?= $Settings->get("info"); ?>
                </div>
              </div>
            </div>
          </div>
          <?php endif; ?>
          
            <form class="js-setup-schedule-form"
                  action="<?= APPURL."/e/".$idname."/".$Account->get("id") ?>"
                  method="POST">             

                <input type="hidden" name="action" value="save">
                <input type="hidden" name="is_active" value="1">

                <div class="section-content" style="max-width:none;">
                    <div class="form-result mb-25" style="display:none;"></div>

                    <div class="clearfix">
                        <div class="col s12 m12 l12">
                          
                          <!-- start: actions list -->
                          <div class="setup-box" id="setup-actions">
                            <div class="setup-head">
                              <h3 class="setup-title"><?=__('<span>Select </span> the actions'); ?></h3>
                            </div>
                            
                            <div class="setup-body">
                              <div class="setup-content">
                                <div class="mb-5 clearfix">
                                  
                                  <?php
                                    $showLikeInfo = false;
                                    foreach($Plugins as $k => $v) :
                                      if ($v["installed"] && $v["show"] && $v["hasAccess"]):
                                      if ($k == 'auto-like') $showLikeInfo = true;
                                  ?>
                                    <label class="inline-block mr-50 mb-15">
                                        <input type="checkbox" class="checkbox stats_plugin_list" data-plugin="<?=$k;?>" name="plugin_status[<?=$k?>]"  value="1" <?= $Settings->get("data.plugin_status.{$k}") ? "checked" : "" ?>>
                                        <span>
                                            <span class="icon unchecked">
                                                <span class="mdi mdi-check"></span>
                                            </span>
                                            <?= $v["title"] ?>
                                        </span>
                                    </label>
                                  <?php
                                    endif;
                                   endforeach;
                                  ?>
                                </div>
                         
                              <div class="setup-helper">
                                <p><?= __("Turn on/off the actions that your account will run!");?></p>
                                <?php if($showLikeInfo) : ?>
                                <p><strong><?= __("When using the Auto Like Module your media's target will be liked, and not the medias of the target followers!"); ?></strong></p>
                                <?php endif; ?>
                              </div>
                            
                              </div>
                            </div>
                            
                          </div>
                          <!-- end: actions list -->
                           
                          
                          <!-- start: search source -->
                          <div class="setup-box" id="setup-source">
                            <div class="setup-head">
                              <h3 class="setup-title"><?=__('<span>Choose</span> the source'); ?></h3>
                            </div>
                            
                            <div class="setup-body">
                              <div class="setup-content">
                                 
                                <div class="mb-5 clearfix">
                                    <label class="inline-block mr-50 mb-15">
                                        <input class="radio" name='type' type="radio" value="hashtag" checked>
                                        <span>
                                            <span class="icon"></span>
                                            #<?= __("Hashtags") ?>
                                        </span>
                                    </label>

                                    <label class="inline-block mr-50 mb-15">
                                        <input class="radio" name='type' type="radio" value="location">
                                        <span>
                                            <span class="icon"></span>
                                            <?= __("Places") ?>
                                        </span>
                                    </label>

                                    <label class="inline-block mb-15">
                                        <input class="radio" name='type' type="radio" value="people">
                                        <span>
                                            <span class="icon"></span>
                                            <?= __("People") ?>
                                        </span>
                                    </label>
                                </div>
                          
                                <div class="clearfix mb-20 pos-r">
                                    <label class="form-label"><?= __('Search') ?></label>
                                    <input class="input rightpad" name="search"  type="text" value="" 
                                           data-url="<?= APPURL."/e/".$idname."/".$Account->get("id") ?>">
                                    <span class="field-icon--right pe-none none js-search-loading-icon">
                                        <img src="<?= APPURL."/assets/img/round-loading.svg" ?>" alt="Loading icon">
                                    </span>
                                </div>

                                <div class="tags clearfix mt-20">
                                    <?php 
                                        $icons = [
                                            "hashtag" => "mdi mdi-pound",
                                            "location" => "mdi mdi-map-marker",
                                            "people" => "mdi mdi-instagram"
                                        ];
                                    ?>
                                    <?php foreach ($targets as $t): ?>
                                        <span class="tag pull-left"
                                              data-type="<?= htmlchars($t->type) ?>" 
                                              data-id="<?= htmlchars($t->id) ?>" 
                                              data-value="<?= htmlchars($t->value) ?>" 
                                              style="margin: 0px 2px 3px 0px;">
                                            <?php if (isset($icons[$t->type])): ?>
                                                <span class="<?= $icons[$t->type] ?>"></span>
                                            <?php endif ?>  

                                            <?= htmlchars($t->value) ?>
                                            <span class="mdi mdi-close remove"></span>
                                          </span>
                                    <?php endforeach ?>
                                </div>
                              <div class="setup-helper">
                                <p><?= $Settings->get("data.min_source") 
                                        ? __("Choose here where your new followers will come from. Select at least %s sources. Our platform will interage (like, comment,follow and more) with what you select here!", $Settings->get("data.min_source") )
                                        : __("Choose here where your new followers will come from. Our platform will interage (like, comment,follow and more) with what you select here!");
                                ?></p>
                              </div>
                            
                              </div>
                            </div>
                            
                          </div>
                          <!-- end: search source -->
                           

                          <!-- start: speed and pause -->
                          <?php if ($Settings->get('data.show_speed') || $Settings->get('data.show_pause')) : ?>
                          <div class="setup-box">
                            <div class="setup-head">
                              <h3 class="setup-title"><?=__('<span>Speed</span> and <span>Pause</span>'); ?></h3>
                            </div>
                            
                            <div class="setup-body">
                              <div class="setup-content">
                                
                                
                                <div class="clearfix">
                                  <!-- start: speed -->
                                  <?php if ($Settings->get('data.show_speed')) : $defaultSpeed = $Settings->get('data.default_speed');?>
                                  <div class="col s12 m6 l6">
                                    <label class="form-label setup-label-speed" style="margin-bottom:48px"><?= __('Speed') ?></label>
                                    <select class="input" name="speed">
                                        <option value="auto" <?= $defaultSpeed == "auto" ? "selected" : "" ?>><?= __("Auto"). " (".__("Recommended").")" ?></option>
                                        <option value="very_slow" <?= $defaultSpeed == "very_slow" ? "selected" : "" ?>><?= __("Very Slow") ?></option>
                                        <option value="slow" <?= $defaultSpeed == "slow" ? "selected" : "" ?>><?= __("Slow") ?></option>
                                        <option value="medium" <?= $defaultSpeed == "medium" ? "selected" : "" ?>><?= __("Medium") ?></option>
                                        <option value="fast" <?=$defaultSpeed == "fast" ? "selected" : "" ?>><?= __("Fast") ?></option>
                                        <option value="very_fast" <?= $defaultSpeed == "very_fast" ? "selected" : "" ?>><?= __("Very Fast") ?></option>
                                    </select>
                                    <div class="setup-helper">
                                      <p>How fast our system should work? Set it here, but be careful.</p>
                                    </div>                                    
                                  </div>
                                  <?php else : ?>
                                  <input type="hidden" name="speed" value="<?= $defaultSpeed; ?>">
                                  <?php endif; ?>
                                  <!-- end: speed -->
                                  
                                  <!-- start: daily pause -->
                                  <?php if ($Settings->get('data.show_pause')) : ?>
                                  <div class="col s12 m6 l6 l-last m-last s-last">
                                    <div class="clearfix">
                                        <div class="mb-20">
                                            <label>
                                                <input type="checkbox" 
                                                       class="checkbox" 
                                                       name="daily-pause" 
                                                       value="1"
                                                       <?= $Settings->get('data.daily_pause') ? "checked" : "" ?>>
                                                <span>
                                                    <span class="icon unchecked">
                                                        <span class="mdi mdi-check"></span>
                                                    </span>
                                                    <?= __('Pause actions everyday') ?> ...
                                                </span>
                                            </label>
                                        </div>

                                        <div class="clearfix mb-20 js-daily-pause-range">
                                            <?php $timeformat = $AuthUser->get("preferences.timeformat") == "12" ? 12 : 24; ?>

                                            <div class="col s6 m6 l6">
                                                <label class="form-label"><?= __("From") ?></label>

                                                <?php 
                                                    $from = new \DateTime(date("Y-m-d")." ".$Settings->get('data.daily_pause_from'));
                                                    $from->setTimezone(new \DateTimeZone($AuthUser->get("preferences.timezone")));
                                                    $from = $from->format("H:i");
                                                ?>

                                                <select class="input" name="daily-pause-from">
                                                    <?php for ($i=0; $i<=23; $i++): ?>
                                                        <?php $time = str_pad($i, 2, "0", STR_PAD_LEFT).":00"; ?>
                                                        <option value="<?= $time ?>" <?= $from == $time ? "selected" : "" ?>>
                                                            <?= $timeformat == 24 ? $time : date("h:iA", strtotime(date("Y-m-d")." ".$time)) ?>    
                                                        </option>

                                                        <?php $time = str_pad($i, 2, "0", STR_PAD_LEFT).":30"; ?>
                                                        <option value="<?= $time ?>" <?= $from == $time ? "selected" : "" ?>>
                                                            <?= $timeformat == 24 ? $time : date("h:iA", strtotime(date("Y-m-d")." ".$time)) ?>    
                                                        </option>
                                                    <?php endfor; ?>
                                                </select>
                                            </div>

                                            <div class="col s6 s-last m6 m-last l6 l-last">
                                                <label class="form-label"><?= __("To") ?></label>

                                                <?php 
                                                    $to = new \DateTime(date("Y-m-d")." ".$Settings->get('data.daily_pause_to'));
                                                    $to->setTimezone(new \DateTimeZone($AuthUser->get("preferences.timezone")));
                                                    $to = $to->format("H:i");
                                                ?>

                                                <select class="input" name="daily-pause-to">
                                                    <?php for ($i=0; $i<=23; $i++): ?>
                                                        <?php $time = str_pad($i, 2, "0", STR_PAD_LEFT).":00"; ?>
                                                        <option value="<?= $time ?>" <?= $to == $time ? "selected" : "" ?>>
                                                            <?= $timeformat == 24 ? $time : date("h:iA", strtotime(date("Y-m-d")." ".$time)) ?>    
                                                        </option>

                                                        <?php $time = str_pad($i, 2, "0", STR_PAD_LEFT).":30"; ?>
                                                        <option value="<?= $time ?>" <?= $to == $time ? "selected" : "" ?>>
                                                            <?= $timeformat == 24 ? $time : date("h:iA", strtotime(date("Y-m-d")." ".$time)) ?>    
                                                        </option>
                                                    <?php endfor; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                  </div>
                                  <?php
                                    else :
                                      $from = new \DateTime(date("Y-m-d")." ".$Settings->get('data.daily_pause_from'));
                                      $from->setTimezone(new \DateTimeZone($AuthUser->get("preferences.timezone")));
                                      $from = $from->format("H:i");
                                  
                                      $to = new \DateTime(date("Y-m-d")." ".$Settings->get('data.daily_pause_to'));
                                      $to->setTimezone(new \DateTimeZone($AuthUser->get("preferences.timezone")));
                                      $to = $to->format("H:i");
                                  ?>
                                  <input type="hidden" name="daily-pause" value="<?= $Settings->get('data.daily_pause') ? 1 : 0; ?>">
                                  <input type="hidden" name="daily-pause-from" value="<?= $from; ?>">
                                  <input type="hidden" name="daily-pause-to" value="<?= $to; ?>">
                                  <?php endif; ?>
                                  <!-- end: daily pause -->
                                </div>
                              </div>
                            </div>
                          </div>
                          <?php endif; ?>
                          <!-- end: speed -->
                          
                          <?php if($Plugins['auto-like']['installed'] && $Plugins['auto-like']['hasAccess']) : ?>
                          <!-- start: like timeline -->
                          <div class="setup-box">
                            <div class="setup-head">
                              <h3 class="setup-title"><?=__('<span>Like</span> your own feed?'); ?></h3>
                            </div>
                            
                            <div class="setup-body">
                              <div class="setup-content">
                                <div class="clearfix">
                                  <div class="col s12 m6 m-last l6 l-last">
                                          <div class="mb-20">
                                              <label>
                                                  <input type="checkbox" 
                                                         class="checkbox" 
                                                         name="timeline-feed" 
                                                         value="1"
                                                         <?= $timeline_feed ? "checked" : "" ?>>
                                                  <span>
                                                      <span class="icon unchecked">
                                                          <span class="mdi mdi-check"></span>
                                                      </span>
                                                      <?= __("Like the feed from the timeline") ?>
                                                  </span>
                                              </label>
                                              <div class="setup-helper">
                                                <p><?= __("Check this and we'll like post that show in your timeline")?></p>
                                              </div>
                                          </div>
                                      </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <!-- end: like timeline -->
                          <?php endif; ?>
                          
                          <?php if($Plugins['auto-unfollow']['installed'] && $Plugins['auto-unfollow']['hasAccess']) : ?>
                          <!-- start: unfollow settgings -->
                          <div class="setup-box" id="setup-unfollow">
                            <div class="setup-head">
                              <h3 class="setup-title"><?=__('<span>Unfollow</span> settings'); ?></h3>
                            </div>
                            
                            <div class="setup-body">
                              <div class="setup-content">
                                <div class="clearfix">
                                  <div class="col s12 m6 l6">
                                    <!-- start: Unfollow source -->
                                    <div class="clearfix">
                                      <label class="form-label"><?= __("Source") ?></label>
                                      <select name="source" class="input">
                                          <option value="all"><?= __("All Followings") ?></option>
                                          <?php if (in_array("auto-follow", $user_modules) && in_array("auto-follow", array_keys($GLOBALS["_PLUGINS_"]))): ?>
                                              <option value="auto-follow" <?= $unfollow_source == "auto-follow" ? "selected" : "" ?>><?= __("Followed via Auto Follow Module") ?></option>
                                          <?php endif ?>
                                      </select>
                                    </div>
                                    <!-- end: Unfollow source -->
                                  </div>
                                  <div class="col s12 m6 l6 m-last l6 l-last">
                                    <!-- start: Unfollow keep followers -->
                                    <div class="mb-20 mt-30">
                                        <label>
                                            <input type="checkbox" 
                                                   class="checkbox" 
                                                   name="keep-followers" 
                                                   value="1"
                                                   <?= $keep_followers ? "checked" : "" ?>>
                                            <span>
                                                <span class="icon unchecked">
                                                    <span class="mdi mdi-check"></span>
                                                </span>
                                                <?= __("Don't unfollow my followers") ?>
                                            </span>
                                        </label>
                                    </div>
                                    <!-- end: Unfollow keep followers -->
                                  </div>
                                  
                                </div>
                                
                                <div class="clearfix mt-40">
                                  <!-- start: whitelist -->
                                  <div class="pos-r">
                                      <label class="form-label"><?= __("White list") ?></label>
                                      <input class="input rightpad" name="search" type="text" value="" 
                                             data-url="<?= APPURL."/e/".$idname."/".$Account->get("id") ?>"
                                             <?= $Account->get("login_required") ? "disabled" : "" ?>>
                                      <span class="field-icon--right pe-none none js-search-loading-icon">
                                          <img src="<?= APPURL."/assets/img/round-loading.svg" ?>" alt="Loading icon">
                                      </span>
                                  </div>
                                  <div class="setup-helper">
                                    <p><?= __("Include the usernames that you don't want to unfollow") ?></p>
                                  </div>
                                  <div class="whitelist clearfix mb-40"></div>
                                  <!-- end: whitelist -->
                                </div>
                                
                              </div>
                            </div>
                          </div>
                          <!-- end: unfollow settgings -->
                          <?php endif; ?>
                          
                          <?php if($Plugins['auto-comment']['installed'] && $Plugins['auto-comment']['hasAccess']) : ?>
                          <!-- start: comments -->
                          <div class="setup-box" id="setup-comments">
                            <div class="setup-head">
                              <h3 class="setup-title"><?=__('<span>Auto</span> comment'); ?></h3>
                            </div>
                            
                            <div class="setup-body">
                              <div class="setup-content">
                                <div class="clearfix">
                                  <div class="col s12 m10 l8">
                                    <div class="mb-20">
                                        <label class="form-label"><?= __("Comment") ?></label>

                                        <div class="clearfix">
                                            <div class="col s12 m12 l8 mb-20">
                                                <div class="new-comment-input input" 
                                                     data-placeholder="<?= __("Add your comment") ?>"
                                                     contenteditable="true"></div>
                                            </div>

                                            <div class="col s12 m12 l4 l-last">
                                                <a href="javascript:void(0)" class="fluid button button--light-outline mb-15 js-add-new-comment-btn">
                                                    <span class="mdi mdi-plus-circle"></span>
                                                    <?= __("Add Comment") ?>    
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="field-tips">
                                        <li>
                                            <?= __("You can use following variables in the comments:") ?>

                                            <div class="mt-5">
                                                <strong>{{username}}</strong>
                                                <?= __("Media owner's username") ?>
                                            </div>

                                            <div class="mt-5">
                                                <strong>{{full_name}}</strong>
                                                <?= __("Media owner's full name. If user's full name is not set, username will be used.") ?>
                                            </div>
                                        </li>
                                    </ul>

                                    <div class="ac-comment-list clearfix"></div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <!-- end: comments -->
                          <?php endif; ?>
                          
                          <?php if($Plugins['welcomedm']['installed'] && $Plugins['welcomedm']['hasAccess']) : ?>                         
                          <!-- start: welcome messages -->
                          <div class="setup-box" id="setup-welcome">
                            <div class="setup-head">
                              <h3 class="setup-title"><?=__('<span>Welcome</span> Direct Message'); ?></h3>
                            </div>
                            
                            <div class="setup-body">
                              <div class="setup-content">
                                <div class="clearfix">
                                  <div class="col s12 m10 l8">
                                      <div class="mb-20">
                                          <label class="form-label"><?= __("Messages") ?></label>

                                          <div class="clearfix">
                                              <div class="col s12 m12 l8">
                                                  <div class="new-message-input input" 
                                                       data-placeholder="<?= __("Add your message") ?>"
                                                       contenteditable="true"></div>
                                              </div>

                                              <div class="col s12 m12 l4 l-last">
                                                  <a href="javascript:void(0)" class="fluid button button--light-outline mb-15 js-add-new-message-btn">
                                                      <span class="mdi mdi-plus-circle"></span>
                                                      <?= __("Add Message") ?>    
                                                  </a>
                                              </div>
                                          </div>
                                      </div>

                                      <ul class="field-tips">
                                          <li>
                                              <?= __("You can use following variables in the comments:") ?>

                                              <div class="mt-5">
                                                  <strong>{{username}}</strong>
                                                  <?= __("Media owner's username") ?>
                                              </div>

                                              <div class="mt-5">
                                                  <strong>{{full_name}}</strong>
                                                  <?= __("Media owner's full name. If user's full name is not set, username will be used.") ?>
                                              </div>
                                          </li>
                                      </ul>
                                      <div class="wdm-message-list clearfix"></div>
                                  </div>
                              </div>
                              </div>
                            </div>
                          </div>
                          <!-- end: welcome messages -->
                          <?php endif; ?>
                          
                          
                          
                          
                          

                            <div class="clearfix mt-20">
                                <div class="col s12 m6 l6">
                                    <input class="fluid button" type="submit" value="<?= __("Save") ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              
            </form>
        </section>
    </div>
</div>