<?php
namespace Plugins\Setup;

// Disable direct access
if (!defined('APP_VERSION')) 
    die("Yo, what's up?");

/**
 * Settings Controller
 */
class SettingsController extends \Controller
{
    /**
     * idname of the plugin for internal use
     */
    const IDNAME = 'setup';


    /**
     * Process
     * @return null
     */
    public function process()
    {
        $AuthUser = $this->getVariable("AuthUser");
        $this->setVariable("idname", self::IDNAME);

        // Auth
        if (!$AuthUser || !$AuthUser->isAdmin()){
            header("Location: ".APPURL."/login");
            exit;
        } else if ($AuthUser->isExpired()) {
            header("Location: ".APPURL."/expired");
            exit;
        }

        // Plugin settings
        require_once PLUGINS_PATH."/".self::IDNAME."/models/SettingsModel.php";
        $Settings = new \Plugins\Setup\SettingsModel();

        $this->setVariable("Settings",$Settings);

        // Actions
        if (\Input::post("action") == "save") {
            $this->save();
        }
        $this->setVariable("Plugins", namespace\checkPlugins($AuthUser));

        $this->view(PLUGINS_PATH."/".self::IDNAME."/views/settings.php", null);
    }


    /**
     * Save plugin settings
     * @return boolean 
     */
    private function save()
    {
        $Settings = $this->getVariable("Settings");
        $AuthUser = $this->getVariable("AuthUser");
      
        $from = new \DateTime(date("Y-m-d")." ".\Input::post("daily_pause_from"),
                                  new \DateTimeZone($AuthUser->get("preferences.timezone")));
        $from->setTimezone(new \DateTimeZone("UTC"));

        $to = new \DateTime(date("Y-m-d")." ".\Input::post("daily_pause_to"),
                            new \DateTimeZone($AuthUser->get("preferences.timezone")));
        $to->setTimezone(new \DateTimeZone("UTC"));

        $Settings->set("data.show_speed", (bool)\Input::post("show_speed"))
                ->set("data.default_speed", \Input::post("default_speed"))
                ->set("data.daily_pause_from", $from->format("H:i:s"))
                ->set("data.daily_pause_to", $to->format("H:i:s"))
                ->set("data.show_pause", (bool)\Input::post("show_pause"))
                ->set("data.show_text", \Input::post("show_text"))
                ->set("data.daily_pause", \Input::post("daily_pause"))
                ->set("data.min_source", (int)\Input::post("min_source"))
                ->set("data.plugin_status", \Input::post("plugin_status"))
                ->set("data.block_duplicate", (int)\Input::post("block_duplicate"))
                ->set("info", \Input::post("info"))
                ->save();

        $this->resp->result = 1;
        $this->resp->msg = __("Changes saved!");
        $this->jsonecho();

        return $this;
    }
}
