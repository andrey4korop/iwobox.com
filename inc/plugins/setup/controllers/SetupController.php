<?php
namespace Plugins\Setup;

// Disable direct access
if (!defined('APP_VERSION')) 
    die("Yo, what's up?");

/**
 * Setup Controller
 */
class SetupController extends \Controller
{
    /**
     * idname of the plugin for internal use
     */
    const IDNAME = 'setup';


    /**
     * Process
     */
    public function process()
    {
        $AuthUser = $this->getVariable("AuthUser");
        $Route = $this->getVariable("Route");
        $this->setVariable("idname", self::IDNAME);

        // Auth
        if (!$AuthUser){
            header("Location: ".APPURL."/login");
            exit;
        } else if ($AuthUser->isExpired()) {
            header("Location: ".APPURL."/expired");
            exit;
        }

        $user_modules = $AuthUser->get("settings.modules");
        if (!is_array($user_modules) || !in_array(self::IDNAME, $user_modules)) {
            // Module is not accessible to this user
            header("Location: ".APPURL."/post");
            exit;
        }
        $this->setVariable("user_modules", $user_modules);

        // Get account
        $Account = \Controller::model("Account", $Route->params->id);
        if (!$Account->isAvailable() || 
            $Account->get("user_id") != $AuthUser->get("id")) 
        {
            header("Location: ".APPURL."/e/".self::IDNAME);
            exit;
        }
        $this->setVariable("Account", $Account);
      
        // Plugin settings
        require_once PLUGINS_PATH."/".self::IDNAME."/models/SettingsModel.php";
        $Settings = new \Plugins\Setup\SettingsModel();

        $this->setVariable("Settings", $Settings);
        $this->setVariable("Plugins", namespace\checkPlugins($AuthUser));
        //print_r($this->getVariable("Plugins")); exit;

        if (\Input::request("action") == "search") {
            $this->search();
        } else if (\Input::post("action") == "save") {
            $this->save();
        }
      

        $this->setVariable('targets', []);

        $this->setVariable('timeline_feed', true);
        $this->setVariable('keep_followers', true);
        $this->setVariable('unfollow_source', 'auto-follow');    
      
        $this->view(PLUGINS_PATH."/".self::IDNAME."/views/setup.php", null);
    }


    /**
     * Search hashtags, people, locations
     * @return mixed 
     */
    private function search()
    {
        $this->resp->result = 0;
        $AuthUser = $this->getVariable("AuthUser");
        $Account = $this->getVariable("Account");

        $query = \Input::request("q");
        if (!$query) {
            $this->resp->msg = __("Missing some of required data.");
            $this->jsonecho();
        }

        $type = \Input::request("type");
        if (!in_array($type, ["hashtag", "location", "people"])) {
            $this->resp->msg = __("Invalid parameter");
            $this->jsonecho();   
        }

        // Login
        try {
            $Instagram = \InstagramController::login($Account);
        } catch (\Exception $e) {
            $this->resp->msg = $e->getMessage();
            $this->jsonecho();   
        }



        $this->resp->items = [];

        // Get data
        try {
            if ($type == "hashtag") {
                $search_result = $Instagram->hashtag->search($query);
                if ($search_result->isOk()) {
                    foreach ($search_result->getResults() as $r) {
                        $this->resp->items[] = [
                            "value" => $r->getName(),
                            "data" => [
                                "sub" => n__("%s public post", "%s public posts", readableNumber($r->getMediaCount()), $r->getMediaCount()),
                                "id" => str_replace("#", "", $r->getName())
                            ]
                        ];
                    }
                }
            } else if ($type == "location") {
                $search_result = $Instagram->location->findPlaces($query);
                if ($search_result->isOk()) {
                    foreach ($search_result->getItems() as $r) {
                        $this->resp->items[] = [
                            "value" => $r->getLocation()->getName(),
                            "data" => [
                                "sub" => false,
                                "id" => $r->getLocation()->getFacebookPlacesId()
                            ]
                        ];
                    }
                }
            } else if ($type == "people") {
                $search_result = $Instagram->people->search($query);
                if ($search_result->isOk()) {
                    foreach ($search_result->getUsers() as $r) {
                        $sub = $r->getFullName() . " - " . readableNumber($r->getFollowerCount()) . " " . __("Followers");
                        $sub .= $r->getIsPrivate() ? __(" (Private)") : "";
                        $this->resp->items[] = [
                            "value" => $r->getUsername(),
                            "data" => [
                                "sub" => $sub,
                                "id" => $r->getPk()
                            ]
                        ];
                    }
                }
            }
        } catch (\Exception $e) {
            $this->resp->msg = $e->getMessage();
            $this->jsonecho();   
        }


        $this->resp->result = 1;
        $this->jsonecho();
    }


    /**
     * Save schedule
     * @return mixed 
     */
    private function save()
    {
        $this->resp->result = 0;
        $AuthUser = $this->getVariable("AuthUser");
        $Account  = $this->getVariable("Account");
        $Settings = $this->getVariable("Settings");
      
        $totalSources = sizeof(@json_decode($_POST['target']));
        $minSources = (int) $Settings->get("data.min_source");
        if($minSources > $totalSources) {
          $this->resp->msg = __("Please, choose at least %s sources", $minSources);
          $this->resp->result = 1;
          $this->jsonecho();
        }
      
      $modules = @json_decode(\Input::post("modules"));
      
      if (!$modules)
      {
        $this->resp->msg = __("Changes saved...");
        $this->resp->result = 1;
        $this->jsonecho();
        return;
      }
      $comments = @json_decode(\Input::post("comments"));
      $messages = @json_decode(\Input::post("messages"));

      
      foreach($modules as $k => $v)
      {
        if ($v->module == 'auto-comment' && $v->value && $comments)
        {
          $this->saveAutoComment();
        }
        elseif($v->module == 'welcomedm' && $v->value && $messages)
        {
          $this->saveWelcomeDM();
        }
        elseif($v->module == 'auto-follow' && $v->value)
        {
          $this->saveAutoFollow();
        }
        elseif($v->module == 'auto-unfollow' && $v->value)
        {
          $this->saveAutoUnfollow();
        }
        elseif($v->module == 'auto-like' && $v->value)
        {
          $this->saveAutoLike();
        }
      }

        $this->resp->msg = __("Changes saved!");
        $this->resp->result = 1;
        $this->resp->redirect = APPURL."/statistics/?id=" . $Account->get("id");
        $this->jsonecho();
    }
  
    private function saveAutoFollow()
    {
        $AuthUser = $this->getVariable("AuthUser");
        $Account = $this->getVariable("Account");
        // Get Schedule
        require_once PLUGINS_PATH . "/auto-follow/models/ScheduleModel.php";
      
        $Schedule = new \Plugins\AutoFollow\ScheduleModel([
            "account_id" => $Account->get("id"),
            "user_id" => $Account->get("user_id")
        ]);
        $this->setVariable("Schedule", $Schedule);
      

        $targets = @json_decode(\Input::post("target"));
        if (!$targets) {
            $targets = [];
        }

        $valid_targets = [];
        foreach ($targets as $t) {
            if (isset($t->type, $t->value, $t->id) && 
                in_array($t->type, ["hashtag", "location", "people"])) 
            {
                $valid_targets[] = [
                    "type" => $t->type,
                    "id" => $t->id,
                    "value" => $t->value
                ];
            }
        }
        $target = json_encode($valid_targets);

        $end_date = count($valid_targets) > 0 
                  ? "2030-12-12 23:59:59" : date("Y-m-d H:i:s");

        $daily_pause = (bool)\Input::post("daily_pause");

        $Schedule->set("user_id", $AuthUser->get("id"))
                 ->set("account_id", $Account->get("id"))
                 ->set("target", $target)
                 ->set("speed", \Input::post("speed"))
                 ->set("is_active", (bool)\Input::post("is_active"))
                 ->set("daily_pause", $daily_pause)
                 ->set("end_date", $end_date);

        $schedule_date = date("Y-m-d H:i:s", time() + 60);
        if ($daily_pause) {
            $from = new \DateTime(date("Y-m-d")." ".\Input::post("daily_pause_from"),
                                  new \DateTimeZone($AuthUser->get("preferences.timezone")));
            $from->setTimezone(new \DateTimeZone("UTC"));

            $to = new \DateTime(date("Y-m-d")." ".\Input::post("daily_pause_to"),
                                new \DateTimeZone($AuthUser->get("preferences.timezone")));
            $to->setTimezone(new \DateTimeZone("UTC"));

            $Schedule->set("daily_pause_from", $from->format("H:i:s"))
                     ->set("daily_pause_to", $to->format("H:i:s"));


            $to = $to->format("Y-m-d H:i:s");
            $from = $from->format("Y-m-d H:i:s");
            if ($to <= $from) {
                $to = date("Y-m-d H:i:s", strtotime($to) + 86400);
            }

            if ($schedule_date > $to) {
                // Today's pause interval is over
                $from = date("Y-m-d H:i:s", strtotime($from) + 86400);
                $to = date("Y-m-d H:i:s", strtotime($to) + 86400);
            }

            if ($schedule_date >= $from && $schedule_date <= $to) {
                $schedule_date = $to;
                $Schedule->set("schedule_date", $schedule_date);
            }
        }
        $Schedule->set("schedule_date", $schedule_date);

        $Schedule->save();
    }
  
    private function saveAutoUnfollow()
    {
      
        $AuthUser = $this->getVariable("AuthUser");
        $Account = $this->getVariable("Account");
        // Get Schedule
        require_once PLUGINS_PATH . "/auto-unfollow/models/ScheduleModel.php";
      
        $Schedule = new \Plugins\AutoUnfollow\ScheduleModel([
            "account_id" => $Account->get("id"),
            "user_id" => $Account->get("user_id")
        ]);
        $this->setVariable("Schedule", $Schedule);
      
        $whitelist = @json_decode(\Input::post("whitelist"));
        if (!$whitelist) {
            $whitelist = [];
        }

        $valid_whitelist = [];
        foreach ($whitelist as $t) {
            if (isset($t->value, $t->id)) {
                $valid_whitelist[] = [
                    "id" => $t->id,
                    "value" => $t->value
                ];
            }
        }
        $whitelist = json_encode($valid_whitelist);

        $end_date = count($valid_whitelist) > 0 
                  ? "2030-12-12 23:59:59" : date("Y-m-d H:i:s");

        $daily_pause = (bool)\Input::post("daily_pause");

        $Schedule->set("user_id", $AuthUser->get("id"))
                 ->set("account_id", $Account->get("id"))
                 ->set("speed", \Input::post("speed"))
                 ->set("daily_pause", $daily_pause)
                 ->set("is_active", (bool)\Input::post("is_active"))
                 ->set("keep_followers", (bool)\Input::post("keep_followers"))
                 ->set("whitelist", $whitelist)
                 ->set("source", \Input::post("source"))
                 ->set("end_date", "2030-12-12 23:59:59");

        $schedule_date = date("Y-m-d H:i:s", time() + 60);
        if ($daily_pause) {
            $from = new \DateTime(date("Y-m-d")." ".\Input::post("daily_pause_from"),
                                  new \DateTimeZone($AuthUser->get("preferences.timezone")));
            $from->setTimezone(new \DateTimeZone("UTC"));

            $to = new \DateTime(date("Y-m-d")." ".\Input::post("daily_pause_to"),
                                new \DateTimeZone($AuthUser->get("preferences.timezone")));
            $to->setTimezone(new \DateTimeZone("UTC"));

            $Schedule->set("daily_pause_from", $from->format("H:i:s"))
                     ->set("daily_pause_to", $to->format("H:i:s"));

            $to = $to->format("Y-m-d H:i:s");
            $from = $from->format("Y-m-d H:i:s");
            if ($to <= $from) {
                $to = date("Y-m-d H:i:s", strtotime($to) + 86400);
            }

            if ($schedule_date > $to) {
                // Today's pause interval is over
                $from = date("Y-m-d H:i:s", strtotime($from) + 86400);
                $to = date("Y-m-d H:i:s", strtotime($to) + 86400);
            }

            if ($schedule_date >= $from && $schedule_date <= $to) {
                $schedule_date = $to;
                $Schedule->set("schedule_date", $schedule_date);
            }
        }
        $Schedule->set("schedule_date", $schedule_date);

        $Schedule->save();
    }
    
    private function saveAutoLike()
    {
        $AuthUser = $this->getVariable("AuthUser");
        $Account = $this->getVariable("Account");
        // Get Schedule
        require_once PLUGINS_PATH . "/auto-like/models/ScheduleModel.php";
      
        $Schedule = new \Plugins\AutoLike\ScheduleModel([
            "account_id" => $Account->get("id"),
            "user_id" => $Account->get("user_id")
        ]);
        $this->setVariable("Schedule", $Schedule);
      
        $targets = @json_decode(\Input::post("target"));
        if (!$targets) {
            $targets = [];
        }

        $valid_targets = [];
        foreach ($targets as $t) {
            if (isset($t->type, $t->value, $t->id) && 
                in_array($t->type, ["hashtag", "location", "people"])) 
            {
                $valid_targets[] = [
                    "type" => $t->type,
                    "id" => $t->id,
                    "value" => $t->value
                ];
            }
        }
        $target = json_encode($valid_targets);
        
        $end_date = count($valid_targets) > 0 || (bool)\Input::post("timeline_feed")
                  ? "2030-12-12 23:59:59" : date("Y-m-d H:i:s");


        $daily_pause = (bool)\Input::post("daily_pause");

        $Schedule->set("user_id", $AuthUser->get("id"))
                 ->set("account_id", $Account->get("id"))
                 ->set("target", $target)
                 ->set("speed", \Input::post("speed"))
                 ->set("is_active", (bool)\Input::post("is_active"))
                 ->set("timeline_feed.enabled", (bool)\Input::post("timeline_feed"))
                 ->set("daily_pause", $daily_pause)
                 ->set("end_date", $end_date);

        if (!$Schedule->get("timeline_feed.schedule_date")) {
            $Schedule->get("timeline_feed.schedule_date", date("Y-m-d H:i:s", time() + 3600));
        }

        $schedule_date = date("Y-m-d H:i:s", time() + 60);
        if ($daily_pause) {
            $from = new \DateTime(date("Y-m-d")." ".\Input::post("daily_pause_from"),
                                  new \DateTimeZone($AuthUser->get("preferences.timezone")));
            $from->setTimezone(new \DateTimeZone("UTC"));

            $to = new \DateTime(date("Y-m-d")." ".\Input::post("daily_pause_to"),
                                new \DateTimeZone($AuthUser->get("preferences.timezone")));
            $to->setTimezone(new \DateTimeZone("UTC"));

            $Schedule->set("daily_pause_from", $from->format("H:i:s"))
                     ->set("daily_pause_to", $to->format("H:i:s"));


            $to = $to->format("Y-m-d H:i:s");
            $from = $from->format("Y-m-d H:i:s");
            if ($to <= $from) {
                $to = date("Y-m-d H:i:s", strtotime($to) + 86400);
            }

            if ($schedule_date > $to) {
                // Today's pause interval is over
                $from = date("Y-m-d H:i:s", strtotime($from) + 86400);
                $to = date("Y-m-d H:i:s", strtotime($to) + 86400);
            }

            if ($schedule_date >= $from && $schedule_date <= $to) {
                $schedule_date = $to;
                $Schedule->set("schedule_date", $schedule_date);
            }
        }
        $Schedule->set("schedule_date", $schedule_date);
        $Schedule->save();
      
      
    }
  
    private function saveAutoComment()
    {
        $this->resp->result = 0;
        $AuthUser = $this->getVariable("AuthUser");
        $Account = $this->getVariable("Account");
      
        // Get Schedule
        require_once PLUGINS_PATH . "/auto-comment/models/ScheduleModel.php";
      
        $Schedule = new \Plugins\AutoComment\ScheduleModel([
            "account_id" => $Account->get("id"),
            "user_id" => $Account->get("user_id")
        ]);
        $this->setVariable("Schedule", $Schedule);

        // Targets
        $targets = @json_decode(\Input::post("target"));
        if (!$targets) {
            $targets = [];
        }

        $valid_targets = [];
        foreach ($targets as $t) {
            if (isset($t->type, $t->value, $t->id) && 
                in_array($t->type, ["hashtag", "location", "people"])) 
            {
                $valid_targets[] = [
                    "type" => $t->type,
                    "id" => $t->id,
                    "value" => $t->value
                ];
            }
        }
        $target = json_encode($valid_targets);

        $end_date = count($valid_targets) > 0 || (bool)\Input::post("timeline_feed")
                  ? "2030-12-12 23:59:59" : date("Y-m-d H:i:s");

        $daily_pause = (bool)\Input::post("daily_pause");

        $Schedule->set("user_id", $AuthUser->get("id"))
                 ->set("account_id", $Account->get("id"))
                 ->set("target", $target)
                 ->set("speed", \Input::post("speed"))
                 ->set("is_active", (bool)\Input::post("is_active"))
                 ->set("timeline_feed.enabled", (bool)\Input::post("timeline_feed"))
                 ->set("daily_pause", $daily_pause)
                 ->set("end_date", $end_date);

        if (!$Schedule->get("timeline_feed.schedule_date")) {
            $Schedule->get("timeline_feed.schedule_date", date("Y-m-d H:i:s", time() + 3600));
        }

        $schedule_date = date("Y-m-d H:i:s", time() + 60);
        if ($daily_pause) {
            $from = new \DateTime(date("Y-m-d")." ".\Input::post("daily_pause_from"),
                                  new \DateTimeZone($AuthUser->get("preferences.timezone")));
            $from->setTimezone(new \DateTimeZone("UTC"));

            $to = new \DateTime(date("Y-m-d")." ".\Input::post("daily_pause_to"),
                                new \DateTimeZone($AuthUser->get("preferences.timezone")));
            $to->setTimezone(new \DateTimeZone("UTC"));

            $Schedule->set("daily_pause_from", $from->format("H:i:s"))
                     ->set("daily_pause_to", $to->format("H:i:s"));


            $to = $to->format("Y-m-d H:i:s");
            $from = $from->format("Y-m-d H:i:s");
            if ($to <= $from) {
                $to = date("Y-m-d H:i:s", strtotime($to) + 86400);
            }

            if ($schedule_date > $to) {
                // Today's pause interval is over
                $from = date("Y-m-d H:i:s", strtotime($from) + 86400);
                $to = date("Y-m-d H:i:s", strtotime($to) + 86400);
            }

            if ($schedule_date >= $from && $schedule_date <= $to) {
                $schedule_date = $to;
                $Schedule->set("schedule_date", $schedule_date);
            }
        }
        $Schedule->set("schedule_date", $schedule_date);
          
        // Emojione Client
        $Emojione = new \Emojione\Client(new \Emojione\Ruleset());

        // Comments
        $raw_comments = @json_decode(\Input::post("comments"));
        $valid_comments = [];
        if ($raw_comments) {
            foreach ($raw_comments as $c) {
                $valid_comments[] = $Emojione->toShort($c);
            }
        }
        $comments = json_encode($valid_comments);
        $Schedule->set("comments", $comments)
            ->save();
    }


    private function saveWelcomeDM()
    {
        $AuthUser = $this->getVariable("AuthUser");
        $Account = $this->getVariable("Account");
        // Get Schedule
        require_once PLUGINS_PATH . "/welcomedm/models/ScheduleModel.php";
      
        $Schedule = new \Plugins\WelcomeDM\ScheduleModel([
            "account_id" => $Account->get("id"),
            "user_id" => $Account->get("user_id")
        ]);
        $this->setVariable("Schedule", $Schedule);
      
        // Daily pause
        $daily_pause = (bool)\Input::post("daily_pause");
        
        // End date
        $end_date = "2030-12-12 23:59:59";
      
      
        $speed = \Input::post("speed");
        switch($speed)
        {
          case 'very_slow':
            $speedVal = 1;
            break;
          case 'slow':
            $speedVal = 2;
            break;
          case 'medium':
            $speedVal = 3;
            break;
          case 'fast':
            $speedVal = 4;
            break;
          case 'very_fast':
            $speedVal = 5;
            break;
          default :
            $speedVal = 0;
        }

        $Schedule->set("user_id", $AuthUser->get("id"))
                 ->set("account_id", $Account->get("id"))
                 ->set("speed", $speedVal)
                 ->set("daily_pause", $daily_pause)
                 ->set("is_active", (bool)\Input::post("is_active"))
                 ->set("end_date", $end_date);

        $schedule_date = date("Y-m-d H:i:s", time() + 60);
        if ($daily_pause) {
            $from = new \DateTime(date("Y-m-d")." ".\Input::post("daily_pause_from"),
                                  new \DateTimeZone($AuthUser->get("preferences.timezone")));
            $from->setTimezone(new \DateTimeZone("UTC"));

            $to = new \DateTime(date("Y-m-d")." ".\Input::post("daily_pause_to"),
                                new \DateTimeZone($AuthUser->get("preferences.timezone")));
            $to->setTimezone(new \DateTimeZone("UTC"));

            $Schedule->set("daily_pause_from", $from->format("H:i:s"))
                     ->set("daily_pause_to", $to->format("H:i:s"));


            $to = $to->format("Y-m-d H:i:s");
            $from = $from->format("Y-m-d H:i:s");
            if ($to <= $from) {
                $to = date("Y-m-d H:i:s", strtotime($to) + 86400);
            }

            if ($schedule_date > $to) {
                // Today's pause interval is over
                $from = date("Y-m-d H:i:s", strtotime($from) + 86400);
                $to = date("Y-m-d H:i:s", strtotime($to) + 86400);
            }

            if ($schedule_date >= $from && $schedule_date <= $to) {
                $schedule_date = $to;
                $Schedule->set("schedule_date", $schedule_date);
            }
        }
      
        $Schedule->set("schedule_date", $schedule_date);
      

        // Emojione Client
        $Emojione = new \Emojione\Client(new \Emojione\Ruleset());

        // Messages
        $raw_messages = @json_decode(\Input::post("messages"));
        $valid_messages = [];
        if ($raw_messages) {
            foreach ($raw_messages as $m) {
                $valid_messages[] = $Emojione->toShort($m);
            }
        }
        $messages = json_encode($valid_messages);

        $Schedule->set("messages", $messages)
                 ->save();
      
    }
}
