<?php
namespace Plugins\IwoHunter;

// Disable direct access
if (!defined('APP_VERSION')) 
    die("Yo, what's up?");

/**
 * Comments Controller
 */
class TaskController extends \Controller
{
    /**
     * Process
     */
    protected $Instagram;

    public function process()
    {
        $AuthUser = $this->getVariable("AuthUser");
        $Route = $this->getVariable("Route");
        $this->setVariable("idname", "iwo-hunter");

        // Auth
        if (!$AuthUser){
            header("Location: ".APPURL."/login");
            exit;
        } else if ($AuthUser->isExpired()) {
            header("Location: ".APPURL."/expired");
            exit;
        }

        $user_modules = $AuthUser->get("settings.modules");
        if (!is_array($user_modules) || !in_array($this->getVariable("idname"), $user_modules)) {
            // Module is not accessible to this user
            header("Location: ".APPURL."/post");
            exit;
        }


        // Get account
        $Account = \Controller::model("Account", $Route->params->id);
        if (!$Account->isAvailable() || 
            $Account->get("user_id") != $AuthUser->get("id")) 
        {
            header("Location: ".APPURL."/e/".$this->getVariable("idname"));
            exit;
        }
        $this->setVariable("Account", $Account);
        $this->Instagram = \InstagramController::login($Account);
        if (\Input::request("action") == "search") {
            $this->search();
        }
        switch (\Input::post('comand')){
            case 'getTask':
                return $this->getTask();
            case 'saveTask':
                return $this->saveTask();
            case 'deleteTask':
                return $this->deleteTask();
        }

        $this->view(PLUGINS_PATH."/".$this->getVariable("idname")."/views/manager.php", null);
    }


    /**
     * Save schedule
     * @return mixed 
     */
    private function getTask(){
        $Account = $this->getVariable('Account');
        // Get Schedule
        require_once PLUGINS_PATH."/".$this->getVariable("idname")."/models/ScheduleModel.php";
        require_once PLUGINS_PATH."/".$this->getVariable("idname")."/models/SchedulesModel.php";
        $Schedule = new SchedulesModel();
        $Schedule->where("account_id" ,'=',$Account->get("id"))->orderBy("id","ASC")->fetchData();
        $this->setVariable("Schedule", $Schedule);
        $list = $Schedule->getDataAs( [PLUGINS_PATH."/iwo-hunter/models/ScheduleModel.php", __NAMESPACE__."\ScheduleModel"]);
        $returnList = [];
        foreach ($list as $item){
            $returnList[] = $this->_getItem($item);
        }

        $this->resp->tasks = $returnList;
        $this->jsonecho();
    }
    private function _getItem($item){
        $el = new \stdClass();
        $el->id = $item->get('id');
        //$el->user_id = $item->get('user_id');
        //$el->account_id = $item->get('account_id');
        $el->tasks = json_decode($item->get('tasks'));
        $el->speed = $item->get('speed');
        $el->is_active = $item->get('is_active');
        //$el->schedule_date = $item->get('schedule_date');
        //$el->end_date = $item->get('end_date');
        //$el->last_action_date = $item->get('last_action_date');
        return $el;
    }
    private function saveTask(){
        $Account = $this->getVariable('Account');
        require_once PLUGINS_PATH."/".$this->getVariable("idname")."/models/ScheduleModel.php";

        $task = json_decode(\Input::request('task'));
        if(property_exists($task, 'id') && $task->id){
            $scheduleModel = new ScheduleModel($task->id);
        }else{
            $scheduleModel = new ScheduleModel();
            $scheduleModel->set('account_id', $Account->get('id'));
            $scheduleModel->set('user_id', $Account->get('user_id'));
        }
        $scheduleModel->set('tasks', json_encode($task->tasks));
        $scheduleModel->set('speed', $task->speed);
        $scheduleModel->set('end_date', $task->is_active ? "2030-12-12 23:59:59" : date("Y-m-d H:i:s"));
        $scheduleModel->set('is_active', $task->is_active);
        $scheduleModel->save();
        $this->resp->status=1;
        $this->resp->task = $this->_getItem($scheduleModel);;
        $this->jsonecho();
    }
    private function _nomalizeText($text, $needBefore = true){
        $Emojione = new \Emojione\Client(new \Emojione\Ruleset());

        $return = $Emojione->shortnameToUnicode($text);
        $return = mb_substr($return, 0, 2200);
        $spec1 = ' 
';
        $spec = '
';
        $spec3 = '⠀
';
        while (strpos($return, '⠀⠀')>-1){
            $return = str_replace('⠀⠀', '⠀', $return);
        }
        while (strpos($return, $spec1)){
            $return = str_replace($spec1, $spec, $return);
        }
        $return = str_replace($spec, $spec3, $return);

        if($needBefore){
            $return = $spec3 . $return;
        }

        return $return;
    }
    private function deleteTask(){
        $task_id = \Input::post('id');
        require_once PLUGINS_PATH."/".$this->getVariable("idname")."/models/ScheduleModel.php";
        $scheduleModel = new ScheduleModel($task_id);
        $scheduleModel->delete();
        $this->resp->status=1;
        $this->jsonecho();
    }
    private function search()
    {
        $this->resp->result = 0;
        $AuthUser = $this->getVariable("AuthUser");
        $Account = $this->getVariable("Account");

        $query = \Input::request("q");
        if (!$query) {
            $this->resp->msg = __("Missing some of required data.");
            $this->jsonecho();
        }

        $type = \Input::request("type");
        if (!in_array($type, ["hunter", "hashtag", "location", "people"])) {
            $this->resp->msg = __("Invalid parameter");
            $this->jsonecho();
        }

        // Login

        try {
            $Instagram = \InstagramController::login($Account);
        } catch (\Exception $e) {
            $this->resp->msg = $e->getMessage();
            $this->jsonecho();
        }

        $this->resp->items = [];

        // Get data
        try {
            if ($type == "hashtag") {
                $search_result = $Instagram->hashtag->search($query);
                if ($search_result->isOk()) {
                    foreach ($search_result->getResults() as $r) {
                        $this->resp->items[] = [
                            "value" => $r->getName(),
                            "data" => [
                                "sub" => n__("%s public post", "%s public posts", $r->getMediaCount(), $r->getMediaCount()),
                                "id" => str_replace("#", "", $r->getName())
                            ]
                        ];
                    }
                }
            } else if ($type == "location") {
                $search_result = $Instagram->location->findPlaces($query);
                if ($search_result->isOk()) {
                    foreach ($search_result->getItems() as $r) {
                        $this->resp->items[] = [
                            "value" => $r->getLocation()->getName(),
                            "data" => [
                                "sub" => false,
                                "id" => $r->getLocation()->getFacebookPlacesId()
                            ]
                        ];
                    }
                }
            } else if ($type == "people" || $type=="hunter") {
                $search_result = $Instagram->people->search($query);
                if ($search_result->isOk()) {
                    foreach ($search_result->getUsers() as $r) {
                        $this->resp->items[] = [
                            "value" => $r->getUsername(),
                            "data" => [
                                "sub" => $r->getFullName(),
                                "id" => $r->getPk()
                            ]
                        ];
                    }
                }
            }
        } catch (\Exception $e) {
            $this->resp->msg = $e->getMessage();
            $this->jsonecho();
        }

        $this->resp->result = 1;
        $this->jsonecho();
    }
}
