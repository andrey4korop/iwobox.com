<?php if (!defined('APP_VERSION')) die("Yo, what's up?"); ?>
<?php 
    return [
        "idname" => "iwo-hunter",
        "plugin_name" => "Iwo Hunter",
        "plugin_uri" => "",
        "author" => "Andrey4korop",
        "author_uri" => "",
        "version" => "1.0",
        "desc" => "",
        "icon_style" => "background: linear-gradient(45deg, #ff3232 0%,#fff728 16%,#28fc32 32%,#30f4ff 48%,#3730ff 64%,#ff30f1 80%,#ff0004 100%);background-size: 1000% 100%; color: #fff; font-size: 18px; animation: thisAnima 5s infinite",
    ];
    