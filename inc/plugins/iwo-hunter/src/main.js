import Vue from 'vue';
import axios from 'axios'
import VueAxios from 'vue-axios'
import InputTag from 'vue-input-tag'
//import Notifications from 'vue-notification'
window.querystring = require('querystring');

import App from './App.vue'

import Task from './components/Task.vue'
import target from './components/target.vue'
import follow from './components/follow.vue'
import like from './components/like.vue'
import unfollow from './components/unfollow.vue'

Vue.use(VueAxios, axios);
//Vue.use(Notifications);
Vue.component('input-tag', InputTag);

Vue.component('Task', Task);
Vue.component('target', target);
Vue.component('follow', follow);
Vue.component('like', like);
Vue.component('unfollow', unfollow);

var _ = require('lodash');
new Vue({
  el: '#hunterApp',
  render: h => h(App)
})
