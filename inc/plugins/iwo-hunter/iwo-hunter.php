<?php 
namespace Plugins\IwoHunter;
const IDNAME = "iwo-hunter";

// Disable direct access
if (!defined('APP_VERSION')) 
    die("Yo, what's up?"); 


/**
 * Event: plugin.install
 */
function install($Plugin)
{
    if ($Plugin->get("idname") != IDNAME) {
        return false;
    }
}
\Event::bind("plugin.install", __NAMESPACE__ . '\install');



/**
 * Event: plugin.remove
 */
function uninstall($Plugin)
{
    if ($Plugin->get("idname") != IDNAME) {
        return false;
    }

    // Remove plugin settings
    $settings = namespace\settings();
    $settings->remove();
}
\Event::bind("plugin.remove", __NAMESPACE__ . '\uninstall');


/**
 * Add module as a package options
 * Only users with granted permission
 * Will be able to use module
 * 
 * @param array $package_modules An array of currently active 
 *                               modules of the package
 */
function add_module_option($package_modules)
{
    $config = include __DIR__."/config.php";
    ?>
        <div class="mt-15">
            <label>
                <input type="checkbox" 
                       class="checkbox" 
                       name="modules[]" 
                       value="<?= IDNAME ?>" 
                       <?= in_array(IDNAME, $package_modules) ? "checked" : "" ?>>
                <span>
                    <span class="icon unchecked">
                        <span class="mdi mdi-check"></span>
                    </span>
                    <?= __('Iwo Hunter') ?>
                </span>
            </label>
        </div>
    <?php
}
\Event::bind("package.add_module_option", __NAMESPACE__ . '\add_module_option');




/**
 * Map routes
 */
function route_maps($global_variable_name)
{
    // Index
    $GLOBALS[$global_variable_name]->map("GET|POST", "/e/".IDNAME."/?", [
        PLUGINS_PATH . "/". IDNAME ."/controllers/IndexController.php",
        __NAMESPACE__ . "\IndexController"
    ]);

    // ManagerController
    $GLOBALS[$global_variable_name]->map("GET|POST", "/e/".IDNAME."/[i:id]/?", [
        PLUGINS_PATH . "/". IDNAME ."/controllers/TaskController.php",
        __NAMESPACE__ . "\TaskController"
    ]);

}
\Event::bind("router.map", __NAMESPACE__ . '\route_maps');



/**
 * Event: navigation.add_special_menu
 */
function navigation($Nav, $AuthUser)
{
    $idname = IDNAME;
    include __DIR__."/views/fragments/navigation.fragment.php";
}
\Event::bind("navigation.add_menu", __NAMESPACE__ . '\navigation');


/**
 * Get Plugin Settings
 * @return \GeneralDataModel 
 */
function settings()
{
    $settings = \Controller::model("GeneralData", "plugin-".IDNAME."-settings");
    return $settings;
}


function addCronTask()
{
    // Get auto like schedules
    require_once __DIR__ . "/models/SchedulesModel.php";
    require_once __DIR__ . "/models/OtherTaskModel.php";

    //$Emojione = new \Emojione\Client(new \Emojione\Ruleset())
    $Schedules = new SchedulesModel;

    $Schedules->where("is_active", "=", 1)
        ->where("schedule_date", "<=", date("Y-m-d H:i:s"))
        ->where("end_date", ">=", date("Y-m-d H:i:s"))
        ->orderBy("last_action_date", "ASC")
        ->setPageSize(10)// required to prevent server overload
        ->setPage(1)
        ->fetchData();
    if ($Schedules->getTotalCount() < 1) {
        // There is not any active schedule
        return false;
    }

    $as = [__DIR__ . "/models/ScheduleModel.php", __NAMESPACE__ . "\ScheduleModel"];
    foreach ($Schedules->getDataAs($as) as $sc) {
        var_dump('iwoHunter');
        // Set next schedule date
        $reqcount = [
            // speed => number of requests per hour
            "1" => 1, // Very Slow
            "2" => 2, // Slow
            "3" => 3, // Medium
            "4" => 4, // Fast
            "5" => 5  // Very Fast
        ];
        $speed = (int)$sc->get("speed");
        $delta = isset($reqcount[$speed]) && $reqcount[$speed] > 0
            ? round(2000 / $reqcount[$speed]) : rand(720, 7200);
        $next_schedule_timestamp = time() + $delta;
        $sc->set("schedule_date", date("Y-m-d H:i:s", $next_schedule_timestamp))
            ->set("last_action_date", date("Y-m-d H:i:s"))
            ->save();

        // Start data validation
        $Account = \Controller::model("Account", $sc->get("account_id"));
        if (!$Account->isAvailable() || $Account->get("login_required")) {
            // Account is either removed (unexected, external factors)
            // Or login required for this account
            // Deactivate schedule
            $sc->set("is_active", 0)->save();
            continue;
        }

        $User = \Controller::model("User", $sc->get("user_id"));
        if (!$User->isAvailable() || !$User->get("is_active") || $User->isExpired()) {
            // User is not valid
            // Deactivate schedule
            $sc->set("is_active", 0)->save();
            continue;
        }

        if ($User->get("id") != $Account->get("user_id")) {
            // Unexpected, data modified by external factors
            // Deactivate schedule
            $sc->set("is_active", 0)->save();
            continue;
        }

        // Create a action log (not save yet)
        require_once __DIR__ . "/models/LogModel.php";
        $Log = new LogModel;
        $Log->set("user_id", $User->get("id"))
            ->set("account_id", $Account->get("id"))
            ->set("status", "error");

        // Login into the account
        try {
            $Instagram = \InstagramController::login($Account);
        } catch (\Exception $e) {
            // Couldn't login into the account
            $Account->refresh();
            if ($Account->get("login_required")) {
                // Deactivate schedule
                $sc->set("is_active", 0)->save();
            }
            // Log data
            $Log->set("data", $e->getMessage())
                ->save();
            continue;
        }

        // Logged in successfully

        // Find user
        $follower_id = null;
        $follower_username = null;
        $follower_fullname = null;
        $follower_profile_pic = null;

        $rank_token = \InstagramAPI\Signatures::generateUUID();

        //$targets = @json_decode($sc->get("tasks"));
        $tasks = @json_decode($sc->get("tasks"));
        if(!$tasks || !count($tasks) || $tasks[0]->type!='target'){
            //$sc->set("is_active", 0)->save();
            continue;
        }
        $targets = $tasks[0]->data->targets;
        if (!$targets) {
            // Unexpected, data modified by external factors
            // Deactivate schedule
            //$sc->set("is_active", 0)->save();
            continue;
        }
        $i = rand(0, count($targets) - 1);
        $target = $targets[$i];

        if ($target->type == "hunter") {
            $round = 1;
            $next_max_id = null;

                try {
                    $feed = \InstagramController::getFollowers($Instagram, $target->id);
                } catch (\Exception $e) {
                    // Couldn't get instagram feed related to the user id

                    if ($round == 1) {
                        // Log data
                        $Log->set("data.error.msg", "Couldn't get the feed")
                            ->set("data.error.details", $e->getMessage())
                            ->save();
                    }
                    continue;
                }
                if (count($feed) < 1) {
                    // Invalid
                    continue;
                }

                // Get friendship statuses
                $user_ids = [];
                foreach ($feed as $user) {
                    $user_ids[] = $user['id'];
                }

                try {
                    $friendships = $Instagram->people->getFriendships($user_ids);
                } catch (\Exception $e) {
                    // Couldn't get instagram friendship statuses

                    if ($round == 1) {
                        // Log data
                        $Log->set("data.error.msg", "Couldn't get the friendship statuses")
                            ->set("data.error.details", $e->getMessage())
                            ->save();
                    }
                    continue;
                }
                $followings = [];

                foreach ($friendships->getFriendshipStatuses()->getData() as $pk => $fs) {
                    if ($fs->getOutgoingRequest() || $fs->getFollowing()) {
                        $followings[] = $pk;
                    }
                }

                foreach ($feed as $user) {

                    if (!in_array($user['id'], $followings) &&
                        $user['id'] != $Account->get("instagram_id") /*&&
                        hasFilter($Instagram, $sc, null, $user)*/) {
                        $_log = new LogModel([
                            "user_id" => $User->get("id"),
                            "account_id" => $Account->get("id"),
                            "target_id" =>$user['id'],
                            "status" => "success"
                        ]);

                        if (!$_log->isAvailable()) {
                            // Found new user
                            $follower_id = $user['id'];
                            $follower_username = $user['username'];
                            $follower_fullname = $user['full_name'];
                            $follower_profile_pic = $user['profile_pic_url'];
                            break;
                        }
                    }
                }
                if(!$follower_id) {
                    $timeLine = $Instagram->timeline->getUserFeed($target->id);
                    $timeLine = $timeLine->getItems();

                    if (count($timeLine) < 1) {
                        continue;
                    }
                    $mediaId = [];
                    foreach ($timeLine as $media) {
                        $mediaId[] = $media->getCode();
                    }

                    //from likers
                    foreach ($mediaId as $mediaItem) {
                        //$likers = $Instagram->media->getLikers($mediaItem);
                        $likers = \InstagramController::getLikers($Instagram, $mediaItem);
                        if (count($likers) < 1) {
                            continue;
                        }

                        $likers = $likers;
                        $likersPk = [];
                        foreach ($likers as $liker) {
                            if (!in_array($liker->getId(), $followings)) {
                                $likersPk[] = $liker->getId();
                            }
                        }

                        $likeUser = [];
                        try {
                            $friendships = $Instagram->people->getFriendships($likersPk);
                        } catch (\Exception $e) {
                            // Couldn't get instagram friendship statuses
                            continue;
                        }
                        foreach ($friendships->getFriendshipStatuses()->getData() as $pk => $fs) {
                            if (!$fs->getOutgoingRequest() || !$fs->getFollowing()) {
                                $likeUser[] = $pk;
                            }
                        }
                        if (count($likeUser)) {
                            foreach ($likers as $user) {

                                if (in_array($user->getId(), $likeUser) &&
                                    $user->getId() != $Account->get("instagram_id") /*&&
                        hasFilter($Instagram, $sc, null, $user)*/) {

                                    $_log = new LogModel([
                                        "user_id" => $User->get("id"),
                                        "account_id" => $Account->get("id"),
                                        "target_id" => $user->getId(),
                                        "status" => "success"
                                    ]);
                                    if (!$_log->isAvailable()) {
                                        // Found new user
                                        $follower_id = $user->getId();
                                        $follower_username = $user->getUsername();
                                        $follower_fullname = $user->getUsername();
                                        //$follower_profile_pic = $user->getProfilePicUrl();
                                        break 2;
                                    }
                                }
                            }
                        }
                    }
                    if(!$follower_id) {
                        //coment
                        foreach ($mediaId as $media) {
                            //$coments = $Instagram->media->getComments($media);
                            $coments = \InstagramController::getComments($media);
                            if (count($coments) < 1) {
                                continue;
                            }
                            $comentPk = [];
                            foreach ($coments as $coment) {

                                if (!in_array($coment->getOwner()->getId(), $followings)) {
                                    $comentPk[] = $coment->getOwner()->getId();
                                }
                            }
                            try {
                                $friendships = $Instagram->people->getFriendships($comentPk);

                            } catch (\Exception $e) {
                                continue;
                            }
                            $comentUser = [];
                            foreach ($friendships->getFriendshipStatuses()->getData() as $pk => $fs) {
                                if (!$fs->getOutgoingRequest() || !$fs->getFollowing()) {
                                    $comentUser[] = $pk;
                                }
                            }
                            if (count($comentUser)) {
                                foreach ($comentUser as $user) {
                                    if ($user != $Account->get("instagram_id") /*&&
                        hasFilter($Instagram, $sc, null, $user)*/) {
                                        $_log = new LogModel([
                                            "user_id" => $User->get("id"),
                                            "account_id" => $Account->get("id"),
                                            "target_id" => $user,
                                            "status" => "success"
                                        ]);
                                        $userInfo = $Instagram->people->getInfoById($user)->getUser();
                                        if (!$_log->isAvailable()) {
                                            // Found new user
                                            $follower_id = $user;
                                            $follower_username = $userInfo->getUsername();
                                            $follower_fullname = $userInfo->getFullName();
                                            $follower_profile_pic = $userInfo->getProfilePicUrl();
                                            break 2;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            //$count_iteretion = 10;
        }else if ($target->type == "hashtag") {
            try {
                $feed = $Instagram->hashtag->getFeed(
                    str_replace("#", "", trim($target->id)),
                    $rank_token);
            } catch (\Exception $e) {
                // Couldn't get instagram feed related to the hashtag

                // Log data
                $Log->set("data.error.msg", "Couldn't get the feed")
                    ->set("data.error.details", $e->getMessage())
                    ->save();
                continue;
            }

            if (count($feed->getItems()) < 1) {
                // Invalid
                continue;
            }


            foreach ($feed->getItems() as $item) {
                if ($item->getUser()->getPk() != $Account->get("instagram_id")/* &&
                    hasFilter($Instagram, $sc, $item)*/) {
                    $_log = new LogModel([
                        "user_id" => $User->get("id"),
                        "account_id" => $Account->get("id"),
                        "follower_id" => $item->getUser()->getPk(),
                        "status" => "success"
                    ]);
                    if (!$_log->isAvailable()) {
                        // Found new user
                        $follower_id = $item->getUser()->getPk();
                        $follower_username = $item->getUser()->getUsername();
                        $follower_fullname = $item->getUser()->getFullName();
                        $follower_profile_pic = $item->getUser()->getProfilePicUrl();
                        break 1;
                    }
                }
            }
        } else if ($target->type == "location") {
            try {
                $feed = $Instagram->location->getFeed(
                    $target->id,
                    $rank_token);
            } catch (\Exception $e) {
                // Couldn't get instagram feed related to the location id

                // Log data
                $Log->set("data.error.msg", "Couldn't get the feed")
                    ->set("data.error.details", $e->getMessage())
                    ->save();
                continue;
            }

            if (count($feed->getItems()) < 1) {
                // Invalid
                continue;
            }

            foreach ($feed->getItems() as $item) {
                if ($item->getUser()->getPk() != $Account->get("instagram_id") /*&&
                    hasFilter($Instagram, $sc, $item)*/) {
                    $_log = new LogModel([
                        "user_id" => $User->get("id"),
                        "account_id" => $Account->get("id"),
                        "follower_id" => $item->getUser()->getPk(),
                        "status" => "success"
                    ]);
                    if (!$_log->isAvailable()) {
                        // Found new user
                        $follower_id = $item->getUser()->getPk();
                        $follower_username = $item->getUser()->getUsername();
                        $follower_fullname = $item->getUser()->getFullName();
                        $follower_profile_pic = $item->getUser()->getProfilePicUrl();
                        break 1;
                    }
                }
            }
        } else if ($target->type == "people") {
            $round = 1;
            $loop = true;
            $next_max_id = null;

            while ($loop) {
                try {
                    $feed = $Instagram->people->getFollowers(
                        $target->id,
                        $rank_token,
                        null,
                        $next_max_id);
                } catch (\Exception $e) {
                    // Couldn't get instagram feed related to the user id
                    $loop = false;

                    if ($round == 1) {
                        // Log data
                        $Log->set("data.error.msg", "Couldn't get the feed")
                            ->set("data.error.details", $e->getMessage())
                            ->save();
                    }
                    continue 2;
                }

                if (count($feed->getUsers()) < 1) {
                    // Invalid
                    $loop = false;
                    continue 2;
                }

                // Get friendship statuses
                $user_ids = [];
                foreach ($feed->getUsers() as $user) {
                    $user_ids[] = $user->getPk();
                }

                try {
                    $friendships = $Instagram->people->getFriendships($user_ids);
                } catch (\Exception $e) {
                    // Couldn't get instagram friendship statuses
                    $loop = false;

                    if ($round == 1) {
                        // Log data
                        $Log->set("data.error.msg", "Couldn't get the friendship statuses")
                            ->set("data.error.details", $e->getMessage())
                            ->save();
                    }
                    continue 2;
                }

                $followings = [];
                foreach ($friendships->getFriendshipStatuses()->getData() as $pk => $fs) {
                    if ($fs->getOutgoingRequest() || $fs->getFollowing()) {
                        $followings[] = $pk;
                    }
                }


                foreach ($feed->getUsers() as $user) {
                    if (!in_array($user->getPk(), $followings) &&
                        $user->getPk() != $Account->get("instagram_id")/* &&
                        hasFilter($Instagram, $sc, null, $user)*/) {
                        $_log = new LogModel([
                            "user_id" => $User->get("id"),
                            "account_id" => $Account->get("id"),
                            "follower_id" => $user->getPk(),
                            "status" => "success"
                        ]);

                        if (!$_log->isAvailable()) {
                            // Found new user
                            $follower_id = $user->getPk();
                            $follower_username = $user->getUsername();
                            $follower_fullname = $user->getFullName();
                            $follower_profile_pic = $user->getProfilePicUrl();
                            break 2;
                        }
                    }
                }

                $round++;
                $next_max_id = $feed->getNextMaxId();
                if ($round >= 5 || !empty($follow_pk) || $next_max_id === null) {
                    $loop = false;
                }
            }
            //$count_iteretion = 10;
        }

        if (empty($follower_id)) {
            $Log->set("data.error.msg", "Couldn't find new user to follow")
                ->save();
            continue;
        }
        $tasks = json_decode($sc->get('tasks'));
        if(count($tasks)<2){
            continue;
        }
        $d = new \DateTime();
        $dt = new \DateInterval('PT1M');
        for ($i=1; $i<count($tasks); $i++){
            if($tasks[$i]->type == 'like'){
                for ($j=0; $j < $tasks[$i]->data->filters->count; $j++){
                    $dt = new \DateInterval('PT'.$tasks[$i]->data->filters->wait.'M');
                    $d = date_add($d, $dt);
                    $task = new OtherTaskModel;

                    $task->set("status", "wait");
                    $task->set("user_id", $User->get("id"));
                    $task->set("account_id", $Account->get("id"));
                    $task->set("type", "like");
                    $task->set("create_date", date("Y-m-d H:i:s"));
                    $task->set("schedule_date", $d->format("Y-m-d H:i:s"));
                    $task->set("worked_date", date("Y-m-d H:i:s"));
                    $task->set("data", json_encode([
                        "pk" => $follower_id,
                        "username" => $follower_username,
                        "likeProperty" => $tasks[$i]->data->filters,
                    ]));

                    $task->save();
                }
            }else if($tasks[$i]->type == 'follow'){
                $dt = new \DateInterval('PT'.$tasks[$i]->data->filters->wait.'M');
                $d = date_add($d, $dt);
                $task = new OtherTaskModel;

                $task->set("status", "wait");
                $task->set("user_id", $User->get("id"));
                $task->set("account_id", $Account->get("id"));
                $task->set("type", "follow");
                $task->set("create_date", date("Y-m-d H:i:s"));
                $task->set("schedule_date", $d->format("Y-m-d H:i:s"));
                $task->set("worked_date", date("Y-m-d H:i:s"));
                $task->set("data", json_encode([
                    "pk" => $follower_id,
                    "username" => $follower_username,
                ]));
                $task->save();
            }else if($tasks[$i]->type == 'unfollow'){
                $dt = new \DateInterval('P'.$tasks[$i]->data->filters->wait.'D');
                $d = date_add($d, $dt);
                //$d = $d->add($dt);
                $task = new OtherTaskModel;

                $task->set("status", "wait");
                $task->set("user_id" , $User->get("id"));
                $task->set("account_id" , $Account->get("id"));
                $task->set("type" , "unfollow");
                $task->set("create_date" , date("Y-m-d H:i:s"));
                $task->set("schedule_date" , $d->format("Y-m-d H:i:s"));
                $task->set("worked_date", date("Y-m-d H:i:s"));
                $task->set("data" , json_encode([
                    "pk" => $follower_id,
                    "username" => $follower_username,
                ]));

                $task->save();
            }
        }

        $Log->set("status", "success")
            ->set("data.followed", [
                "pk" => $follower_id,
                "username" => $follower_username,
                "full_name" => $follower_fullname,
                "profile_pic_url" => $follower_profile_pic
            ])
            ->set("target_id", $follower_id)
            ->save();

    }
}
\Event::bind("cron.add", __NAMESPACE__."\addCronTask");
